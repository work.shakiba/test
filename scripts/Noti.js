//region require
var helper = require('../scripts/helper.js');
var ws = require('../scripts/websocket.js');
var auth = require('../scripts/auth');
var models = require('../models');
var formidable = require('formidable');
var fs = require('fs-extra');
var gm = require('gm').subClass({ imageMagick: true });
var async = require('async');
var MetaInspector = require('node-metainspector');
var cheerio = require('cheerio');
var sizeOf = require('image-size');
//endregion

//region http
exports.app = function(app) {

    /**
     * get the count of unread notifications
     */
    app.get('/api/auth/get_notifications_count', function (req, res, next) {
        var userID = req.session.user._id
        actions.getNotiCountForUser(req.session.user._id, function(err, model){
            return res.json({result: model})
        })
    })

    app.get('/api/auth/get_notifications', function (req, res, next) {
        var skip = req.query.offset
        actions.getNotiForUser(req.session.user._id, skip, function(err, model){
            return res.json({result: model})
        })
    })

    app.put('/api/auth/clear_noti', function (req, res, next) {
        models.Noti.update({objectOwnerId: req.session.user._id}, {$set: {seen: true}}, {multi: true}, function(err, model){
            if(err) helper.log("error in clearing the notification", err)
            return res.json({result: true})
        });
    })

    app.get('/api/auth/send_fake_noti', function (req, res, next) {
        models.Noti.findOne({}, function(err, model){
            ws.emit("id1", 'notification', model)
            return res.json({result: model})
        })

    });

}
//endregion

var noties = [
    "follow",
    "like",
    "comment",
    "post_blocked", // your post has been removed
    "flag_post_blocked", //post that you have flagged, has been removed
    "you_are_blocked", // you are now blocked
    "flagged_user_blocked", // user which you have flagged has been blocked
    "you_are_sustained::", // you are now sustained till
    "you_unsustained", // your sustained profile has been lifted
]

//region noti
var actions = {

    // type_ = [comment, like, following]
    setNotiForUser: function(objectOwnerId_, dowerId_, objectId_, type_){
        helper.log("objectOwnerId_", objectOwnerId_)
        helper.log("objectId_", objectId_)
        if(objectOwnerId_ == dowerId_){return;}
        helper.logEvent("event", "noti - setNotiForUser");
        var notiModel = {"objectOwnerId": objectOwnerId_, "dowerId": dowerId_, "objectId": objectId_, "type": type_};
        models.Noti.findOne(notiModel ,function(error, model){
            if(!model){
                // set date
                notiModel.date = Date.now();
                // create the noti
                models.Noti.create(notiModel, function(err, model){
                    helper.log("Noti model to created", err)
                    models.Noti.findOne({_id: model._id}).populate("dowerId").exec(function(err, model){
                        ws.emit(objectOwnerId_, 'notification', model);
                    })
                });
            }
        });
    },

    getNotiForUser: function(userId_, offset_, callback_){
        helper.logEvent("event", "noti - getNotiForUser");
        models.Noti
            .find({objectOwnerId: userId_})
            .limit(20)
            .sort({date: -1})
            .skip(offset_)
            .populate("dowerId")
            .exec(function (error, model) {
            return callback_(null, model);
        });
    },

    getNotiCountForUser: function(userId_, callback_){
        models.Noti
            .count({objectOwnerId: userId_, seen: false})
            .exec(function (error, model) {
            return callback_(null, model);
        });
    },

    emptyNotiForUser: function(req){
        helper.logEvent("event", "noti - emptyNotiForUser");
        if(!req.session.user){
            return "error";
        }
        models.Noti.remove({noti_user_id: session.user._id}, function(err, model){
            return "error";
        });
    },

}
exports.actions = actions
//endregion