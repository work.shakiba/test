//region convert
app.get('/api/user/convert', function(req, res, next){
    if(!req.session.user || req.session.user._id != "id1"){return;}
    var fs = require('fs');
    var file = __dirname + '/Joky/users.json';
    fs.readFile(file, 'utf8', function (err, data) {

        var data = JSON.parse(data);

        for(var i = data.length - 1; i >= 0; i--) {
            var joke = data[i];
            var id = helper.makeId();
            req.models.User.create({
                "_id": "id"+joke.user_id,
                "slug": "id"+joke.user_id,
                "date": joke.date,
                "name": joke.name,
                "thumbnail": joke.thumbnail,
            }, function(err, model){
                if(err){
                    helper.log("err", err);
                }
            })
            req.models.UserCredential.create({
                "user_id": "id"+joke.user_id,
                "email": joke.email,
                "password": joke.password,
            }, function(err, model){
                if(err){
                    helper.log("err", err);
                }
            })
        };

        return res.send("done");

    });
});

var convertToSlug = function(Text){
    return Text
        .toLowerCase()
        .replace(/[^\w ]+/g,'')
        .replace(/ +/g,'-')
}

app.get('/api/item/convert', function(req, res, next){
    if(!req.session.user || req.session.user._id != "id1"){return;}
    var fs = require('fs');
    var file = __dirname + '/Joky/jokes.json';
    fs.readFile(file, 'utf8', function (err, data) {

        data = JSON.parse(data);

        var count = 0;
        var create = function(joke){
            if(count == (data.length-1)){return res.send("done");}
            req.models.User.findOne({"_id": "id"+joke.joke_user_id}, function(error, model){
                if(!model){return;}
                req.models.User.update({"_id": "id"+joke.joke_user_id}, {$inc: {"posts": 1, "userScore": 5, "userScorePost": 20}}, function(){})
                var userId = model._id;
                var correctTime = joke.date_created.split(" ");
                var date = correctTime[0].split("-");
                var month = parseInt(date[1])
                month--;
                var date = date[0]+"-"+month+"-"+date[2]
                var fulldate = date+" "+correctTime[1];
                req.models.Item.create({
                    "_id": helper.makeId(),
                    "item_userid": userId,
                    "body": cleanText(joke.body),
                    "date": fulldate,
                    "page": "joky",
                    "like": joke.likes,
                }, function(){
                    count++;
                    create(data[count]);
                })
            })
        }
        create(data[count]);
        return res.send("done");
    });
});

app.get('/api/new-item/convert', function(req, res, next){
    if(!req.session.user || req.session.user._id != "id1"){return;}
    var fs = require('fs');
    var file = __dirname + '/Joky/new-jokes.json';
    fs.readFile(file, 'utf8', function (err, data) {

        data = JSON.parse(data);

        var count = 0;
        var create = function(joke){
            if(count == (data.length-1)){return res.send("done");}
            req.models.User.findOne({"_id": "id"+joke.joke_user_id}, function(error, model){
                if(!model){return;}
                // req.models.User.update({"_id": "id"+joke.joke_user_id}, {$inc: {"posts": 1, "userScore": 5, "userScorePost": 20}}, function(){})
                var userId = model._id;
                req.models.NewItem.create({
                    "_id": helper.makeId(),
                    "item_userid": userId,
                    "body": cleanText(joke.body),
                    "date": helper.getMysqlTime(),
                    "like": joke.likes,
                }, function(){
                    count++;
                    create(data[count]);
                })
            })
        }
        create(data[count]);
        return res.send("done");
    });
});

var cleanText = function(str_){
    var str = str_;
    str = str.split("--br--").join("<br/>");
    str = str.split("&#039;").join("\"");
    str = str.split("#039;").join("\"");
    str = str.split("&quot;").join("\"");
    str = str.split("quot;").join("\"");
    str = str.split("&gt;").join("").split("gt;").join("").split("&lt;").join("").split("lt;").join("");
    str = str.split("&amp;").join("");
    str = str.split("amp;").join("");
    return str;
}
// endregion