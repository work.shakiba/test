    var fs = require('fs-extra');
    var xss = require('xss');
    var models = require('../models');
    var CronJob = require('cron').CronJob;
    var formidable = require('formidable');
    var async = require("async")
    var gm = require('gm').subClass({ imageMagick: true });

// log
    log = function(flag_, message_, debugMode_){
        return
        if(!message_){
            message_ = "NULL";
        }
        console.log('(---'+flag_+'---) =>', message_);
    }
    exports.log = log
// log error
    exports.logError = function(flag_, message_){
        return
        //var log_file = fs.createWriteStream('/errors.log', {flags : 'a'});
        //log_file.write('(---'+flag_+'---) =>'+message_+ '\n');
        // var logFile = fs.createWriteStream("../logFile.txt", {
        //     flags: "a",
        //     encoding: "encoding",
        //     mode: 0744
        // })
        // logFile.write(flag_ + "---" + new Date().toSting + ' -> ' + message_);
         console.log('(---'+flag_+'---) =>', message_);
    }
    exports.logEvent = function(flag_, message_){
        //var log_file = fs.createWriteStream('/logs.log', {flags : 'a'});
        //log_file.write('(---'+flag_+'---) =>'+message_+ '\n');
        // var logFile = fs.createWriteStream("../logFile.txt", {
        //     flags: "a",
        //     encoding: "encoding",
        //     mode: 0744
        // })
        // logFile.write(flag_ + "---" + new Date().toSting + ' -> ' + message_);
        // console.log('(+++'+flag_+'+++) =>', message_);
    }

    event = function(package_, action_, err_, extra_){
        models.Log.create(
            {
                package: package_,
                action: action_,
                type: "event",
                error: err_,
                extra: extra_,
                date: Date.now()
            },
            function(err, model){})
    }
    exports.event = event

    error = function(package_, action_, err_, extra_){
        models.Log.create(
            {
                package: package_,
                action: action_,
                type: "error",
                error: err_,
                extra: extra_,
                date: Date.now()
            },
            function(err, model){})
    }
    exports.error = error
// ID methods
    exports.makeId = function(){
        var text = "id";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for( var i=0; i < 15; i++ ){
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return text;
    }

    exports.generateID = function(string_, length_){
        // 5 -> 1 billion
        if(!string_){string_ = "id"}
        if(!length_){length_ = 5}
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for(var i=0; i < length_-1; i++){
            string_ += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return string_;
    }

    exports.checkIDForString = function(ID_, string_){
        var stringLength = string_.length;
        var splitedID = ID_.substring(0 , stringLength);
        if(splitedID === string_){
            return true;
        }else{
            return false;
        }
    }
// time
    exports.getMysqlTime = function(){
      var d = new Date;
      return (d.getFullYear()+'-'+d.getMonth()+'-'+d.getDate()+' '+d.toTimeString().split(' ')[0])
    }
// email to
    exports.emailTo = function(config_){
        process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
        process.env.smtp_sasl_auth_enable = "yes";
        var nodemailer = require('nodemailer');
        var smtpTransport = require('nodemailer-smtp-transport');
        var transporter = nodemailer.createTransport(smtpTransport({
            host: "127.0.0.1",
            port: 25,
            auth: false
        }));
        var mailOptions = {
            from: 'support@joky.ir', // sender address
            to: config_.to, // list of receivers
            subject: config_.subject, // Subject line
            html: config_.body // html body
        };

        transporter.sendMail(mailOptions, function(error, info){
            if(error){
                console.log(error);
            }else{
                console.log('Message sent: ' + info.response);
            }
        });
    }

    exports.emailFromAdmin = function(to_, body_){
        email(to_, "سایت جوکی", "ایمیل از طرف ادمین", body_)
    }

    var email = function(to_, from_, subject_, body_){
        process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
        process.env.smtp_sasl_auth_enable = "yes";
        var nodemailer = require('nodemailer');
        var smtpTransport = require('nodemailer-smtp-transport');
        var transporter = nodemailer.createTransport(smtpTransport({
            host: "127.0.0.1",
            port: 25,
            auth: false
        }));
        var mailOptions = {
            from: from_, // sender address
            to: to_, // list of receivers
            subject: subject_, // Subject line
            html: body_ // html body
        };

        transporter.sendMail(mailOptions, function(error, info){
            if(error){
                console.log(error);
            }else{
                console.log('Message sent: ' + info.response);
            }
        });
    }

// cleanText
    /**
     * this function removes mallicous html or js codes
     * extra lines and white spaces from beginning and ending
     * @param text_
     * @returns {clean string}
     */
    exports.sanetize = function(text_){
        // this regex removes white space and lines from beginning and ending of the string
        text_ = text_.replace(/^\s+|\s+$/g, '');
        return xss(text_);
    }
// safeFromArray
    exports.safeStringFromArray = function(String, Array){
        var clean = false;
        for(i in Array){
            if(String == Array[i]){
                clean = true;
                return String;
            }
        }
        return Array[0];
    }
// safeInteger
    exports.safeInt = function(Int, Min, Max){
        var clean = false;
        var trueInt = parseInt(Int);
        if(isNaN(trueInt)){
            return Min;
        }else if(trueInt < Min){
            return Min;
        }else if(trueInt > Max){
            return Max;
        }else{
            return trueInt;
        }
    }
// parseBoolean
    /**
     * parse a variable to get a valid boolean with default if not boolean
     * @param var_
     * @param default_
     * @returns {*}
     */
    exports.parseBoolean = function(var_, default_){
        if(var_ === true || var_ == "true"){
            return true
        }else if(var_ === false || var_ == "false"){
            return false
        }else{
            return default_
        }
    }
// slug
    exports.convertToSlug = function(Text){
        if(!Text){return false;}
        return Text
        .toLowerCase()
        .replace(/[^\w ]+/g,'')
        .replace(/ +/g,'-')
    }
// detectFileChange
    exports.detectFileChange = function(array_){
        var changed = false;
        for(i in array_){
            fs.stat(array_[i], function(err, data){
                if(err){return console.log(err)}
                fs.stat(fileName_+'.js', function(err, data_){
                    if(err){console.log(err)}
                    if(data.mtime != data_.mtime){

                    }
                })
            });
        }
    }
// get newest file
    function getNewestFile(files, callback) {
        var newest = { file: files[0] };
        var checked = 0;
        fs.stat(newest.file, function(err, stats) {
            newest.mtime = stats.mtime;
            for (var i = 0; i < files.length; i++) {
                var file = files[i];
                (function(file) {
                    fs.stat(file, function(err, stats) {
                        ++checked;
                        if (stats.mtime.getTime() > newest.mtime.getTime()) {
                            newest = { file : file, mtime : stats.mtime };
                        }
                        if (checked == files.length) {
                            callback(newest);
                        }
                    });
                })(file);
            }
        });
    }

    function compressFilesIntoOutput(files_, name_){
        var UglifyJS = require("uglify-js");
        var fs = require('fs-extra');
        var uglified = UglifyJS.minify(files_);
        fs.writeFile("public/js/"+name_+".js", uglified.code, function (err){});
        fs.writeFile("public/js/minified.js", uglified.code, function (err){});
    }

    exports.getNewestFileAndGenerateOnFileChange = function(files_, callback_){
        // get the newest file
        getNewestFile(files_, function(newestFile_){
            // newest file contains the
            getNewestFile("public/js/minified.js", function(outputFile_){
                if(newestFile_.mtime != outputFile_.mtime){
                    // generate the new file
                    var name = makeId();
                    compressFilesIntoOutput(files_, name);
                    callback_(name, "needs refreshing")
                }else{
                    callback_(false, "up to date")
                }
            })
        })
    }
// generateTemplateFile
    exports.generateTemplateFile = function(fileName_){
        // watch for change
        //fs.watch(fileName_+".html", function (event, filename) {
        //    if(event === "change"){
        //        fs.readFile(fileName_+".html", 'utf-8', function(err, data){
        //            fs.writeFile("public/js/html.js", "var Templates = "+JSON.stringify(data), 'utf-8', function(err, data){
        //                console.log("html file updated by change watch")
        //            })
        //        })
        //    }
        //});
        // see if change has happened
        fs.readFile(fileName_+".js", 'utf-8', function(err, data) {
            fs.stat(fileName_ + ".html", function (err, data_) {
                if (data.mtime != data_.mtime || !data_) {
                    fs.readFile(fileName_ + '.html', 'utf-8', function (err, data) {
                        fs.writeFile("public/html/html.js", "var Templates = " + JSON.stringify(data), 'utf-8', function (err, data) {
                            console.log("html files updated by time dismatch")
                        })
                    })
                }
            })
        })
    }
// watch the changes to js files
    exports.watchJsFiles = function(files_, fileName_){
        var tasks = []
        for(var i in files_){
            //fs.watch("public/js/"+files_[i]+".js", function (event, filename) {
            //    if(event === "change") minifyJS(files_, fileName_, function(){})
            //});
            console.log("reading js file", files_[i])
            tasks.unshift(function(callback){
                // see if change has happened
                fs.readFile("public/js/"+fileName_+".js", 'utf-8', function(err, data) {
                    if(!data){
                        console.log("js file not exist, creating it")
                        minifyJS(files_, fileName_, function(){callback(null, true)})
                    }
                    fs.stat("public/js/"+files_[i]+".js", function (err, data_) {
                        if(!data) return callback(null, true)
                        if (data.mtime < data_.mtime) {
                            console.log("updated js files by time difference")
                            minifyJS(files_, fileName_, function(){callback(null, true)})
                        }else{
                            callback(null, true)
                        }
                    })

                })
            })
        }
        async.series(tasks, function(err, result){
            console.log("tasks done")
        })
    }
// minify js files
    minifyJS = function(jsFiles_, fileName_, callback_){
        var UglifyJS = require("uglify-js");
        var files = []
        for(var i in jsFiles_){
            console.log("minifying file", jsFiles_[i])
            files.unshift("public/js/"+jsFiles_[i]+".js")
        }
        var uglified = UglifyJS.minify(files);
        console.log("trying tocreate js file")
        fs.writeFile("public/js/"+fileName_+".js", uglified.code, function (err){
            if(err) console.log("err", err)
            console.log("uglified ready")
            callback_()
        });
    }
// encode
    exports.encode = function(req_, array_){
        if(req_.cookies.device != "android"){return extractMainItem(array_);}
        for(i in array_){
            var item = array_[i];
            type = "";
            if(item.item_userid){type = "posts"}else
            if(item.name){type = "users"}else
            if(item.commentor){type = "comments"}

            if(type == "posts"){
                item.body = encodeURIComponent(item.body);
                item.item_userid.name = encodeURIComponent(item.item_userid.name);
                item.item_userid.brief = encodeURIComponent(item.item_userid.brief);
            }else if(type == "users"){
                item.name = encodeURIComponent(item.name);
                item.brief = encodeURIComponent(item.brief);
            }else if(type == "comments"){
                item.body = encodeURIComponent(item.body);
                item.commentor.name = encodeURIComponent(item.commentor.name);
                item.commentor.brief = encodeURIComponent(item.commentor.brief);
            }
        }
        return extractMainItem(array_);
    }

// extract main item
    /*
    * extracts main item from array
    * used when tables have been populated
    */
    extractMainItem = function(itemArray_){
        for(i in itemArray_){
            // if is_following is available
            if(itemArray_[i].is_following){
                if(typeof(itemArray_[i].is_following) === "object"){
                    console.log("type changed");
                    itemArray_[i] = itemArray_[i].is_following;
                }else if(typeof(itemArray_[i].follow_user_id) === "object"){
                    console.log("type changed");
                    itemArray_[i] = itemArray_[i].follow_user_id;
                }
            }
        }
        // console.log(itemArray_);
        return itemArray_;
    }
// region process image
    /**
     * receives image temp URL
     * @param req
     * @param width
     * @param height
     * @param destination
     * @callback {result: Boolean,
     */
    exports.processImage = function(req, width, height, destination, callback){
        var form = new formidable.IncomingForm();
        form.parse(req, function(err, fields, files) {});
        form.on('end', function(fields, files){
            var temp_path = this.openedFiles[0].path;
            var file_name = this.openedFiles[0].name;
            var file_type = this.openedFiles[0].type.replace('image/','.');
            var temp_location = 'upload/'+destination+'/original/';
            var new_location = 'upload/' + destination+"/";
            fs.copy(temp_path, temp_location + file_name, function(err) {
                if(err){
                    if (err) return exports.log("helper", err)
                }else{
                    var name = exports.makeId() + file_type;
                    // small image
                    gm(temp_location + file_name)
                        .gravity('Center')
                        .resize(width)
                        .noProfile()
                        .write(new_location + name, function (err) {
                            if (err) return exports.log("helper", err)
                            callback(name);
                        });
                }
            });
        });
    }
// endregion
// check for swear
    exports.checkForSwear = function(body){
        var arr = [
            " کیر ",
            " کیرم ",
            " کس ",
            " کسکش ",
            " کس کش ",
            " تخمی ",
            " تخمم ",
            " ممه ",
            " ممتو ",
            " کون ",
            " جنده ",
        ]
        for (i in arr) {
            if(body.indexOf(arr[i]) != -1){
                body.replace(arr[i], "...")
            }
        }
        return body;
    }
// add cron
    exports.addCron = function(pattern_, callback_){
        /**
         * pattern -> dayofweek(0-7) month(0-12) dayofmonth(0-31) hour(0-23) minute(0-59)
         */
        new CronJob({
            cronTime: pattern_,
            onTick: function() {
                callback_
            },
            start: true,
            timeZone: 'Asia/Tehran'
        });
    }

    var adapter = function(){

        this.itemsArray = []
        this.mongoRequest = false

        this.init = function(timeout_, mongo_request_){
            var self = this
            if(timeout_){
                setInterval(function(){
                    self.itemsArray = []
                }, timeout_)
            }
            this.mongoRequest = mongo_request_
        }

        this.fetch = function(limit_, offset_, callback_){
            if(this.itemsArray.length < limit_ + offset_ || offset_ > 1000){
                this.fetchFromServer(limit_, offset_, callback_)
            }else{
                log(limit_+"-"+offset_, this.itemsArray.length)
                callback_(this.itemsArray.slice(offset_, limit_ + offset_))
            }
        }

        this.fetchFromServer = function(limit_, offset_, callback_){
            var self = this
            log("fetch from mongo")
            this.mongoRequest.skip(this.itemsArray.length).limit(100).exec(function(err, result){
                if(offset_ < 1000){
                    self.itemsArray = self.itemsArray.concat(result)
                }
                callback_(self.itemsArray.slice(offset_, limit_ + offset_))
            })
        }

    }

    exports.adapter = adapter