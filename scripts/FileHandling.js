    var fs = require('fs-extra');
    var formidable = require('formidable');
    var async = require("async")
    var helper = require('../scripts/helper.js');

// detectFileChange
    exports.detectFileChange = function(array_){
        var changed = false;
        for(i in array_){
            fs.stat(array_[i], function(err, data){
                if(err){return console.log(err)}
                fs.stat(fileName_+'.js', function(err, data_){
                    if(err){console.log(err)}
                    if(data.mtime != data_.mtime){

                    }
                })
            });
        }
    }
// get newest file
    function getNewestFile(files, callback) {
        var newest = { file: files[0] };
        var checked = 0;
        fs.stat(newest.file, function(err, stats) {
            newest.mtime = stats.mtime;
            for (var i = 0; i < files.length; i++) {
                var file = files[i];
                (function(file) {
                    fs.stat(file, function(err, stats) {
                        ++checked;
                        if (stats.mtime.getTime() > newest.mtime.getTime()) {
                            newest = { file : file, mtime : stats.mtime };
                        }
                        if (checked == files.length) {
                            callback(newest);
                        }
                    });
                })(file);
            }
        });
    }

    function compressFilesIntoOutput(files_, name_){
        var UglifyJS = require("uglify-js");
        var fs = require('fs-extra');
        var uglified = UglifyJS.minify(files_);
        fs.writeFile("public/js/"+name_+".js", uglified.code, function (err){});
        fs.writeFile("public/js/minified.js", uglified.code, function (err){});
    }

    exports.getNewestFileAndGenerateOnFileChange = function(files_, callback_){
        // get the newest file
        getNewestFile(files_, function(newestFile_){
            // newest file contains the
            getNewestFile("public/js/minified.js", function(outputFile_){
                if(newestFile_.mtime != outputFile_.mtime){
                    // generate the new file
                    var name = makeId();
                    compressFilesIntoOutput(files_, name);
                    callback_(name, "needs refreshing")
                }else{
                    callback_(false, "up to date")
                }
            })
        })
    }
// generateTemplateFile
    exports.generateTemplateFile = function(input_, output_){
        var active = false
        minify = function(){
            if(active) return
            active = true
            fs.readFile(input_, 'utf-8', function(err, data){
                fs.writeFile(output_, "var Templates = "+JSON.stringify(data), 'utf-8', function(err, data){
                    helper.log("html file updated")
                    active = false
                })
            })
        }
        // watch for change
        fs.watch(input_, function (event, filename) {
            if(event === "change"){
                minify()
            }
        });
        // see if change has happened
        fs.stat(input_, function(err, htmlFile) {
            if(err) return helper.log("error in reading html file", err)
            fs.stat(output_, function (jserr, jsFile) {
                if(jserr) return helper.log("error in reading js file", jserr)
                if (htmlFile.mtime.getTime() > jsFile.mtime.getTime()) {
                    minify()
                }
            })
        })
    }
// watch the changes to js files
    exports.watchJsFiles = function(inputs_, output_){
        var UglifyJS = require("uglify-js");
        var active = false
        minifyJS = function(callback_){
            if(active) return
            active = true
            var tasks = []
            var uglifiedCode = ""
            var counter = 0
            for(var i in inputs_){
                tasks.unshift(function(callback){
                    var uglified = UglifyJS.minify("public/js/"+inputs_[counter]+".js", {mangle: false});
                    uglifiedCode += uglified.code+" "
                    helper.log("minified", inputs_[counter])
                    counter++
                    callback(null, true)
                })
            }
            async.series(tasks, function(err, result){
                fs.writeFile("public/js/"+output_+".js",uglifiedCode, function (err){
                    console.log("uglified ready")
                    active = false
                    callback_()
                });
            })
        }
        var tasks = []
        for(var i in inputs_){
            fs.watch("public/js/"+inputs_[i]+".js", function (event, filename) {
                if(event === "change") minifyJS(function(){})
            });
            tasks.unshift(function(callback){
                // see if change has happened
                fs.stat("public/js/"+output_+".js", function(err, outputFile) {
                    if(!outputFile){
                        return minifyJS(function(){callback(null, true)})
                    }
                    fs.stat("public/js/"+inputs_[i]+".js", function (err, inputFile) {
                        if (outputFile.mtime.getTime() < inputFile.mtime.getTime()) {
                            console.log("updated js files by time difference")
                            minifyJS(function(){callback(null, true)})
                        }else{
                            callback(null, true)
                        }
                    })
                })
            })
        }
        async.series(tasks, function(err, result){
            console.log("tasks done")
        })
    }
// minify js files

// encode
    exports.encode = function(req_, array_){
        if(req_.cookies.device != "android"){return extractMainItem(array_);}
        for(i in array_){
            var item = array_[i];
            type = "";
            if(item.item_userid){type = "posts"}else
            if(item.name){type = "users"}else
            if(item.commentor){type = "comments"}

            if(type == "posts"){
                item.body = encodeURIComponent(item.body);
                item.item_userid.name = encodeURIComponent(item.item_userid.name);
                item.item_userid.brief = encodeURIComponent(item.item_userid.brief);
            }else if(type == "users"){
                item.name = encodeURIComponent(item.name);
                item.brief = encodeURIComponent(item.brief);
            }else if(type == "comments"){
                item.body = encodeURIComponent(item.body);
                item.commentor.name = encodeURIComponent(item.commentor.name);
                item.commentor.brief = encodeURIComponent(item.commentor.brief);
            }
        }
        return extractMainItem(array_);
    }

// extract main item
    /*
    * extracts main item from array
    * used when tables have been populated
    */
    extractMainItem = function(itemArray_){
        for(i in itemArray_){
            // if is_following is available
            if(itemArray_[i].is_following){
                if(typeof(itemArray_[i].is_following) === "object"){
                    console.log("type changed");
                    itemArray_[i] = itemArray_[i].is_following;
                }else if(typeof(itemArray_[i].follow_user_id) === "object"){
                    console.log("type changed");
                    itemArray_[i] = itemArray_[i].follow_user_id;
                }
            }
        }
        // console.log(itemArray_);
        return itemArray_;
    }