//region require
var helper = require('../scripts/helper.js');
var ws = require('../scripts/websocket.js');
var auth = require('../scripts/auth');
var formidable = require('formidable');
var fs = require('fs-extra');
var gm = require('gm').subClass({ imageMagick: true });
var async = require('async');
var MetaInspector = require('node-metainspector');
var sizeOf = require('image-size');
var models = require('../models');
var User = require('../scripts/User.js');
var Noti = require('../scripts/Noti.js');
var Follow = require('../scripts/Follow.js');
var Item = Item;
//endregion

// region http
module.exports = function(app) {


    /**
     * publicAPI
     * @returns result: [*]
     */
    app.get('/api/get_popular_tags', function (req, res, next){
        models.Tag
        .find({})
        .limit(25)
        .sort({count: -1})
        .exec(function(err, model){
            return res.json({result: model})
        })
    });

    /**
     * publicAPI
     * @param tagID
     * @returns result: [*]
     */
    app.get('/api/get_items_for_tag', function (req, res, next){
        if(!req.query.tagID){return res.send("invalid request");}
        models.Tag.findOne({_id: req.query.tagID}, function(err, model){
            if(err){return helper.logError("get_items_for_tag", err);}
            if(model){
                fetchModule.fetchItemByTagBody(model.body);
            }
        })
    });

}
// endregion

//region actions
var actions = {
    /**
     * extracts tags from text and returns body with no tag
     * must clean the returned body to clean the extra lines or whitespaces
     * @param rawBody_
     * @returns body with no tag
     */
    extractTagFromBody: function(rawBody_){
        // golden regex
        var regex = /(#\w+)/;
        var matchedStrings;
        // find matches
        do{
            matchedStrings = regex.exec(rawBody_);
            if (matchedStrings){
                var tagBody = matchedStrings[1];
                // process this tag
                this.processTag(tagBody);
                // remove this tag from the body
                // fixme do not replace the #tag yet, might result in breaking the string
                //rawBody_.replace(tagBody, "");
            }
        }while(matchedStrings);
        // return the body
        return rawBody_;
    },

    /**
     * checks if tag with this body exists, if not create it, if does update the count
     * @param tagBody_
     * @return null
     */
    processTag: function(tagBody_){
        var tagBody = tagBody_;
        // remove the hash from tag
        tagBody.replace("#", "");
        models.Tag.findOne({body: tagBody}, function(err, model){
            if(!model){
                this.addTag(tagBody);
            }else{
                models.Tag.update({_id: model._id}, {$inc: {count: 1, today_count: 1}}, function(err, model){
                    if(err){helper.logError("creating tag", err);}
                })
            }
        });
        return;
    },

    /**
     * add a tag to Tag collection - doesn't check for duplicate
     * but the collection is unique proof
     * @param tagBody_
     * @return null
     */
    addTag: function(tagBody_){
        models.Tag.create({body: tagBody_}, function(err, model){
            if(err){helper.logError("creating tag", err);}
        })
    }

}
//endregion

// region fetch
var fetch = {
}
// endregion