//region require
var helper = require('../scripts/helper.js');
var ws = require('../scripts/websocket.js');
var auth = require('../scripts/auth');
var formidable = require('formidable');
var fs = require('fs-extra');
var gm = require('gm').subClass({ imageMagick: true });
var async = require('async');
var MetaInspector = require('node-metainspector');
var sizeOf = require('image-size');
var models = require('../models');
var User = require('../scripts/User.js');
var Item = require('../scripts/Item.js');
var Noti = require('../scripts/Noti.js').actions;
var Follow = require('../scripts/Follow.js');
var Like = models.Like;
var Comment = models.Comment;
//endregion

// region http
exports.app = function(app) {

    app.get('/authapi/item_extra/like', function (req, res, next) {
        var itemID = req.query.id
        actions.like(req.session.user._id, itemID)
        return res.json({result: true})
    });

    app.get('/authapi/item_extra/create_comment', function (req, res, next) {
        var itemID = req.query.id;
        var body = req.query.body;
        var userID = req.session.user._id;
        actions.createComment(itemID, userID, body)
        return res.json({result: true})
    });

    app.get('/api/item_extra/get_comments', function (req, res, next) {
        var itemID = req.query.id;
        fetch.getComments(itemID, function(err, model){
            return res.json({result: model})
        })
    });

    app.get('/api/item_extra/flag_comment', function (req, res, next) {
        var commentID = req.query.id;
        helper.log("comment about to get flagged")
        Comment.update({_id: commentID}, {$inc: {flag: 1}}, function(err, model){
            if(err) helper.log("err", err)
            helper.log("comment flagged")
        })
        return res.json({result: true})
    });

    app.get('/api/item_extra/remove_comment', function (req, res, next) {
        var commentID = req.query.id;
        actions.removeComment(req.session.user._id, commentID)
        return res.json({result: true})
    });

    app.get('/authapi/item_extra/test', function (req, res, next) {
        var commentID = req.query.id;
        actions.refrshComments()
        return res.end("ok")
    });

}
// endregion

//region actions
var actions = {

    like: function(SelfId, ItemId){
        helper.logEvent("event", "itemModule - like");
        models.Like.findOne({"like_user_id": SelfId, "like_item_id": ItemId}, function(error, model){
            if(!model){
                models.Like.create({"like_user_id": SelfId, "like_item_id": ItemId});
                models.Item.update({"_id": ItemId}, {$inc: {'like': 1}}, function(){});
                models.Item.findOne({_id: ItemId}, function(error, model){
                    if(model){
                        //user.manageScore("being-liked", model.item_userid, ItemId);
                        Noti.setNotiForUser(model.item_userid, SelfId, ItemId, "like");
                    }
                });
            }else{
                models.Like.remove({"like_user_id": SelfId, "like_item_id": ItemId},function(){});
                models.Item.update({"_id": ItemId}, {$inc: {'like': -1}}, function(){});
                models.Item.findOne({_id: ItemId},function(error, model){
                    if(model){
                        //user.manageScore("being-unliked", model.item_userid, ItemId);
                    }
                });
            }
        });
        return true;
    },

    createComment: function(ItemId, SelfId, Body){
        helper.logEvent("event", "itemModule - createComment");
        body = helper.sanetize(Body);
        // get last 10 comment - check if they are identical
        models.Comment.find({"commentor": SelfId}).sort({sortDate: -1}).limit(10).exec(function(err, model){
            for(i in model){
                var comment = model[i];
                if(comment.body == Body){
                    return false;
                }
            }
            // check if item with the userID exists
            models.Item.findOne({"_id": ItemId}, function(err, model){
                if(!model){return helper.logError("comment", err);}
                // check if item allows for comment
                if(model.allow_comment == false) return
                if(Body.length < 1){return "error"}
                // insert
                models.Comment.create({"post_id": model._id, "creator": model.item_userid, "commentor": SelfId, "body": body, "date": Date.now(), "sortDate": Date.now()}, function(error, model){})
                // notify creator
                Noti.setNotiForUser(model.item_userid, SelfId, model._id, "comment");
                models.Item.update({"_id": model._id}, {$inc: {'comment': 1}}, function(){});
                //User.manageScore("being-commented", model.item_userid, model._id);
                // get the model if comment < 3 add to hot_comments
                // find the user for name and thumbnail
                models.User.findOne({"_id": SelfId}, function(err_, model_){
                    if(model.comment < 3){
                        var commentModel = {body: Body, name: model_.name, thumbnail: model_.thumbnail}
                        models.Item.update({"_id": model._id}, {$push: {hot_comments: commentModel}}, function(err, model){});
                    }
                })
            })
        })
    },

    refrshComments: function(){
        models.Item
            .find({})
            .exec(function(err, model){
                for(i in model){
                    helper.log("finding")
                    var itemModel = model[i];
                    var tasks = []
                    tasks.push(function(callback){
                        // get 10 recent comments for this model
                        models.Comment.findOne({post_id: itemModel._id}).populate("commentor", User.populate.item).exec(function(err, comment){
                            // set the new array
                            if(!comment || !comment.commentor) return callback(null, true)
                            var result =  {
                                _id: comment._id,
                                commentor:
                                { _id: comment.commentor._id,
                                    name: comment.commentor.name,
                                    thumbnail: comment.commentor.thumbnail
                                },
                                body: comment.body,
                                sortDate: comment.sortDate,
                                }
                            helper.log("updated", comment)
                            models.Item.update({_id: comment.post_id}, {comment_holder: result}, function(err, model){
                                callback(null, true)
                            })
                        })
                    })
                    async.parallel(tasks, function(err, result){
                        helper.log("done")
                    })

                }
            })
    },

    /**
     * remove a comment of it's the owner of post or the owner of the comment
     * @param userID_
     * @param commentID_
     */
    removeComment: function(userID_, commentID_){
        Comment.findOne({_id: commentID_}, function(err, model){
            if(model && (model.commentor == userID_ || model.creator == userID_)){
                Comment.remove({_id: commentID_}, function(){})
                Item.actions.setItemAs(model.post_id, "$inc", {comment: -1})
            }
        })
    },

}
//endregion

// region fetch
var fetch = {
    getComments: function(ItemId, Callback){
        Comment.find({"post_id": ItemId}).populate('commentor').sort({"sortDate": 1}).exec(function(error, model){
            Callback(null, model);
        });
    },
}
// endregion