//region require
var helper = require('../scripts/helper.js');
var async = require('async');
var models = require('../models');
var ws = require('../scripts/websocket.js');
//endregion
//region inbox requests
module.exports = function(app){

    /**
     * authAPI
     * @offset
     * @returns result: [*] * 25
     */
    app.get('/authAPI/get_inboxes', function(req, res, next){
        // find userID
        var userId = req.session.user._id;
        // get offset
        var offset = helper.safeInt(req.query.offset, 0, 999);
        // find inbox by UserID
        models.UserInbox
            .find({"self_id": userId})
            .sort({"date": -1})
            .limit(25)
            .populate("target_id")
            .skip(offset)
            .exec(function(err, model){
                if(err){return helper.logError("getInboxForUser", err);}
                return res.json({result: model});
            })
    });

    /**
     * used to get the inbox, when it's not avaliable on the client
     * returns inbox with userModel and messages if exists
     * else, creates the inboxs
     * @inboxID
     * @targetUserID
     * @returns inbox
     */
    app.get('/authAPI/request_inbox', function(req, res, next){
        var selfID = req.session.user._id;
        var targetUserID = req.query.targetUserID;
        var userInboxID = req.query.userInboxID;
        if(!userInboxID && !targetUserID){return res.json({err: true});}
        // setup the find param
        if(userInboxID){
            var searchParam = {_id: userInboxID};
        }else if(targetUserID){
            var searchParam = {self_id: selfID, target_id: targetUserID}
        }
        // find the model
        models.UserInbox.findOne(searchParam).populate("target_id").exec(function(err, model){
            if(model){
                helper.log("inbox found returning valid userInbox")
                return res.json({inbox: model});
            }else{
                helper.log("inbox doesn't exist returning fake user model")
                models.User.findOne({_id: targetUserID}, function(err, model){
                    return res.json({inbox: {target_id: model}});
                });
            }
        })
    });

    /**
    *@inboxID
    * @returns array {} * 50
    */
    app.get('/authAPI/get_messages', function(req, res, next){
        messagesModule.getMessages(req.query.inboxID, function(err, model){
            return res.json({err: err, model: model});
        })
    });

    /**
     * authAPI
     * @name String
     * @thumbnail String
     * @returns {err: {*}, result: true}
     */
    app.get('/authAPI/create_group_inbox', function(req, res, next){
        // vars
        var adminUserID = req.session.user._id;
        var type = "group";
        var name = req.query.name;
        var thumbnail = req.query.thumbnail;
        // create the groupe
        models.GroupInbox.create({admin: adminUserID, name: name, thumbnail: thumbnail, members: [adminUserID]}, function(err, model){
            return res.json({err: model});
        });
    });

    /**
     * @inboxID
     * @result true
     */
    app.get('/authAPI/enter_group_inbox', function(req, res, next){
        // get vars
        var userID = req.session.user._id;
        var inboxID = req.query.inboxID;
        // check if inbox exists and is not stopped
        models.GroupInbox.findOne({_id: inboxID}, function(err, model){
            if(!model){return res.send("!Invalid request!")}
            // check if record exists
            models.UserInbox.findOne({inbox_id: inboxID, user_id: userID}, function(err, model){
                if(!model){
                    models.UserInbox.create({inbox_id: inboxID, user_id: userID}, function(err, model){})
                }
            });
            // add user to inbox memebers
            models.GroupInbox.update({_id: inboxID}, {$push: {members: userID}}, function(err, model){});
        })
        // result
        return res.json({result: true})
    });

    /**
     * inboxID: safe String
     * result: true
     */
    app.get('/authAPI/exit_group_inbox', function(req, res, next){
        // get vars
        var userID = req.session.user._id;
        var inboxID = req.query.inboxID;
        // remove 
        models.UserInbox.remove({inbox_id: inboxID, user_id: userID}, function(err, model){});
        //remove user from inbox
        models.GroupInbox.update({_id: inboxID}, {$pull: {members: userID}}, function(err, model){});
        // result
        return res.json({result: true})
    });

    /**
     * inboxID: safe String
     * result: true
     */
    app.get('/authAPI/delete_group_inbox', function(req, res, next){
        // get vars
        var userID = req.session.user._id;
        var inboxID = req.query.inboxID;
        // check if user is admin
        models.GroupInbox.findOne({_id: inboxID}, function(err, model){
            if(userID === model.admin){
                // remove the groupInbox 
                models.GroupInbox.remove({_id: inboxID}, function(err, model){})
                // remove the USerIinbox
                models.UserInbox.find({inbox_id: inboxID}, function(err, model){})
            }
        })
    });

    /**
     * this process consists of adding user to the blocked users in inbox
     * inboxID: safe String
     */
    app.get('/authAPI/block_user_from_group_inbox', function(req, res, next){
        // get vars
        var userID = req.session.user._id;
        var maliciousUserId = req.query.muser;
        var inboxID = req.query.inboxID;
        // check if user is admin
        models.Inbox.findOne({_id: inboxID}, function(err, model){
            if(userID === model.admin){
                // remove user from members and add it to blocked users 
                models.GroupInbox.update({_id: inboxID}, {$pull: {memebers: maliciousUserId}, $push: {blocked_memebers: maliciousUserId}}, function(err, model){})
                // make the userInbox as blocked
                models.UserInbox.update({self_id: maliciousUserId, inbox_id: inboxID}, {blocked: true}, function(err, model){})
            }
        })
    });

    /**
    * authAPI
    * removes the message by ID
    * @messageID
    * @inboxID
    */
    app.get('/authAPI/remove_message_from_group_inbox', function(req, res, next){
        var messageID = req.query.messageID;
        var inboxID = req.query.inboxID;
        var adminID = req.session.user._id;
        // check if user is admin
        models.Inbox.findOne({_id: inboxID, admin: adminID}, function(err, model){
            if(model){
                // remove all the messages
                models.Message.remove({_id: messageID, inbox_id: inboxID}, function(err, model){});
            }
        })
        return res.json({result: true})
    });

    /**
     * @body
     * @inboxID(only for group chat)
     */
    app.get('/authAPI/send_message', function (req, res, next){
        // local vars
        var inboxNotExists = false;
        // client vars
        var body = req.query.body;

        var selfUserID = req.session.user._id;
        var targetUserID = req.query.targetUserID;
        var inboxID = req.query.inboxID;

        var relationKeyOne = selfUserID+"-"+targetUserID;
        var relationKeyTwo = targetUserID+"-"+selfUserID;

        // check if inboxID is for groups
        if(helper.checkIDForString("group")){
            return messagesModule.groupSendFunction(inboxID, selfUserID, body);
        }

        // check if inbox exists then User inbox exists too
        models.Inbox.findOne({$or: [{relation_key: relationKeyOne}, {relation_key: relationKeyTwo}]}, function(err, model){
            // inbox doesn't exist
            if(!model){
                helper.log("model not exists by relation - creating the inbox and userInboxes");
                inboxID = helper.makeId();
                messagesModule.createTwoWayInboxAndUserInbox(inboxID, selfUserID, targetUserID, body);
            }else{
                // update Inbox
                helper.log("model exists by relation - updating the inbox");
                messagesModule.updateTwoWayInbox(model._id, selfUserID, targetUserID, body);
            }
        });
        return res.json({result: true})
    })

};
//endregion

var messagesModule = {

    groupSendFunction: function(inboxID_, selfID_, body_, callback_){
        // check if inboxID is avaliable
        models.GroupInbox.findOne({_id: inboxID_}, function(err, model){
            if(!model){return callback_("0", null);}
            if(model){
                // check if user is in the members
                if(model.members.indexOf(selfID_) != -1){
                    // 
                    messagesModule.createAndSendMessageToUserArray(inboxID_, selfID_, inboxID_, body_, models.members);
                }
            }
        })
    },

    createTwoWayInboxAndUserInbox: function(inboxID, sender, reciever, body){
        async.parallel(
            [
                function(callback){
                    models.Inbox.create({_id: inboxID, relation_key: sender+"-"+reciever, last_message: body, date: Date.now(), members: [sender, reciever]}, function(err, model){
                        if(err){helper.log(err)}
                        callback(null, true);
                    });
                },
                function(callback){
                    models.UserInbox.create({self_id: sender, target_id: reciever, inbox_id: inboxID}, function(err, model){
                        if(err){helper.log(err)}
                        callback(null, true);
                    });
                },
                function(callback){
                    models.UserInbox.create({self_id: reciever, target_id: sender, inbox_id: inboxID}, function(err, model){
                        if(err){helper.log(err)}
                        callback(null, true);
                    });
                },
            ],
            // send the result
            function(err, results){
                messagesModule.createAndSendMessageToUserArray(inboxID, sender, reciever, body, [sender, reciever]);
            }
        );
    },

    updateTwoWayInbox: function(inboxID, sender, reciever, body, memberArray){
        models.Inbox.update({_id: inboxID}, {last_message: body, date: Date.now()}, function(err, model){})
        messagesModule.createAndSendMessageToUserArray(inboxID, sender, reciever, body, [sender, reciever]);
    },

    createAndSendMessageToUserArray: function(inboxID, sender, reciever, body, memberArray){
        // create the model
        models.Message.create({sender: sender, reciever: reciever, body: body, inbox_id: inboxID}, function(err, messageModel){
            // then -> send messages to the member array
            for(i in memberArray){
                var userID = memberArray[i];
                if(userId != sender){
                    ws.sendMessageToUser(userID, messageModel);
                }
            }
        })
    },

    getMessages: function(inboxID_, callback_){
        // get messages
        models.Message
            .find({inbox_id: inboxID_})
            .limit(50)
            .skip(0)
            .exec(function(err, model){
                callback_(err, model);
            })
    },

    getInboxModel: function(inboxID_, callback_){
        models.Inbox
            .findOne({_id: inboxID_})
            .exec(function(err, model){
                if(err){
                    callback_(err, null)
                    helper.logError("messagesModule getInboxModel", err);
                    return;
                }
                // check if target is user or group
                if(model){
                    var inboxModel = {};
                    if(model.targe_id){
                        inboxModel.name = model.targe_id.name;
                    }
                    callback_(null, model);
                }else{
                    return callback_("no result found", null);
                }
            })
    },

    createUserInbox: function(selfID, targetID, callback){
        // create
        models.UserInbox
            .create()
            .exec(function(err, model){

            })
    }

}

