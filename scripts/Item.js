﻿//region require
var helper = require('../scripts/helper.js');
var ws = require('../scripts/websocket.js');
var auth = require('../scripts/auth');
var formidable = require('formidable');
var fs = require('fs-extra');
var gm = require('gm').subClass({ imageMagick: true });
var CronJob = require('cron').CronJob;
var async = require('async');
var sizeOf = require('image-size');
var models = require('../models');
var User = require('../scripts/User.js');
var Noti = require('../scripts/Noti.js').actions;
var Follow = require('../scripts/Follow.js');
var Item = models.Item;
//endregion

// region http
exports.app = function(app) {

    app.get('/api/item/fetch', function (req, res, next) {
        var userID = (req.session.user ? req.session.user._id : false);
        // create a fetchObj
        var fetchObject = {
            type: req.query.type,
            skip: req.query.offset,
            limit: req.query.limit,
            term: req.query.term,
            userID: userID,
        }
        fetch.multiPlexer(fetchObject, function(err, model){
            return res.json({result: model})
        })
    })

    /**
     * @param term
     * @param limit
     * @param offset
     */
    app.get('/api/item/search', function (req, res, next) {
        var term = req.query.term;
        if (term.indexOf("جنت") != -1) {
            term += "111"
        }
        var skip = helper.safeInt(req.query.offset, 0, 9999);
        var limit = helper.safeInt(req.query.limit, 10, 50);
        // begin the search
        Item
            .find({$text: {$search: term}})
            .limit(limit)
            .skip(skip)
            .populate("item_userid", User.populate.item)
            .exec(function(error, model){
                return res.json({result: model})
            });
    });

    app.get('/authapi/item/flag', function (req, res, next) {
        flag.flagItem(req.query.id, req.session.user._id)
        return res.json({result: true})
    })

    app.get('/api/auth/delete_item', function (req, res, next) {
        actions.deleteItem(req.session.user._id, req.query.item_id)
        return res.json({result: true})
    })


    app.get('/authapi/item/disallow_comment', function (req, res, next) {
        var itemID = req.query.id
        var allow = helper.parseBoolean(req.query.allow, true)
        actions.toggleAllowComment(req.session.user._id, itemID, allow)
        return res.json({result: true})
    });

    app.get('/api/item/get_item', function (req, res, next) {
        var itemID = req.query.id
        fetch.getSingleItem(itemID, function(err, model){
            return res.json({result: model})
        })
    });

    app.get('/api/stat', function (req, res, next) {
        fetch.getUserWhoHavePostedItemInPast24Hours()
        return res.json({ok: true})
    });

}
// endregion

//region actions
var actions = {
    /**
     * selects the trending items and filter the distinct
     * only for items in the last 48 hours
     */
    distinctFilteringForControversial: function(){
        Item
            .find({safe: 1, comment: {$gt: 7}, sort_date: {$gt: Date.now() - 24 * 60 * 60 * 1000}})
            .sort({sort_date: -1})
            .populate("item_userid", User.populate.item)
            .exec(function(err, result){
                actions.distinctFiltering(result, 4)
            })
    },

    /**
     * selects the trending items and filter the distinct
     * only for items in the last 24 hours
     */
    distinctFilteringForTrending: function(){
        Item
            .find({safe: 1, like: {$gt: 3, $lt: 7}, sort_date: {$gt: Date.now() - 24 * 60 * 60 * 1000}})
            .sort({sort_date: -1})
            .populate("item_userid", User.populate.item)
            .exec(function(err, result){
                actions.distinctFiltering(result, 2)
            })
    },

    /**
     * selects the popular items and filter the distinct
     */
    distinctFilteringForPopular: function(){
        Item
            .find({safe: 1, like: {$gt: 7}, sort_date: {$gt: Date.now() - 24 * 60 * 60 * 1000}})
            .sort({sort_date: -1})
            .populate("item_userid", User.populate.item)
            .exec(function(err, result){
                actions.distinctFiltering(result, 3)
            })
    },

    /**
     * selects the new items and filter the distinct
     */
    distinctFilteringForNew: function(){
        Item
            .find({safe: 1, like: {$gt: 0, $lt: 4}, sort_date: {$gt: Date.now() - 24 * 60 * 60 * 1000}})
            .sort({sort_date: -1})
            .populate("item_userid", User.populate.item)
            .exec(function(err, result){
                actions.distinctFiltering(result, 1)
            })
    },

    /**
     * marks the distincst items based in item_userid
     * if item_userid has been repeated in the "5" preceding or proceeding items, it wont be distinct
     * @param itemArray
     */
    distinctFiltering: function(itemArray, distinct){
        while(itemArray.length) {
            var currentCLuster = itemArray.splice(0,15)
            var currentClusterUserIDs = {}
            for(var i in currentCLuster){
                // get the item
                var item = currentCLuster[i]
                // if(!currentClusterUserIDs[item.item_userid] || currentClusterUserIDs[item.item_userid].like >= item.like){
                //     currentClusterUserIDs[item.item_userid] = item
                // }
                if(!currentClusterUserIDs[item.item_userid] || item.attach.type == "none"){
                    currentClusterUserIDs[item.item_userid] = item
                }
            }
            var count = 0
            for(var l in currentClusterUserIDs) count++
            this.applyDistinctFiltering(currentClusterUserIDs, distinct)
            currentClusterUserIDs = {}
        }
        itemArray = null
    },

    applyDistinctFiltering: function(itemObject, distinct){
        for(var i in itemObject){
            actions.setItemAs(itemObject[i]._id, "distinct", distinct);
        }
    },

    /**
     * see what items are safe to see and what are not
     * items that are added in the last 24 hours
     */
    cronSafeItems: function() {
        Item.find({sort_date: {$gt: Date.now() - 24 * 60 * 60 * 1000}, safe: {$ne: -1}}).exec(function (err, model) {
            for (var i in model) {
                if (model[i].flag < 1 && model[i].safe != 1) {
                    actions.setItemAs(model[i]._id, "safe", 1);
                }else if(model[i].flag > 0 && model[i].safe != 0){
                    actions.setItemAs(model[i]._id, "safe", 0);
                }
            }
        })
    },

    setItemAs: function(itemID, key, value){
        var change = {}
        change[key] = value;
        Item.update({_id: itemID}, change, function(err, model){
        })
    },

    deleteItem: function(SelfId, ItemId){
        models.Item.findOne({_id: ItemId}, function(error, model){
            if(model.item_userid == SelfId){
                models.Item.update({_id: ItemId}, {safe: -1}, function(error, model){});
                User.actions.manageUserInfo(SelfId, "post-deleted")
            }
        })
    },


    /**
     * if set to false, disable the item comment field
     * if set to true, enable the allow_comment field of item
     */
    toggleAllowComment: function(userID_, itemID_, allow_) {
        // check if item owner is correct, if true
        Item.findOne({_id: itemID_}, function(err, model){
            if(model && model.item_userid == userID_){
                if(allow_){
                    Item.update({_id: itemID_}, {allow_comment: true}, function(){})
                }else{
                    Item.update({_id: itemID_}, {allow_comment: false}, function(){})
                }
            }
        })
    }

}
exports.actions = actions
//endregion

// region flag
var flag = {

    /**
     * resets weight 24 on items added in past 23-24 hours
     * for performance purposes, this only works on items that have been added during the last 23-24 hours
     * after this time, the reset 24 doesn't work
     * runs ever hour
     */
    cronResetWeight24: function(){
        // move all the weight_all to weight_24
        // only for items that have been reset in the last 24 hour
        var last23 = Date.now() - 23*60*60*1000;
        var last24 = Date.now() - 24*60*60*1000;
        Item.update({date: {$gt: last24, $lt: last23}}, {$set: {flag_24: 0}}, {multi: true}).limit(10000).exec()
    },

    /**
     * experimental function
     * checks item with high flag_24, if more than 10, set them as dirty
     * runs every 15 minute
     */
    cronCheckForFlagWithHigh24: function(){
        Item.find({flag_24: {$gt: 10}}, function(err, model){
            for(var i in model){
                flag.setItemAsDirty(model[i]._id)
            }
        })
    },

    /**
     * setting item as dirty results in the following actions:
     * item will be set as blocked: true, safe: false
     * owner user will be dirty up
     * users who have flagged will be notified
     * @param itemID
     */
    setItemAsDirty: function(itemID){
        // find the model
        Item.findOne({_id: itemID}, function(err, model){
            if(model){
                // set it as blocked
                Item.update({_id: itemID}, {safe: -1}, function(err, model){});
                User.actions.manageUserInfo(model.item_userid, "post-deleted")
                // dirty up the user
                User.flag.dirtyUpProfile(model.item_userid, function(err, model){});
                // notify the user
                Noti.setNotiForUser(model.item_userid, null, itemID, "post_blocked");
                // notify the flaggers
                for(var i=0; i<model.flagged_by.length; i++){
                    var userID = model.flagged_by[i]
                    Noti.setNotiForUser(userID, null, itemID, "flag_post_blocked");
                }
            }
        })
    },

    /**
     * reset the flag and flag_24 fields
     * @param itemID
     */
    setItemAsCLean: function(itemID, safe_){
        // find the model
        Item.update({_id: itemID}, {flagged_by: [], flag: 0, flag_24: 0, safe: safe_}, function(err, model){
            if(err) return helper.logError("setItemAsCLean", err)
        })
    },

    /**
     * update flag info
     * @param itemID
     */
    flagItem: function (itemID, userID){
        var asyncFunc = [
            function(callback){
                Item.findOne({_id: itemID}, {flagged_by: 1}).exec(function(err, model){
                    callback(null, model)
                })
            },
            function(callback){
                User.actions.getUserCredentialModel(userID, function(err, model){
                    callback(null, model)
                })
            }
        ]
        async.parallel(asyncFunc, function(err, results){


            if(results[1] && results[1].user_flag_level){
                var user_flag_level = results[1].user_flag_level
            }else{
                var user_flag_level = 1
            }

            if(userID == "id1"){
                flag.setItemAsDirty(itemID)
            }else{

                if(results[0].flagged_by.indexOf(userID) == -1){
                    var changes = {
                        $push: {flagged_by: userID},
                        $inc: {flag: user_flag_level, flag_24: user_flag_level},
                    }
                    Item.update({_id: itemID}, changes, function(err, model){
                        if(err) return helper.logError("flagItem", err)
                    })
                }
            }

        })
    }

}
exports.flag = flag;
// endregion

// region fetch
var fetch = {
    trendingAdapter: false,
    newAdapter: false,
    multiPlexer: function(fetchObj_, callback_){
        // safe the variables
        var limit = helper.safeInt(fetchObj_.limit, 10, 50);
        var skip = helper.safeInt(fetchObj_.skip, 0, 9999);
        var type = helper.safeStringFromArray(fetchObj_.type, ["feeds", "new", "trending", "popular", "you", "user", "users", "search-user", "search-item", "top-users", "controversial"]);
        var selfID = fetchObj_.userID;
        var targetID = fetchObj_.targetID;
        var term = fetchObj_.term;
        // multiplex
        switch(type){
            case "feeds":
                if(!selfID) return callback_(true, null);
                Follow.fetch.getFollowingData(selfID, 999, 0, function(err, model){
                    var userList = [selfID]
                    for (var i = model.length - 1; i >= 0; i--) {
                        userList.push(model[i]);
                    };
                    var where = {safe: {$ne: -1}, item_userid: {$in: userList}}
                    Item
                        .find(where)
                        .limit(limit)
                        .skip(skip)
                        .sort({'sort_date':'desc'})
                        .populate('item_userid', User.populate.item)
                        .exec(function(err, model){
                            return callback_(null, model);
                        });
                })
                break;
        /**
         * safe : -1
         * like > 5
         * like < 10
         */
            case "trending":
                Item
                    .find({safe: {$ne: -1}, distinct: 2})
                    .sort({sort_date: -1})
                    .limit(limit)
                    .skip(skip)
                    .populate("item_userid", User.populate.item)
                    .exec(function(err, result){
                        return callback_(null, result);
                    })
                break;
            case "new":
                Item
                    .find({safe: {$ne: -1}, distinct: 1})
                    .sort({sort_date: -1})
                    .limit(limit)
                    .skip(skip)
                    .populate("item_userid", User.populate.item)
                    .exec(function(err, result){
                        return callback_(null, result);
                    })
                break;
            case "popular":
                Item
                    .find({safe: {$ne: -1}, distinct: 3})
                    .sort({sort_date: -1})
                    .limit(limit)
                    .skip(skip)
                    .populate("item_userid", User.populate.item)
                    .exec(function(err, result){
                        return callback_(null, result);
                    })
                break;
            case "controversial":
                Item
                    .find({safe: {$ne: -1}, distinct: 4})
                    .sort({sort_date: -1})
                    .limit(limit)
                    .skip(skip)
                    .populate("item_userid", User.populate.item)
                    .exec(function(err, result){
                        return callback_(null, result);
                    })
                break;
            case "you":
                if(!selfID) return callback_(true, null);
                result = Item
                    .find({safe: {$ne: -1}, item_userid: selfID})
                    .limit(limit)
                    .skip(skip)
                    .sort({sort_date: -1})
                    .populate("item_userid", User.populate.item)
                    .exec(function(err, model){
                        return callback_(null, model);
                    })
                break;
            case "user":
                if(!targetID) return callback_(true, "false")
                result = Item
                    .find({safe: {$ne: -1}, item_userid: targetID})
                    .limit(limit)
                    .skip(skip)
                    .sort({sort_date: -1})
                    .populate("item_userid", User.populate.item)
                    .exec(function(err, model){
                        return callback_(null, model);
                    })
                break;
            case "users":
                User.fetch.fetchRisingUsers(limit, skip, function(err, model){
                    return callback_(null, model);
                })
                break;
            case "top-users":
                User.fetch.fetchTopUsers(limit, skip, function(err, model){
                    return callback_(null, model);
                })
                break;
            case "search-item":
                if(!term){return callback_(null, "")}
                Item
                    .find({safe: {$ne: -1}, $text: {$search: term}})
                    .limit(limit)
                    .skip(skip)
                    .populate("item_userid", User.populate.item)
                    .exec(function(error, model){
                        return callback_(null, model);
                    });
                break;
            case "search-user":
                if(!term){return callback_(null, "")}
                User.fetch.search(term, limit, skip, function(err, model){
                    return callback_(null, model);
                })
                break;
            default :
                return callback_(null, null)
                break;
        }
    },

    /**
     * this method only can access hidden items -> items safe: -1
     * so that notifications would still work
    */
    getSingleItem: function (itemID_, callback_) {
        var bannedIDs = ["idadicw2e1OWpijfg", "idsmnf692iurJpAnB", "idTK8wmfm3yH", "idiMEaZoEPMu", "id9EqxPBVizx", "idDH7eH3yXzs", "idYBPj4D9C5K", "idBTHJVAFIu9", "idQmZf4GGomIC7Bbi", "idAdbVEoRf8C", "id1rLEYhTSpi9B3nS", "ide8NN3Ca2xz", "idbUwa7MmHVr", "idwJhJY7uGoe", "idqW1KWQwaUo", "id6YokJSFADK", "idmTeq1SHFqU", "idxPPWNOZrM0", "idyyHVFatWHA", "idtkaNhAE0al", "id1WIUdpCxP8", "id8RxNrkLxFi", "idJI3Z4fp38W", "id8MqmvJkxT1", "idYrBfeUcnW5", "id52f6sXkg3r", "idKCcsFqDnuR", "idYzBQWRY0YI", "idczRE3ygRcz", "idDlKorFU7K1", "idw7zFx8C0re", "id1kYWlmtjn3", "id0Nu8WK8eD0", "idbRb1qvIOLm", "idCRN7RnTUjYssRKZ", "id3UZH11snANFCTta", "idovjGGDxrjm", "idgD8DQGSsES", "id2sQyXfXq6A", "idV0Atbq8AKj", "idyPzhryRO3y", "id2BcCetwhyr", "idQDYBPPljTc", "idFKQkdA6UbE", "idSNKtcqPlOD", "idgFXRGQW41h", "idtg5k20zeMR", "idWRmpWEfLrN", "idRaEBvb0bmn", "idF33sSOAVnl", "idcwkjS4Ycc4", "idt6O3dNDUwe", "idBuEJ8DihWN", "idgpRfzaX4FNJwtgI", "idfKR58Yo3iu", "idfiv8xtnXdv", "idmgobyNIBM9Bp8hn", "idCxQqRKKXQM", "id7ni2sRo5U4", "idQsBAmkIel1S6dEH", "idx8w9CqlYkP", "idkvXSwR1ZEU", "idujlOTxcnfw", "id2IUokWMuX0", "iddCVruVsRCt", "id9czpBRhgrtxLllG", "idljzuoj0W4s", "idwzpzSapMgnHn8JQ", "id7LnOUjf6sE", "idZXgLxDlJBK", "idpFBjd4tHPQ", "idJJtK2hnV86", "idNyzzLJKBBP", "idUBYtuq1USJ", "id58NefQLaWs", "idgSBj4Wwo30dG7Sc", "idpNLsBuHg1u", "idEhASbAttrt", "idKy709lcSUD", "idiWeg4Pap7n2WNoN", "idvvcZ87VSFO", "id5iCCNKC3qJ", "ida1Bn5fKeQt", "idPZM4lrJPku5MVkl", "idHlEjtJ70ZS", "idvu6ZKwitTz", "idOE8WdPKNe1", "idtH8FxZqqch", "idOn0Cqnwckd", "id1qlBPKYyrF", "idFBRe8tiTCt", "idtNDR1UmcTc", "idkBJwMM2jem", "idHRTquybT4E", "idUb4jhcYEnr", "idlBDEeE5OnI", "idRhem9WTHOz", "id5dvRLDclmW", "idPgKzRejjAf", "id3ctNTOZA8M", "idRB2ZsPkION", "id6EQznveqqv", "id8GV87WmuWhZkr7b", "idVbgfwI9ua4", "idqaW2usRgg8", "idgb5IcsQPOD", "idzdbPwtyiCa", "idsJJqSS94RB", "idt9YRB0fYCI", "idh2btdirFVA", "idtQxJ8kogb9", "idR4Tg6EMeF0", "idprPojtWEkJ", "idwcKVnrvk10nQGfC", "idWIBNpHhjZd", "idccMoiYHAmOGenPF", "idFiJkWcH2cF", "idsXrlbuBIHcMZeRv", "id8QlZMMD4Go", "idWmVYQemjVORMUYg", "idWUA1aEtvam", "idLy4ILt9yqk", "idncj7NVuKV9", "idaeBM0thpXv", "idYdjMp7A1d8", "idPbJDrPYUsk", "idtiUPEAtKjI", "idYT3QUwlVGj", "iduor2weoqHWPoKts", "id6hPkHcuquf", "idkYdafKTK6k", "idyLK51cFCP0", "id7FNtLDrsby", "idz4PjS5HCOQ", "idu0DPzZJAJn", "idjCPPYuOVVs", "idapGfM4Mc2g", "idyOZVw4IxV8", "idNsXAaBauPb", "idMKs9alxaSx", "idHmp1N3ZN0d", "idQsb5PIWdR0vZz7A", "id7p2bLANfxk", "ide4dJH3S3tu", "idOztPReWFbO", "idvcv1tdQ7pS", "idL1xTiNvxMS", "idCyslt3VQWV", "idoAnHplMeFujH0ua", "idgm05F6RUNh", "idkC9xZuhEid", "idTAiOUQGdSOhoWD8", "idutgcRePyFlOPUgI", "id5Jr50447Db", "idLeMPp4WzUC", "idRAKzI7uAVi", "id8oEtmIjhEe", "id8RTKvF1Y3V", "id9i9K8ZTKvh", "id9nbdInwime", "idj7eeaRfCrA", "idisCKef5hen", "id6L48SfGrWr", "idWnuM4mCgTF", "ide7mt9ZhwFg", "id1vrqMGLtxj", "idpcWBNfjYYf", "idOEMlYscfJj", "idViDxU769sE", "idkiQrcQpmr0", "idZMjPbJtUU5", "idMtfN5UN1dZ", "idXLSE4VLbwK", "idfX1TVm7XMCJomha", "id1MvFtPayYU", "idYqkpEVP29j", "idSRPwTqThqr", "idCcLu8srmwR", "idKD25EKBZBs", "id63Ubb8FPhJ", "idztIH4n4NcR", "idl2o9CEXKlz", "idPV9WNBEFyD", "idTgZISzTYDvXTqhe", "idB5zbCYa05x", "idgrTpdiSEZe", "idd6N4XysaEc", "id038pEKy5Yh", "idat7EssVzij", "idrMWhBKSBiQ", "idTDuy8bKqtK", "id3VVXsor1vb", "idN0WuQEr96e", "id2DcSJqB0hk", "idPUA8LIzXFq", "ide7EhYRszmwfZb2y", "idoQJKVV9GUE", "id0GcvhhP0Mr", "idXWR8NX1Kb6", "id3g4BP0NLzn", "idaAbS0ABG41", "idb8tQnfATXv", "idBg4uDOtJDc", "ido57sQJwh3m", "idxODKHU9w2x"]
        if (bannedIDs.indexOf(itemID_) != -1) {
            callback_(null, null)
        }
        Item
        .findOne({_id: itemID_})
        .populate("item_userid", User.populate.item)
        .exec(function(err, model){
            callback_(null, model)
        })
    },

    getUserWhoHavePostedItemInPast24Hours: function(){
        Item
        .find({sort_date: {$gt: Date.now() - 24 * 3600 * 1000 }})
        .exec(function(err, model){
            users = {}
            usersCount = 0
            itemCount = 0
            for(var i in model){
                itemCount++
                if(!users[model[i]["item_userid"]]){
                    usersCount++
                    users[model[i]["item_userid"]] = true
                }
            }
            var conf = {to: "work.shakiba@gmail.com", subject: "users", body: "usersCount = "+usersCount+"  itemCount="+itemCount}
            helper.emailTo(conf)
            model = null
        })
    }

}
exports.fetch = fetch;
// endregion

//region cronJobs

// reset weight24 - every hour
new CronJob({
    cronTime: "00 00 * * * *",
    onTick: function() {
        flag.cronResetWeight24();
    },
    start: true,
    timeZone: 'Asia/Tehran'
});


/**
 * every 15 minutes
 */
new CronJob({
    cronTime: "00 */25 * * * *",
    onTick: function() {
        actions.cronSafeItems();
    },
    start: true,
    timeZone: 'Asia/Tehran'
});

/**
 * every 15 minutes
 */
new CronJob({
    cronTime: "00 */45 * * * *",
    onTick: function() {
        actions.distinctFilteringForTrending();
    },
    start: true,
    timeZone: 'Asia/Tehran'
});


/**
 * every 30 minutes
 */
new CronJob({
    cronTime: "00 */30 * * * *",
    onTick: function() {
        actions.distinctFilteringForPopular();
    },
    start: true,
    timeZone: 'Asia/Tehran'
});

/**
 * every 15 minutes
 */
new CronJob({
    cronTime: "00 */30 * * * *",
    onTick: function() {
        actions.distinctFilteringForNew();
    },
    start: true,
    timeZone: 'Asia/Tehran'
});

/**
 * every 15 minutes
 */
new CronJob({
    cronTime: "00 00 */12 * * *",
    onTick: function() {
        actions.distinctFilteringForControversial();
    },
    start: true,
    timeZone: 'Asia/Tehran'
});

 new CronJob({
     cronTime: "00 00 * * * *",
     onTick: function() {
         fetch.restartServer();
     },
     start: true,
     timeZone: 'Asia/Tehran'
 });

//endregion