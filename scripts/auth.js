	var helper = require('../scripts/helper.js');
    var async = require('async');
    var requestIp = require('request-ip');
    var sessionStore = require('../app').sessionStore;

module.exports = function(app) {
// validate user
    app.all('/*', function(req, res, next) {
    	// filter images
    	if(req.path.indexOf("/static/") != -1 || req.path.indexOf("/upload/") != -1){
            return next()
        }
        // validate user token
        if(!req.session.user && req.cookies.email && req.cookies.email != "false"){
            login(req, res, req.cookies.email, req.cookies.password, false, function(errCode){
            	return next();
            });
        }else{
            return next();
        }
    });
// user authentication
    app.get('/api/auth*', function(req, res, next){
        if(!req.session.user){
            return res.send("Access Denied");
        }else{
            return next();
        }
    });
    app.get('/api/auth/get-session-id', function(req, res, next){
        return res.json({result: req.sessionID})
    });
// get session id

// signup
    app.get('/api/signup', function (req, res, next){
        var userName = req.query.name;
        var email = req.query.email;
        var pass = req.query.password;
        // if empty
        // check for duplicates
        async.series(
            [
                // check if email exists
                function(callback){
                    var emailLower = email.toLowerCase();
                    req.models.UserCredential.findOne({email: emailLower}, function(err, user){
			            if(err){return helper.logError("signup looking for duplicate emails", err);};
                        if(user){
                            return res.json({'error': 'email'});
                        }else{
                            callback(0, 1);
                        }
                    });
                },
                // check if name exists
                function(callback){
                    req.models.User.findOne({name: userName}, function(err, user){
			            if(err){return helper.logError("signup looking for duplicate names", err);};
                        if(user){
                            return res.json({'error': 'name'});
                        }else{
                            callback(0, 1);
                        }
                    })
                },
            ],
            function(error, results){
                var tempPassword = helper.makeId();
                var userId = helper.makeId();
                var slug = userId;
                // create new instance
                var userModel = {"_id": userId, "name": userName, "slug": slug};
                var uderCreModel = {"user_id": userId, "email": email, "password": hashPassword(pass), "tempPassword": tempPassword};
                // insert
                req.models.User.create(userModel, function(err, model){
		            if(err){return helper.logError("signup adding to user", err);};
                })
                req.models.UserCredential.create(uderCreModel, function(err, model) {
		            if(err){return helper.logError("signup adding to user credentials", err);};
	                // sendVerificationEmail(userName, email, tempPassword);
                    login(req, res, email, hashPassword(pass), true, function(){
                        if(req.cookies.device != "android"){
                            return res.json({"result": true});
                        }else{
                            req.models.User.findOne({_id: req.session.user._id}, function(err, model){
                                return res.json({"response": response_, "sid": req.sessionID, "user": model});
                            })
                        }
                    })
                });
            }
        )
    });
// login
    app.get('/api/login', function (req, res, next){
		helper.logEvent("event", "auth - login");
        var email = req.query.email;
        var password = hashPassword(req.query.password);
        login(req, res, email, password, true, function(response_){
            if(req.cookies.device != "android" || response_ != "ok"){
            	return res.json({"response": response_});
            }else{
                req.models.User.findOne({_id: req.session.user._id}, function(err, model){
                    return res.json({"response": response_, "sid": req.sessionID, "user": model});
                })
            }
        })
    });
// login process
	var login = function(req, res, email_, hashedPass_, fromLogin_, callback_){
        // fixme: compatibility
		req.models.UserCredential.findOne({"email": email_, "password": hashedPass_}, function(err, model){
			var accountResponse = false;
			var ipResponse = false;
			if(!model){
				helper.logEvent("event", "auth - login function failed"+email_+"--"+hashedPass_);
				accountResponse = "no-user";
			}else if(model.account == "not-verified" || model.account == "sustained"){
				helper.logEvent("event", "auth - login function success");
				accountResponse = "ok";
				req.session.user = {_id: model.user_id, account: model.account};
		        res.cookie('email', email_, {maxAge: 100*3600*60*24*30, httpOnly: true});
		        res.cookie('password', hashedPass_, {maxAge: 100*3600*60*24*30, httpOnly: true});
			}else if(model.account == "blocked"){
				helper.logEvent("event", "auth - blocked login attempt");
				accountResponse = "blocked";
				req.session.user = undefined;
			}
			callback_(accountResponse)
            return accountResponse;
        })
	}
// logout
    app.get('/api/logout', function (req, res, next){
        helper.logEvent("event", "auth - logout");
        res.cookie('email', "false");
        res.cookie('password', "false");
        req.session.destroy();
        return res.json({result: false})
    });
// send email verification
	var sendVerificationEmail = function(name_, email_, tempPassword_){
		var config = {
            "to": email_,
            "subject": "جوکی - تایید اکانت",
            "body":
                (
                    "<div style=\"direction: rtl; text-align: right;\">"+
                    "<img src=\"http://joky.ir/static/images/logo_text.svg\" width=150 height=50 style=\"display: inline-block; \">"+
                    "<h4>سلام <span>"+name_+"</span> عزیز</h4>"+
                    "<p>به جوکی خوش آمدین، برای تایید اکانت خود روی لینک زیر کلیک کنید.</p>"+
                    "<a href=\"http://80.75.14.65:3000/api/verifyAccount?key="+tempPassword_+"&email="+email_+"\">لینک: تایید اکانت</a>"+
                    "<p>و اما در صورتی که این ثبت نام توسط شما انجام نشده، روی لینک زیر کلیک کنید تا اکانت شما حذف شود.</p>"+
                    "<a href=\"http://80.75.14.65:3000/api/deVerifyAccount?key="+tempPassword_+"&email="+email_+"\">لینک: حذف اکانت</a>"+
                    "<p>موفق باشید.</p>"+
                    "</div>"
                )
        }
        helper.emailTo(config);
	}
// verify by email
    app.get('/api/verifyAccount', function (req, res, next){
        var tempPassword = req.query.key;
        var email = req.query.email;
        req.models.UserCredential.findOne({"email": email, "tempPassword": tempPassword}, function(err, model){
			if(err){return helper.logError("verify account find user credentials", err);};
        	if(model){
		        req.models.UserCredential.update({"email": email}, {"account": "verified", "tempPassword": "null"}, function(err, model){
					if(err){return helper.logError("verify account -- updating credentials", err);};
	        		return res.json({"result": true})
		        })
        	}
        	return res.json({"result": false})
        })
    })
// deverify by email
    app.get('/api/deVerifyAccount', function (req, res, next){
    	var tempPassword = req.query.key;
        var email = req.query.email;
        req.models.UserCredential.findOne({"email": email, "tempPassword": tempPassword}, function(err, model){
			if(err){return helper.logError("de-verify account -- updating credentials", err);};
        	if(model){
		        req.models.UserCredential.update({"email": email}, {"account": "de-verified", "tempPassword": "null"}, function(err, model){
					if(err){return helper.logError("de-verify account -- updating credentials", err);};
	        		return res.json({"result": true})
		        })
        	}
        	return res.json({"result": false})
        })
    })
// send password link
    app.get('/api/sendPasswordLinkToEmail', function (req, res, next){
		helper.logEvent("event", "auth - sendPasswordLinkToEmail");
        // find the document by email
        req.models.UserCredential.findOne({email: req.query.email.toLowerCase()}, function(err, user){
			if(err){return helper.logError("sendPasswordLinkToEmail -- find user credentials", err);};
            if(!user){
                return res.json({'result': "error"});
            }else{
                var tempPassword = hashPassword(helper.makeId())
                var lowerPassword = req.query.email.toLowerCase();
                req.models.UserCredential.update({email: req.query.email}, {"tempPassword": tempPassword}, function(err, user_){
					if(err){return helper.logError("sendPasswordLinkToEmail -- update user credentials", err);};
                    req.models.User.findOne({_id: user.user_id}, function(err, model){
                        var config = {
                            "to": req.query.email,
                            "subject": "جوکی - تغییر رمز عبور",
                            "body":
                                (
                                    "<div style=\"direction: rtl; text-align: right;\">"+
                                    "<img src=\"http://joky.ir/static/images/logo_text.svg\" width=150 height=50 style=\"display: inline-block; \">"+
                                    "<h4>سلام <span>"+model.name+"</span> عزیز</h4>"+
                                    "<p>این ایمیل در پی خواسته شما برای تغییر رمز عبور فرستاده شده و حد اکثر تا یک ساعت آینده قابل استفاده است.</p>"+
                                    "<p>درصورتی که این درخواست از طرف شما نبوده، به این ایمیل بی اعتنایی کنید.</p>"+
                                    "<p>برای تغییر رمز عبور از لینک زیر اقدام کنید:</p>"+
                                    "<a href=\"http://joky.ir/api/setNewPassword?key="+tempPassword+"&email="+req.query.email+"\">تغییر رمز عبور</a>"+
                                    "</div>"
                                )
                        }
                        helper.emailTo(config);
                    })
                })
                return res.json({'result': "succ"});
            }
        })
    });
// set password
    app.get('/api/setNewPassword', function (req, res, next){
		helper.logEvent("event", "auth - setNewPassword");
        var tempPassword = req.query.key;
        var email = req.query.email;
        res.render("html/email", {key: tempPassword, email: email})
    })
// save password
    app.get('/api/saveNewPassword', function (req, res, next){
		helper.logEvent("event", "auth - saveNewPassword");
        req.models.UserCredential.findOne({email: req.query.email, tempPassword: req.query.key}, function(err, user){
			if(err){return helper.logError("saveNewPassword -- find user credentials", err);};
            if(user.account == "blocked"){return ;}
            if(user){
                req.models.UserCredential.update({"email": req.query.email, "tempPassword": req.query.key}, {"password": hashPassword(req.query.password), "tempPassword": "null", "account": "not-verified"}, function(err, user){
					if(err){return helper.logError("saveNewPassword -- update credentials", err);};
					return res.json({result: "true"})
                })
			    login(req, res, req.query.email, hashPassword(req.query.password), true, function(response_){})
            }else{
                return res.json({result: "false"})
            }
        })
    })
// hash
    var hashPassword = function(string_){
        var crypto = require("crypto");
        var sha256 = crypto.createHash("sha256");
        var result = sha256.update(string_);//utf8 here
        result = sha256.digest("hex");
        return result;
    }
// generate token
    var generateUserToken = function(email_, pass_, salt_){
        return hashPassword(email_+pass_+"the incredible machine");
    };
// slug
    var convertToSlug = function(Text){
    	if(!Text){return false;}
        return Text
        .toLowerCase()
        .replace(/[^\w ]+/g,'')
        .replace(/ +/g,'-')
    }
};
