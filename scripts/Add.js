//region require
var helper = require('../scripts/helper.js');
var ws = require('../scripts/websocket.js');
var auth = require('../scripts/auth');
var fs = require('fs-extra');
var async = require('async');
var MetaInspector = require('node-metainspector');
var sizeOf = require('image-size');
var models = require('../models');
var User = require('../scripts/User.js');
var Noti = require('../scripts/Noti.js');
var Follow = require('../scripts/Follow.js');
var Item = models.Item;
//endregion

// region http
exports.app = function(app) {
    
    app.post('/api/auth/add', function(req, res, next){
        // check if user has permission to this
        var timeB = Date.now()
        if(req.session.user.account == "blocked" || req.session.user.account == "sustained") return res.json({err: "401"})
        actions.add(req.session.user._id, req.body.body, req.body.attachment, req.body.comment, function(err, model){
            return res.json({done: true})
        });
    });

    app.post('/api/auth/addImage', function(req, res, next){
        helper.processImage(req, 300, null, "items", function(imageName){
            return res.json({"done": true, "image": imageName});
        })
    });

    app.get('/api/auth/processLink', function(req, res, next){
        actions.addModule.processLink(req.query.url, function(result){
            return res.json(result);
        })
    });

}
// endregion

//region actions
var actions = {

    // sanetize attachment
    sanetizeAttach: function(attachment_){
        var attachment = {type: "none"}
        if(attachment_.type == "image"){
            attachment.type = "image";
            attachment.image = attachment_.image;
            var dimensions = sizeOf("upload/items/"+attachment.image);
            attachment.dimentions = dimensions.width / dimensions.height
        }else if(attachment_.type == "link"){
            var title = (attachment_.title.length > 100 ? attachment_.title.slice(0, 50)+"..." : attachment_.title);
            var desc = (attachment_.desc.length > 50 ? attachment_.desc.slice(0, 50)+"..." : attachment_.desc);
            attachment.type = "link";
            attachment.url = attachment_.url;
            attachment.title = title;
            attachment.desc = desc;
        }else if(attachment_.type == "video"){
            attachment.type = "video";
            attachment.iframe = attachment_.iframe;
        }
        return attachment;
    },

    /*
     SelfId: safe
     body: unsafe
     body: unsafe
     */
    add: function(SelfId, Body, Attachment, comment_, Callback){
        if(comment_ == "false" || comment_ == ""){
            var allowComment = true
        }else{
            var allowComment = false
        }
        helper.logEvent("event", "addModule - add");
        //var cleanedBody = tagModule.extractTagFromBody(cleanedBody);
        var sanetized = helper.sanetize(Body);
        var cleanedBody = helper.checkForSwear(sanetized);
        // types
        var new_item = {
            "_id": helper.makeId(),
            "item_userid": SelfId,
            "body": cleanedBody,
            "sort_date": Date.now(),
            "attach": this.sanetizeAttach(Attachment),
            "page": "check",
            allow_comment: allowComment
        };
        // get last 10 items - check if they are identical
        Item.find({"item_userid": SelfId}).sort({sort_date: -1}).limit(10).exec(function(err, model){
            if(Body == "" && new_item.attach.type == "none"){
                return Callback();
            }
            //for(i in model){
            //    var item = model[i];
            //    if(item.body == Body){
            //        return Callback();
            //    }
            //}
            // insert
            Item.create(new_item, function(err, model) {
                if(err){return helper.logError("add", err);};

                // ask user module for the add action
                User.actions.manageUserInfo(SelfId, "post-inserted")
                // // send item to those who are following this user
                // Follow.fetch.getFollowersForUserID(SelfId, function(err, result){
                //     Item.findOne({"_id": model._id}).populate("item_userid", User.populate.item).exec(function(err, model){
                //         if(err){return helper.logError("add", err);};
                //         result.push(SelfId)
                //         for(var i in result){
                //             ws.emit(result[i], "new-item", model)
                //         }
                //     })
                // })
                return Callback();
                // send socket
            });
        })
    },

    /*
     Url: Unsafe
     */
    processLink: function(Url, Callback){
        helper.logEvent("event", "addModule - processLink");
        var client = new MetaInspector(Url, {});
        client.on("fetch", function(){
            Callback({"result": true, "title": client.title, "description": client.description, "url": Url})
        });
        client.fetch();
    },

    // chec video link
    // app.get('/api/auth/processVideo', function(req, res, next){
    //     var client = new MetaInspector(req.query.url, {});
    //     client.on("fetch", function(){
    //         var iframe = cheerio(client.document).find("#videoCodeIframe textarea").val();
    //         return res.json({"result": true, "iframe": iframe});
    //     });
    //     client.fetch();
    // });
}
//endregion