//region require
var helper = require('../scripts/helper.js');
var ws = require('../scripts/websocket.js');
var auth = require('../scripts/auth');
var models = require('../models');
var formidable = require('formidable');
var fs = require('fs-extra');
var gm = require('gm').subClass({ imageMagick: true });
var async = require('async');
var MetaInspector = require('node-metainspector');
var cheerio = require('cheerio');
var sizeOf = require('image-size');
var User = require('../scripts/User.js');
var Item = require('../scripts/Item.js');
//endregion

//region http
exports.app = function(app) {

    /**
     * @param one
     */
    app.get('/admin', function (req, res, next){
        return res.sendfile('public/admin/admin.html')
    });

    app.get('/admin/refresh', function (req, res, next){
        Item.fetch.multiPlexer({type: "new", limit: 50, skip: 0,}, function(err, model){
            return res.json({result: model})
        })
        
    });

    /**
     * @param one
     */
    app.get('/adminAPI/fetch', function (req, res, next){
        var type = req.query.type
        // multiplexer for admin
        fetch.multiplexer(type, function(err, result){
            return res.json({result: result})
        })
    });

    app.get('/adminAPI/set-profile-as', function (req, res, next){
        var type = req.query.type
        if(type == "clean"){
            User.flag.setProfileAsCLean(req.query.id)
        }else if(type == "block"){
            User.flag.setProfileAsBlocked(req.query.id)
        }else if(type == "sustain"){
            User.flag.setProfileAsSustained(req.query.id)
        }
        return res.end("OK")
    });

    app.get('/adminAPI/set-item-as', function (req, res, next){
        var safe = parseInt(req.query.safe)
        if(safe == 1){
            Item.flag.setItemAsCLean(req.query.id, 1)
        }else if(safe == 0){
            Item.flag.setItemAsCLean(req.query.id, 0)
        }else if(safe == -1){
            Item.flag.setItemAsDirty(req.query.id)
        }
        return res.json({result: true})
    });

    app.get('/adminAPI/set-comment-as', function (req, res, next){
        Item.flag.setItemAsDirty(req.query.id)
        return res.json({result: true})
    });

    app.get('/adminAPI/set-comment-as', function (req, res, next){
        Item.flag.setItemAsDirty(req.query.id)
        return res.json({result: true})
    });

    app.get('/adminAPI/send-email', function (req, res, next){
        var to = req.query.to
        var body = req.query.body
        helper.emailFromAdmin(to, body)
        return res.json({result: true})
    });

    app.get('/api/test_', function (req, res, next){
        var beg = Date.now();
        models.Item.find({like: 10}).limit(10).sort({sort_date: -1}).exec(function(err, model){
            var end = Date.now();
            helper.log("took", end-beg);
        })
        return res.json({result: true})
    });

}
//endregion

//region fetch
fetch = {

    multiplexer: function(type_, callback_){

        switch (type_) {
            case "item-trending":
                Item.fetch.multiPlexer({type: "new", limit: 100, skip: 0,}, function(err, model){
                    callback_(null, model)
                })
                break;
            case "item-popular":
                Item.fetch.multiPlexer({type: "popular", limit: 100, skip: 0,}, function(err, model){
                    callback_(null, model)
                })
                break;
            case "item-dirty-24":
                models.Item.find(
                    {
                        safe: {$ne: -1},
                        flag: {$gt: 0}
                    }
                )
                    .sort({flag_24: -1})
                    .limit(20)
                    .populate("item_userid", User.populate.item)
                    .exec(function(err, model){
                        callback_(null, model);
                    });
                break;
            case "item-dirty-all":
                models.Item.find(
                    {
                        safe: {$ne: -1},
                        flag: {$gt: 1}
                    }
                ).sort({flag: -1})
                    .limit(100)
                    .populate("item_userid", User.populate.item)
                    .exec(function(err, model){
                        callback_(null, model);
                    });
                break;
            case "item-high-dirty":
                models.Item.find(
                    {
                        $or: [
                            {flag: {$gt: 15}},
                            {flag_24: {$gt: 10}}
                        ],
                    }
                )
                    .sort({flag_24: -1})
                    .limit(100)
                    .populate("item_userid", User.populate.item)
                    .exec(function(err, model){
                        callback_(null, model);
                    });
                break;
            case "user-hot":
                User.fetch.fetchRisingUsers(100, 0, function(err, model){callback_(null, model)})
                break;
            case "user-popular":
                User.fetch.fetchTopUsers(100, 0, function(err, model){callback_(null, model)})
                break;
            case "user-dirty":
                models.UserCredential.find(
                    {
                        account: {$nin: ["sustained", "blocked"]},
                        flag: {$gt: 0}
                    }
                ).sort({flag: -1}).limit(20).exec(function(err, model_){
                        var userIDarr = []
                        for(i in model_){
                            userIDarr.push(model_[i].user_id)
                        }
                        models.User.find({_id: {$in: userIDarr}}).lean().exec(function(err, model){
                            helper.log(typeof model[0])
                            for(i in model){
                                model[i].user = model_[i]
                            }
                            callback_(null, model);
                        })
                    });
                break
            case "comment-dirty":
                models.Comment.find({flag: {$gt: 1}}).sort({flag: -1}).limit(100).exec(function(err, model){
                        callback_(null, model);
                    });
                break
        }
    }

}
//endregion