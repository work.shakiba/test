//region require
var helper = require('../scripts/helper.js');
var async = require('async');
var models = require('../models');
var ws = require('../scripts/websocket.js');
var Noti = require('../scripts/Noti.js').actions;
var User = require('../scripts/User.js');

Follow = models.Follow;
FollowData = models.FollowData;
//endregion

//region http
exports.app = function(app) {

    /**
     * @param target
     */
    app.get('/api/auth/follow', function (req, res, next){
        var userID = req.session.user._id;
        var targetID = req.query.id;
        actions.follow(userID, targetID);
        return res.json({result: true})
    });

    /**
     * @param target
     */
    app.get('/api/auth/get-following-data-since', function (req, res, next){
        var userID = req.session.user._id;
        var since = req.query.since;
        fetch.getFollowingDataSince(userID, since, function(err, model){
            return res.json({result: model})
        });
    });

}
//endregion

//region actions
var actions = {

    /*
     UserId = safe
     TargetUserId = unsafe
     */
    follow: function(UserId, TargetUserId){
        var thisClass = this;
        async.parallel(
            [
                // find the target user
                function(callback){
                    User.actions.exists(TargetUserId, function(error, model){
                        if(!model){return helper.log("not found");}
                        callback(null, model)
                    })
                },
                // chech if user has followed before
                function(callback){
                    Follow.findOne({"follow_user_id": UserId, "is_following": TargetUserId}, function(err,model){
                        if(model){
                            callback(null, true)
                        }else{
                            callback(null, false)
                        }
                    });
                },
            ],
            function(err, results){
                var is_following = false;
                // user is flowwing
                if(results[1]){
                    Follow.remove({"follow_user_id": UserId, "is_following": TargetUserId},function(){});
                    User.actions.manageUserInfo(UserId, "disfollow")
                    User.actions.manageUserInfo(TargetUserId, "disfollowed")
                    // user is not floowong
                }else{
                    is_following = true;
                    Follow.create({"follow_user_id": UserId, "is_following": TargetUserId});
                    User.actions.manageUserInfo(UserId, "follow")
                    User.actions.manageUserInfo(TargetUserId, "followed")
                    Noti.setNotiForUser(TargetUserId, UserId, false, "follow");
                }
                // set the following data in the FollowData table
                thisClass.enterFollowingDataToDB(UserId, is_following, TargetUserId);
            }
        );
    },

    /*
     * enter following and disfollowing data to db
     * so that user can enable edits in client by last modified date
     */
    enterFollowingDataToDB: function(userID_, isFollowing_, targetUserID_){
        FollowData.create({user_id: userID_, is_following: isFollowing_, target_user_id: targetUserID_, date: Date.now()}, function(err, model){})
        return true;
    },

}
exports.actions = actions;
//endregion

//region fetch
var fetch = {

    /*
     * selfID_: safe
     * since_: unsafe
     *
     * send all the data to client after the since
     * return the array
     */
    getFollowingDataSince: function(selfID_, since_, callback_){
        // safe the sine_
        var since = helper.safeInt(since_, 0);
        if(isNaN(since)){return ;}
        // get the data
        FollowData.find({user_id: selfID_, date: {$gt: since_}}, function(err, model){
            return callback_(null, model);
        });
    },

    getFollowingData: function(userID_, limit_, offset_, callback_){
        Follow
            .find({follow_user_id: userID_})
            .limit(limit_)
            .skip(offset_)
            .exec(function(err, model){
                if(model){
                    var users = [];
                    for(i in model){
                        users.unshift(model[i].is_following)
                    }
                    return callback_(null, users);
                }
                callback_(true, null)
            })
    },

    getFollowingData: function(userID_, limit_, offset_, callback_){
        Follow
            .find({follow_user_id: userID_})
            .limit(limit_)
            .skip(offset_)
            .exec(function(err, model){
                if(model){
                    var users = [];
                    for(i in model){
                        users.unshift(model[i].is_following)
                    }
                    return callback_(null, users);
                }
                callback_(true, null)
            })
    },

    /**
     * get array of user IDs who are following this user
     * @param userID_
     * @param callback_
     */
    getFollowersForUserID: function(userID_, callback_){
        Follow.find({is_following: userID_}, function(err, model){
            var users = [];
            for(i in model){
                users.unshift(model[i].follow_user_id)
            }
            return callback_(null, users);
        })
    },

    /**
     * get array of user IDs whom this user is following
     * @param userID_
     * @param callback_
     */
    getFollowingsForUserID: function(userID_, callback_){
        Follow.find({follow_user_id: userID_}, function(err, model){
            var users = [];
            for(i in model){
                users.unshift(model[i].is_following)
            }
            return callback_(null, users);
        })
    },

    getFollowingUsers: function(userID_, type_, limit_, offset_, callback_){
        if(type_ == "following"){
            var find = {follow_user_id: userID_};
            var populate = "is_following";
        }else{
            var find = {is_following: userID_};
            var populate = "follow_user_id";
        }
        Follow
        .find(find)
        .limit(limit_)
        .skip(offset_)
        .populate(populate, User.populate.profile)
        .exec(function(err, model){
            if(model){
                var users = [];
                for(i in model){
                    users.unshift(model[i][populate])
                }
                return callback_(null, users);
            }
            callback_(true, null)
        })
    },

}
exports.fetch = fetch;
//endregion