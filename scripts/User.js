//region require
var helper = require('../scripts/helper.js');
var async = require('async');
var models = require('../models');
var CronJob = require('cron').CronJob;
var ws = require('../scripts/websocket.js');
var fs = require('fs-extra');
var gm = require('gm').subClass({ imageMagick: true });
var Noti = require('../scripts/Noti.js').actions;
var formidable = require('formidable');
var Item = require('../scripts/Item.js');
var Follow = require('../scripts/Follow.js');
User = models.User;
UserCredential = models.UserCredential;
//endregion

//region http
exports.app = function(app) {

    /**
     * @param one
     */
    app.get('/authapi/user/flag', function (req, res, next){
        var userID = req.session.user._id;
        var targetID = req.query.target;
        flag.flag(userID, targetID)
        return res.json({result: true})
    });

    /**
     * @param userID
     * @param type [posts, following, follwers]
     * @param limit
     * @param offset
     * @returns {user: data:}
     */
    app.get('/api/user/profile', function (req, res, next){
        var userID = req.query.userID;
        var type = helper.safeStringFromArray(req.query.type, ["posts", "following", "followers"]);
        var skip = helper.safeInt(req.query.offset, 0, 9999);
        var limit = helper.safeInt(req.query.limit, 10, 100);
        var funcArr = [
            function(callback){
                fetch.getUserModel(userID, function(err, model){
                    callback(null, model)
                })
            },
            function(callback){
                if(type == "posts"){
                    Item.fetch.multiPlexer({targetID: userID, limit: limit, skip: skip, type: "user"}, function(err, model){
                        callback(null, model)
                    })
                }else{
                    Follow.fetch.getFollowingUsers(userID, type, limit, skip, function(err, model){
                        callback(null, model)
                    })
                }
            }
        ];
        async.parallel(funcArr, function(err, results){
            return res.json({user: results[0], result: results[1]})
        })
    });

    /**
     * name: String
     * brief: String
     * res: json(true) || json {error: errName}
     */
    app.get('/api/auth/user/edit', function (req, res, next){
        var edits = {
            "name": req.query.name,
            "brief": req.query.brief,
        }
        actions.editProfile(req.session.user._id, edits, function(result){
            return res.json(result)
        })
    });

    app.post('/api/auth/thumbnail', function (req, res){
        actions.recieveImageForThumbnail(req, function(imageName){
            return res.json({'done': true, 'image': imageName});
        })
    });

    /**
     * @param term
     * @param limit
     * @param offset
     */
    app.get('/api/user/search', function (req, res, next) {
        var term = req.query.term;
        var skip = helper.safeInt(req.query.offset, 0, 9999);
        var limit = helper.safeInt(req.query.limit, 10, 50);
        // begin the search
        fetch.search(term, limit, skip, function(err, result){
                return res.json({result: result})
        })
    });

    /**
     * returns user model
     */
    app.get("/api/get-user-info", function(req, res, next){
        if(!req.session.user){
            return res.send("var User = "+JSON.stringify({}))
        }

        var tasks = [
            function(callback){
                User.findOne({_id: req.session.user._id}, function(err, model){
                    if(!model) model = {}
                    callback(null, model)
                })
            },
            function(callback){
                UserCredential.findOne({user_id: req.session.user._id}, function(err, model){
                    callback(null, model.account)
                })
            }
        ]

        async.parallel(tasks, function(err, result){
            result[0].slug = result[1]
            return res.send("var User = "+JSON.stringify(result[0]))
        })

    })

    /**
     * returns user model
     */
    app.get("/api/auth/get-suggestions", function(req, res, next){
        fetch.fetchSuggestionsForUserID(req.session.user._id, 0, function(err, model){
            return res.json({result: model})
        })
    })



}
//endregion

//region allowed user populate
populate = {
    item: {_id: 1,   name: 1,   thumbnail: 1},
    profile: {_id: 1,   name: 1,   thumbnail: 1,   brief: 1,   posts: 1,   following: 1, followers: 1,}
}
exports.populate = populate;
//endregion

//region flag
var flag = {

    /**
     * increment the flag property of UserCredentials and add the userID to flagged_by
     * @param userID
     * @param targetID
     */
    flag: function(userID, targetID){
        UserCredential.findOne({user_id: userID}, function(err, userModel){
            UserCredential.findOne({user_id: targetID}, function(err, targetModel){
                if(targetModel && targetModel.flagged_by.indexOf(userID) == -1){
                    UserCredential.update({user_id: targetID}, {$push: {flagged_by: userID}, $inc: {flag: 1}}, function(err, model){})
                }
            })
        })
    },

    /**
     * if the user has correctly flagged an item, his/her userCredentials will be incremented/ decremeneted if incorrectly flag an item
     * @param userID
     * @param value
     */
    levelUpOrDownUserFlag: function(userID, value){
        models.UserCredential.update({user_id: userID}, {$inc: {user_flag_level: value}})
    },

    /**
     * testme
     * when post is determined as dirty, credential is +1, if reached three in one week, the profile is sustained
     * @param userID
     * @callback (null, new_post_dirty_count)
     */
    dirtyUpProfile: function(userID, callback){
        models.UserCredential.update({user_id: userID}, {$inc: {post_dirty_count: +1}}, function(err, model){
            models.UserCredential.findOne({user_id: userID}, function(err, model){
                if(model){
                    if(model.post_dirty_count > 2){
                        flag.setProfileAsSustained(userID)
                    }
                    return callback(null, model.post_dirty_count)
                }else{
                    return callback(null, 0)
                }
            })
        })
    },

    /**
     * testme
     * clear post_dirty_count
     * every week
     */
    cron_clearDirty: function(){
        models.UserCredential.update({}, {post_dirty_count: 0}, function(err, model){})
    },


    /**
     * testme
     * when profile has been detected as dirty by admin
     * @param userID
     * @param flagModel
     */
    setProfileAsBlocked: function(userID){
        // set profile account as blocked
        UserCredential.update({user_id: userID}, {account: "blocked"}, function(err, model){});
        Noti.setNotiForUser(userID, null, null, "you_are_blocked");
        UserCredential.findOne({user_id: userID}, function(err, model){
            if(model){
                for(var i in model.falgged_by){
                    var thisUserID = model.flagged_by[i];
                    Noti.setNotiForUser(thisUserID, null, userID, "flagged_user_blocked");
                }
            }
        })
    },

    /**
     * testme
     * when profile dirty reach three, it gets sustained
     * @param userID
     */
    setProfileAsSustained: function(userID){
        models.UserCredential.find({user_id: userID}, function(err, model){
            if(model && model.account != "sustained"){
                helper.log("setting profile as sustained", userID)
                var time = model.sustain_time
                if(!time) time = 1
                var duration = 1000 * 60 * 60 * 24 * time
                // set credentials as sustained
                models.UserCredential.update({user_id: userID}, {account: "sustained", sustain_time: time + 2, sustain: Date.now() + duration}, function(err, model){
                    if(err) helper.log("err", err)
                });
                // send notification that user is now sustained
                Noti.setNotiForUser(userID, null, time, "you_are_sustained");
            }
        })
    },

    setProfileAsCLean: function(userID){
        UserCredential.update({user_id: userID}, {flagged_by: [], flag: 0}, function(err, model){
            if(err) helper.log(err)
        })
    },

    /**
     * every hour checks if sustain has finished for user
     */
    cron_unsustain: function(){
        models.UserCredential.find({account: "sustained", sustain: {$lt: Date.now()}}, function(err, model){
            for (var i in model) {
                UserCredential.update({user_id: model[i].user_id}, {account: "not-verified"}, function(err, model){})
                Noti.setNotiForUser(model[i].user_id, null, null, "you_unsustained");
            }
        })
    },

}
exports.flag = flag;
//endregion

//region actions
var actions = {

    getUserCredentialModel: function(userID, callback){
        UserCredential.findOne({user_id: userID}, function(err, model){
            if(err) return callback(err, null);
            if(model) return callback(null, model);
        })
    },

    getUserModel: function(userID, callback){
        User.findOne({_id: userID}, function(err, model){
            if(err) return callback(err, null);
            if(model) return callback(null, model);
        })
    },

    exists: function(userID_, calback_){
        User.findOne({_id: userID_}).exec(function(err, model){
            if(model){
                calback_(null, true)
            }else{
                calback_(null, false)
            }
        })
    },

    /*
     SelfId = safe
     Edits = Object unsafe
     Callback = funct
     */
    editProfile: function(SelfId, Edits, Callback){
        // find user with the same slug, but not the same Id as the slefId
        User.findOne({"name": Edits.name, "_id": {$ne: SelfId}}, function(err, user){
            if(user){return Callback({"error": "name"});}
            var slug = helper.convertToSlug(Edits.name);
            models.User.findOne({"slug": slug, "_id": {$ne: SelfId}}, function(err, user){
                if(user){
                    slug = helper.makeId();
                }
                // updated data
                var updated_data = {
                    slug: slug,
                    name: helper.sanetize(Edits.name),
                    brief: helper.sanetize(Edits.brief)
                };
                // update
                models.User.update({"_id": SelfId}, updated_data, function(err, db_result){
                    Callback(true);
                });
            })
        })
    },

    manageUserInfo: function(userID_, action_){
        var postsInc = 0;
        var followingInc = 0;
        var followersInc = 0;
        var activity_inc = 0;

        switch(action_){
            case "post-inserted":
                postsInc++
                activity_inc++
                break
            case "post-deleted":
                postsInc--
                break
            case "follow":
                followingInc++
                break
            case "disfollow":
                followingInc--
                break
            case "followed":
                activity_inc++
                followersInc++
                break
            case "disfollowed":
                followersInc--
                break
            default:
                helper.log("nothing to do--------")
        }

        var activity_inc = postsInc*10;

        User.update({_id: userID_}, {$inc: {
            posts: postsInc,
            following: followingInc,
            followers: followersInc,
            recent_activity: activity_inc,
            long_activity: activity_inc,
        }}, function(err, model){
            if(err) helper.logError("error in manageUserInfo", err)
        })
    },

    recieveImageForThumbnail: function(req, callback){
        helper.logEvent("event", "image - recieveImageForThumbnail");
        var form = new formidable.IncomingForm();
        form.parse(req, function(err, fields, files) {});
        form.on('end', function(fields, files){
            var temp_path = this.openedFiles[0].path;
            var file_name = this.openedFiles[0].name;
            var file_type = this.openedFiles[0].type.replace('image/','.');
            var temp_location = 'upload/users/temp/';
            var new_location = 'upload/users/';
            fs.copy(temp_path, temp_location + file_name, function(err) {
                if(err){
                    if(err){return helper.logError("error while creating temp image", err);};
                }else{
                    var name = helper.makeId()+file_type;
                    gm(temp_location + file_name)
                        .gravity('Center')
                        .extent(500, 500)
                         .resize(500)
                        .crop(500, 500)
                        .noProfile()
                        .write(new_location + name, function (err) {
                            if(err){return helper.logError("error while creating profile image", err);}
                            // fixme -> must be in a function
                            models.User.update({_id: req.session.user._id}, {"thumbnail": name}, function(){})
                            callback(name);
                        });

                    gm(temp_location + file_name)
                        .gravity('Center')
                        .extent(50, 50)
                        .noProfile()
                        .write(new_location + "s"+name, function (err) {});
                }
            });
        });
    },

    /**
     * this function devides recent_activity filed of every model
     * runs every week
     */
    cron_resetLongActivities: function(lol){
        User.update({}, {$divide: {long_activity: 1.25}}, function(err, model){})
    },

    /**
     * this function divides recent_activity filed of every model
     * runs every 24 hour
     */
    cron_resetRecentActivities: function(lol){
        User.update({}, {$divide: {recent_activity: 1.5}}, function(err, model){})
    },

}
exports.actions = actions;
//endregion

//region fetch
var fetch = {

    getUserModel: function(userID, callback){
        User.findOne({_id: userID}, function(err, model){
            if(err) return callback(err, null);
            if(model) return callback(null, model);
        })
    },

    fetchRisingUsers: function(limit_, skip_, callback_){
        User
            .find({
                blocked: false,
                followers: {$lt: 50},
                followers: {$gt: 5},
                posts: {$gt: 5}
            }, populate.profile)
            .limit(limit_)
            .sort({long_activities: -1})
            .skip(skip_)
            .exec(function(err, model){
                if(err) return callback_(err, null);
                if(model) return callback_(null, model);
            })
    },

    fetchTopUsers: function(limit_, skip_, callback_){
        User.find({
            blocked: false,
        }, populate.profile)
            .limit(limit_)
            .skip(skip_)
            .sort({posts: -1, followers: -1})
            .exec(function(err, model){
                if(err) return callback_(err, null);
                if(model) return callback_(null, model);
            })
    },

    search: function(term, limit, skip, callback){
        // begin the search
        User
            .find({$text: {$search: term}}, populate.profile)
            .limit(limit)
            .skip(skip)
            .exec(function(error, model){
                return callback(null, model)
            });
    },

    fetchSuggestionsForUserID: function(selfID_, offset_, callback_){
        // get following of this user
        Follow.fetch.getFollowingsForUserID(selfID_, function(err, array){
            User
                .find({blocked: false, posts: {$gt: 10}, followers: {$lt: 30}, _id: {$nin: array}}, populate.profile)
                .limit(25)
                .sort({recent_activity: -1})
                .skip(offset_)
                .exec(function(error, model){
                    return callback_(null, model)
                });
        })
    }

}
exports.fetch = fetch;
//endregion

//region cronJobs

new CronJob({
    cronTime: '00 00 */24 * * *',
    onTick: function() {
        flag.cron_unsustain();
    },
    start: true,
    timeZone: 'Asia/Tehran'
});

new CronJob({
    cronTime: '00 00 0 * * 0',
    onTick: function() {
        flag.cron_clearDirty();
    },
    start: true,
    timeZone: 'Asia/Tehran'
});

new CronJob({
    cronTime: '00 00 0 * * *',
    onTick: function() {
        actions.cron_resetRecentActivities();
    },
    start: true,
    timeZone: 'Asia/Tehran'
});

new CronJob({
    cronTime: '00 00 0 * * 0',
    onTick: function() {
        actions.cron_resetLongActivities();
    },
    start: true,
    timeZone: 'Asia/Tehran'
});

//endregion