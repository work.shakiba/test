//region require
    var WebSocketServer = require('ws').Server;
    var helper = require('../scripts/helper.js');
    var wss = new WebSocketServer({port: 81});
    var models = require('../models');
    var app = require("../app.js")

    // client regstered sockets
    var userIdToSocketId = {};
    var severListeningString = {};
//endregion

//region socket
    wss.on('connection', function(socket){
        // get query string
        var keyString = socket.upgradeReq.url.split("/");
        if(keyString.length < 2) socket.close()
        var sessionID = keyString[1];
        var userID = keyString[2];

        // check if user is valid
        app.authenticateBySessionID(userID, function(result, userID_){
            if(!result){
                socket.close();
            }else{
                userIdToSocketId[userID] = socket;
            }
        })

        // on message
        socket.on('message', function(message){
            socketOnMessage(message, userID)
        });

        // on error
        socket.on('error', function(message){
        });

        // on close
        socket.on('close', function(){
            socketOnClose(userID)
        });

    });
//endregion

//region on message
    var socketOnMessage = function(message_, userId_){
        // if listening to this string
        var messageObject = JSON.parse(message_);
        if(typeof(messageObject) != "object"){return false;}
        var message = messageObject.message;
        var key = messageObject.key;
        var serverHandler = severListeningString[key];
        if(serverHandler){
            serverHandler(message);
        }
    }
//endregion

//region on close
    var socketOnClose = function(userId_){
        delete userIdToSocketId[userId_];
    }

    exports.on = function(String_, handler_){
        severListeningString[String_] = handler_;
    }
//endregion

//region emit
    exports.emit = function(userID_, String_, Message){
        var socket = userIdToSocketId[userID_]
        if(socket){
            helper.log("socket found")
            try{
                socket.send(JSON.stringify({event: String_, data: Message}));
            }catch(e){
                helper.logError("error in sending socket", e)
            }
        }
    }
//endregion


