var mongoose = require('mongoose');

var schema = new mongoose.Schema({

    body: {
        type: String,
        unique: true,
    },

    count: {
        type: Number,
        default: 0,
    },

    today_count: {
        type: Number,
        default: 0,
    },

    count_array: {
        type: Array,
        default: [],
    },
    
});

module.exports = mongoose.model('Tag', schema);