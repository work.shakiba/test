var mongoose = require('mongoose');

var likeSchema = new mongoose.Schema({
  like_user_id: {
    type: String,
    required: true,
    ref: 'User'
  },
  like_item_id: {
    type: String,
    required: true,
    ref: 'Item'
  },
});

module.exports = mongoose.model('Like', likeSchema);