var mongoose = require('mongoose');

var notiSchema = new mongoose.Schema({

    /**
     * the person who will receive this notification
     */
    objectOwnerId: {
        type: String,
        index: true,
    },

    /**
     * type
     */
    type: {
        type: String,
    },
    date:{
        type: String,
    },

    /**
     * the person who resulted in creation of this notificatio
     */
    dowerId: {
        type: Object,
        ref: "User",
    },

    /**
     * reference to which itemID this notification is
     */
    objectId: {
        type: String,
        ref: "Item",
    },

    /**
     * if the user has seen this or not
     */
    seen: {
        type: Boolean,
        default: false
    }
});

module.exports = mongoose.model('Noti', notiSchema);