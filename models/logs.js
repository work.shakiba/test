var mongoose = require('mongoose');

var schema = new mongoose.Schema({

    package: {
        type: String,
    },

    action: {
        type: String,
    },

    type: {
        type: String,
    },

    error: {
        type: String,
    },

    extra: {
        type: String,
    },

    date: {
        type: Number,
    }

});

module.exports = mongoose.model('Log', schema);