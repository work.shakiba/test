    var mongoose = require('mongoose');

    var itemSchema = new mongoose.Schema({

        _id: {
            type: String,
            index: true,
        },

        item_userid: {
            type: String,
            index: true,
            ref: 'User',
        },

        body: {
            type: String,
            index: "text"
        },

        /**
         * sort_date is the date on which item is created
         */
        sort_date:{
          index: true,
            type: Number,
        },

        /**
         * number of likes
         */
        like: {
            type: Number,
            index: true,
            default: 0
        },

        /**
         * check if this item can contain comments or not
         */
        allow_comment: {
            type: Boolean,
            default: true,
        },

        /**
         * count of comments for this post
         */
        comment: {
            type: Number,
            default: 0
        },

        /**
         * comment to show
         */
        comment_holder: {
            type: Object,
        },

        /**
         * contains the attachment status and information
         */
        attach: {
            type: Object,
        },

        /**
         * 0 -> none -> no distinct filtering has been done
         * 1 -> new -> distinct filtering has been done
         * 2 -> trending -> //
         * 3 -> popular -> //
         * 4 -> controversial -> //
         */
        distinct: {
            type: Number,
            index: true,
            default: 0,
        },

        /**
         * it can have 3 states -> -1, 0, 1
         * 1 means it's safe to be in lists
         * 0 means its not safe to be seen in lists
         * -1 means it's hidden
         * if item has even a small amount of flags, it won't be set as 1
         * default safe value of item is 0 but with cron we make it as 1 if the flags are low
         * it is indexed, so that list can find by this value easily
         */
        safe: {
            type: Number,
            default: 0,
            index: true,
        },

        /**
         * number of flags in the first 24 hour
         * after that set to 0 by cron
         */
        flag_24: {
            default: 0,
            type: Number,
        },

        /**
         * total number of flagged
         * only set to zero by admin
         */
        flag: {
            default: 0,
            type: Number,
        },

        /**
         * list of usersIDs who have flagged this item
         */
        flagged_by: {
            type: Array
        },

    });
module.exports = mongoose.model('Item', itemSchema);
