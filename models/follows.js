var mongoose = require('mongoose');

var followSchema = new mongoose.Schema({
  follow_user_id: {
    type: String,
    index: true,
    required: true,
    ref: 'User'
  },
  is_following: {
    type: String,
    index: true,
    required: true,
    ref: 'User'
  },
  date: {
    type: String,
  }
});

module.exports = mongoose.model('Follow', followSchema);