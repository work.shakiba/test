var mongoose = require('mongoose');

var userSchema = new mongoose.Schema({
  _id: {
    type: String,
    index: true,
  },
  relation_key: {
    type: String,
    index: true,
  },
  members: {
    type: Array,
  },
  date: {
    type: Number,
  },
  last_message: {
    type: String,
    default: "",
  },
});

module.exports = mongoose.model('Inbox', userSchema);
