    var mongoose = require('mongoose');

    var itemSchema = new mongoose.Schema({

        socket_id: {
            type: String,
            index: true,
            required: true,
            ref: 'User',
        },

        user_id: {
            type: String,
            index: true,
            required: true,
            ref: 'User',
        },

    });
module.exports = mongoose.model('Socket', itemSchema);
