var mongoose = require('mongoose');

var userSchema = new mongoose.Schema({

    _id:{
        type: String,
        index: true,
        unique: true,
    },

    date:{
        type: String,
    },

    name: {
        type: String,
        index: "text",
        min: 5,
        max: 20,
        required: true,
    },

    slug: {
        type: String,
        index: true,
        min: 5,
        max: 30,
        required: true,
        unique: true,
    },

    brief: {
        type: String,
    },

    thumbnail: {
        type: String,
        default: "def.svg",
    },

    posts: {
        type: Number,
        default: 0,
    },

    following: {
        type: Number,
        default: 0,
    },

    followers: {
        type: Number,
        default: 0,
    },

    /**
     * used for raising users
     * divided by 1.2 every week
     */
    long_activity: {
        type: Number,
        index: true,
    },

    /**
     * used for raising users
     * divided by 1.5 every 24 hours
     */
    recent_activity: {
        type: Number,
        index: true,
    },

    /**
     * if user is blocked, it can't be found anywhere
     * it can only be true or false
     */
    blocked: {
      type: Boolean,
      default: false,
        index: true,
    }

});

module.exports = mongoose.model('User', userSchema);
