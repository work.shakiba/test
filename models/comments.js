var mongoose = require('mongoose');

var userSchema = new mongoose.Schema({
    post_id: {
        type: String,
        index: true,
        ref: 'Item'
    },
    creator: {
        type: String,
        ref: 'User'
    },
    commentor: {
        type: String,
        ref: 'User'
    },
    /**
     * stores the name of the user whom has been responded to
     */
    response_to: {
        type: String,
    },
    sortDate: {
        type: Number,
    },
    body: {
        type: String,
    },
    like: {
        type: Number,
    },
    flag: {
        type: Number,
        default: 0,
    }
});

module.exports = mongoose.model('Comment', userSchema);
