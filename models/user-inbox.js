var mongoose = require('mongoose');

var userSchema = new mongoose.Schema({
  self_id: {
    type: String,
  },
  target_id: {
    type: String,
    ref: "User"
  },
  inbox_id: {
    type: String,
  },
  blocked: {
    type: Boolean,
    default: false
  }
});

module.exports = mongoose.model('UserInbox', userSchema);