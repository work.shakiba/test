var mongoose = require('mongoose');

var userSchema = new mongoose.Schema({

    user_id:{
        type: String,
        required: true,
    },

    user_is_viewing_user_id:{
        type: String,
        required: true,
    },

});

module.exports = mongoose.model('userIsViewing', userSchema);