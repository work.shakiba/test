var mongoose = require('mongoose');

var Schema = new mongoose.Schema({
  _id:{
    type: String,
    index: true,
  },
  name: {
    type: String,
    required: true,
  },
  brief: {
    type: String,
  },
  image: {
    type: String,
    default: "def.svg",
  },
  posts: {
    type: Number,
    default: 0,
  },
  following: {
    type: Number,
    default: 0,
  },
  followers: {
    type: Number,
    default: 0,
  },
  admin: {
    type: String,
    required: true,
  },
});

module.exports = mongoose.model('Page', Schema);