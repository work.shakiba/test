var mongoose = require('mongoose');

var userCredentialsSchema = new mongoose.Schema({

    user_id:{
        type: String,
        index: true,
        unique: true,
        ref: "User",
    },

    email: {
        type: String,
        index: true,
        required: true,
        min: 5,
        unique: true,
        max: 50,
        set: function(value){
            return value.toLowerCase().trim()
        },
        // validate: [
        //     function(email) {
        //         return (email.match(/[a-z0-9!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+\/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/i) != null)
        //     },
        //     'Invalid email'
        // ]
    },

    password: {
        type: String,
        required: true,
        min: 5,
        max: 50,
        set: function(value){
            return value.trim()
        },
    },

    tempPassword: {
        type: String,
    },

    user_flag_level : {
        type: Number,
        default: 1
    },

    /**
     * this filed show how many posts of this user has been dirty in the past week
     * if > 3 the user will be sustained
     */
    post_dirty_count: {
        type: Number,
        default: 0
    },

    /**
     * normal
     * sustained -> cannot post or comment
     * blocked -> cannot post or comment & profile in not visible
     */
    account: {
        type: String,
        default: "not-verified",
    },

    /**
     * number of times, this profile has been flagged
     */
    flag: {
        type: Number,
        default: 0,
    },

    /**
     * userIDs who have flagged this profile
     */
    flagged_by: {
        type: Array
    },

    /**
     * date until which the user cannot post or comment
     */
    sustain: {
        type: Number,
    },

    /**
     * number of times where user has been sustained,
     * this number increments every time, thus resulting in higher sustain duration
     */
    sustain_time: {
        type: Number,
        default: 1,
    }

});

module.exports = mongoose.model('UserCredential', userCredentialsSchema);
