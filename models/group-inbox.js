var mongoose = require('mongoose');

var userSchema = new mongoose.Schema({
  _id: {
    type: String,
    index: true,
  },
  relation: {
    type: String,
    index: true,
  },
  admin: {
    type: String,
    ref: 'User'
  },
  name: {
    type: String,
    min: 5,
    max: 50,
  },
  thumbnail: {
    type: String,
    default: "inbox.svg",
  },
  members: {
    type: Array,
  },
  blocked_memebers: {
    type: Array,
  },
  date: {
    type: Number,
  },
  last_message: {
    type: String,
    default: "",
  },
  stopped: {
    type: Boolean,
    default: false,
  }
});

module.exports = mongoose.model('Inbox', userSchema);