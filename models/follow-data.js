var mongoose = require('mongoose');

var followSchema = new mongoose.Schema({
  user_id: {
    type: String,
    index: true,
    ref: 'User'
  },
  is_following: {
    type: Boolean,
  },
  target_user_id: {
    type: String,
  },
  date: {
    type: Number,
    index: true,
  }
});

module.exports = mongoose.model('Follow-data', followSchema);