var mongoose = require('mongoose');

var userSchema = new mongoose.Schema({
  blocker_user_id: {
    type: String,
    index: true,
  },
  blocked_user_id: {
    type: String,
    index: true,
  },
  blocked: {
    type: Boolean,
  },
  flagged: {
    type: Boolean,
  },
});

module.exports = mongoose.model('InboxBlockList', userSchema);