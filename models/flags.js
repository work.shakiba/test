var mongoose = require('mongoose');

var schema = new mongoose.Schema({
    ref_id: {
        tyep: String,
    },
    type: {
        type: String,
    },
    flag_item_id: {
        type: Object,
    },
    weight_all: {
        type: Number,
    },
    weight_24: {
        type: Number
    },
    date: {
        type: Number
    }
});

module.exports = mongoose.model('Flag', schema);