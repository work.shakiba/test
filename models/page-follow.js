var mongoose = require('mongoose');

var Schema = new mongoose.Schema({
  user_id: {
    type: String,
    required: true,
    ref: 'User'
  },
  page_id: {
    type: String,
    required: true,
    ref: 'Page'
  },
  admin: {
    type: String,
    default: "false"
  }
});

module.exports = mongoose.model('pageFollow', Schema);