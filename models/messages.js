var mongoose = require('mongoose');

var userSchema = new mongoose.Schema({
  sender: {
    type: String,
    index: true,
    ref: 'User'
  },
  reciever: {
    type: String,
    index: true,
    ref: 'User'
  },
  body: {
    type: String,
  },
  inbox_id: {
    type: String,
    index: true,
  }
});

module.exports = mongoose.model('Messages', userSchema);