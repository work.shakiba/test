    var mongoose = require('mongoose');

    var itemSchema = new mongoose.Schema({

        _id: {
            type: String,
            index: true,
            required: true, 
        },

        item_userid: {
            type: String,  
            index: true,
            required: true, 
            ref: 'User', 
        },

        body: {
            type: String,
            index: "text"
        },

        date:{
            type: String,
            required: true, 
        },

        sort_date:{
            type: Number,
            select: false,
        },

        like: {
            type: Number,
            default: 0
        },

        comment: {
            type: Number,
            default: 0
        },

        attach: {
            type: Object,
            default: {type: "none"}
        },

        itemScore: {
            type: Number,
            default: 0,
            select: false,
        },

        page: {
            type: String
        }

    });
module.exports = mongoose.model('NewItem', itemSchema);