
	window.onload = function(){

	}

	var drawImageOnWindow = function(image_){

	 imageElem = $("<img/>",{src: image_.src})

		var container = $(".imageCropper .image");
		var containerWidth = container.width();
		var containerHeight= container.height();

		var image = image_;
		var imageWidth = image.width;
		var imageHeigth = image.height;
		
		var widthRatio = imageWidth/containerWidth;
		var heightRatio = imageHeigth/containerHeight;

		// everthing is ok and image is smaller than the container
		if(widthRatio <= 1 && heightRatio <= 1){
			// draw the image
			$('.imageCropper .image')
				.css("width", imageWidth+"px")
				.css("height", imageHeigth+"px")
				.css("background", "url("+image_.src+") no-repeat");

		// image with or height is bigger than the container
		}else{
			var biggerRation = (widthRatio >= heightRatio ? widthRatio : heightRatio);
			$('.imageCropper .image')
				.css("width", imageWidth/biggerRation+"px")
				.css("height", imageHeigth/biggerRation+"px")
				.css("background", "url("+image_.src+") no-repeat")
				.css("background-size", imageWidth/biggerRation+"px "+imageHeigth/biggerRation+"px")
		}
		console.log("bigger ratio",biggerRation)
		global_biggerRation = biggerRation;

		drawHandler();
	}

	var drawHandler = function(){

		var container = $(".imageCropper .image");
		var containerWidth = container.width();
		var containerHeight= container.height();

		var handlerWidth = 200;
		var handlerHeight = 100;

		container.append(
			"<div class=frame>"+
				"<span class=move></span>"+
				"<span class=\"handle handle1\"></span>"+
				"<span class=\"handle handle2\"></span>"+
				"<span class=\"handle handle3\"></span>"+
				"<span class=\"handle handle4\"></span>"+
			"</div>"
		);
		$(".frame")
			.css("width", handlerWidth+"px")
			.css("height", handlerHeight+"px")
			.css("top", (containerHeight-handlerHeight)/2+"px")
			.css("left", (containerWidth-handlerWidth)/2+"px")

		$(document).on('mousedown', '.frame .move', function(e){
		    e.preventDefault();
		    e.stopPropagation();
		    $(document).on('mousemove', move);
		    $(document).on('mouseup', endMove);
		});

		$(document).on('mousedown', '.frame .handle', function(e){
		    e.preventDefault();
		    e.stopPropagation();
		    resizeStart(e);
		    $(document).on('mousemove', resizeHandler);
		    $(document).on('mouseup', endMove);
		});
	}

	move = function(e){

		var handler = $(".frame");
		var handlerHeight = handler.height();
		var handlerWidth = handler.width();

		var container = $(".imageCropper .image");
		var containerWidth = container.width();
		var containerHeight= container.height();
		var containerOffsetX = container.offset().left;
		var containerOffsetY = container.offset().top;

	    var top = (e.pageY - containerOffsetY - handlerHeight/2);
	    var left = (e.pageX - containerOffsetX - handlerWidth/2);

	    if(top > (containerHeight - handlerHeight - 10)){top = containerHeight - handlerHeight - 10}
	    if(top < 10){top = 10}
	    if(left > (containerWidth - handlerWidth - 10)){left = containerWidth - handlerWidth - 10}
	    if(left < 10){left = 10}
	    
	    $(".frame").css("top", top+"px")
	    $(".frame").css("left", left+"px")

	}

	endMove = function(e){
	    e.preventDefault();
	    $(document).off('mouseup touchend', endMove);
	    $(document).off('mousemove touchmove', move);
	    $(document).off('mousemove touchmove', resizeHandler);
	};

	resizeStart = function(e){
		console.log("mouse down")
		global_resizeStartPosX = (e.clientX || e.pageX || e.originalEvent.touches[0].clientX) + $(window).scrollLeft();
		global_resizeStartPosY = (e.clientY || e.pageY || e.originalEvent.touches[0].clientY) + $(window).scrollTop();
		var frame = $(".frame");
	    global_frameHeight = frame.height();
	    global_frameWidth = frame.width();
	    global_frameLeft = parseInt(frame.css("left"));
	    global_frameTop = parseInt(frame.css("top"));
		var handler = $(".frame");
		var handlerHeight = handler.height();
		var handlerWidth = handler.width();
		var handlerRation = handlerWidth/handlerHeight;
		globalHandlerRatio = handlerRation;
		global_frameCenterY = frame.offset().top+(global_frameHeight/2)
		global_frameCenterX = frame.offset().left+(global_frameWidth/2)
	}

	resizeHandler = function(e){
		var container = $(".imageCropper .image");
		var containerWidth = container.width();
		var containerHeight= container.height();

	    var XResizedDistance = Math.abs(e.pageX - global_frameCenterX);
	    var YResizedDistance = Math.abs(e.pageY - global_frameCenterY);

	    var biggerDistance = Math.sqrt(YResizedDistance*YResizedDistance + XResizedDistance*XResizedDistance);

	    var distanceWithRationX = (biggerDistance)*2;
	    var distanceWithRationY = (biggerDistance/globalHandlerRatio)*2;

	    if(distanceWithRationX > containerWidth){return ;}
	    if(distanceWithRationY > containerHeight){return ;}

	    var prevWidth = parseInt($(".frame").css("width"));
	    var prevHeight = parseInt($(".frame").css("height"));

	    $(".frame").css("width", (distanceWithRationX)+"px");
	    $(".frame").css("height", (distanceWithRationY)+"px");

	    var frame = $(".imageCropper .image");
		var frameWidth = frame.width();
		var frameHeight= frame.height();

	    var frameCenterX = global_frameLeft + global_frameWidth/2;
	    var frameCenterY = global_frameTop + global_frameHeight/2;

	    var prevLeft = parseInt($(".frame").css("left"));
	    var prevTop = parseInt($(".frame").css("top"));

	    $(".frame").css("left", frameCenterX - (distanceWithRationX)/2+"px");
	    $(".frame").css("top",  frameCenterY - (distanceWithRationY)/2+"px");

	    var sizeIncorrect = false;
	    if($(".frame").offset().left <= (container.offset().left + 10)){sizeIncorrect = true; }
	    if($(".frame").offset().top <= (container.offset().top + 10)){sizeIncorrect = true; }
	    if(($(".frame").offset().left + $(".frame").width()) >= (container.offset().left + container.width() + 10)){sizeIncorrect = true; }
	    if(($(".frame").offset().top + $(".frame").height()) >= (container.offset().top + container.height() + 10)){sizeIncorrect = true; }
	    if(sizeIncorrect){
	    	$(".frame").css("width", prevWidth+"px");
		    $(".frame").css("height", prevHeight+"px");
	    	$(".frame").css("left", prevLeft+"px");
		    $(".frame").css("top",  prevTop+"px");
	    }
	}

	crop = function(){
	    var crop_canvas = document.createElement('canvas');
	    var width = $(".frame").width()*global_biggerRation;
	    var height = $(".frame").height()*global_biggerRation;
	    var imagewidth = $(".image").width()*global_biggerRation;
	    var imageheight = $(".image").height()*global_biggerRation;
	    var left = parseInt($(".frame").css("left"));
	    left = left*global_biggerRation;
	    var top = parseInt($(".frame").css("top"));
	    top = top*global_biggerRation;
	    crop_canvas.width = 600;
	    crop_canvas.height = 300;
	    console.log("left is",left)
	    console.log("top is",top)
	    crop_canvas.getContext('2d').drawImage(imageElem[0], left, top, 600, 300, 0, 0, 600, 300);
	    window.open(crop_canvas.toDataURL("image/png"));
	}

	var imageChange = function(this_){
		var reader = new FileReader();
	    var image  = new Image();
		var imageFile = $(this_)[0].files[0];

		image_target = imageFile;

	    reader.readAsDataURL(imageFile); 
	    reader.onload = function(_file) {
	        image.src    = _file.target.result;              // url.createObjectURL(file);
	        image.onload = function() {
	            var w = this.width,
	                h = this.height,
	                t = imageFile.type,                           // ext only: // imageFile.type.split('/')[1],
	                n = imageFile.name,
	                s = ~~(imageFile.size/1024) +'KB';
				drawImageOnWindow(this);
	        };
	        image.onerror= function() {
	            alert('Invalid file type: '+ imageFile.type);
	        };      
    	};

	    
		console.log(image);
	}