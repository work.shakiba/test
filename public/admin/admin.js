var emptyMessage = "<div class=\"empty-message\" id=\"loading\"> <div> <span class=\"logo\"></span> <span class=\"text\">محتویاتی برای نمایش وجود ندارد</span></div>"

//region main view controller
	var getFlaggedItems = function(type_){
        var type = type_
        $(".menu .active").removeClass("active")
        $(".menu [type="+type+"]").addClass("active")
        $(".item_div").find(".item").remove()
        httpRequest("/adminAPI/fetch", {type: type}, function(data){
            say("result in", data)
			var result = data.result
            var temps = []
			for(i in result){
                temps.push(decideWhatKindOfTemplateIsNeeded(result[i]))
			}
            gridify($(".item_div"), 300)
            gridifyAppendWithBalance($(".item_div"), temps)
		})
	}
//endregion
//region item construct

var decideWhatKindOfTemplateIsNeeded = function(item_){
    if(item_.item_userid){
        return construct(item_)
    }else if(item_.body){
        return commentConstruct(item_)
    }else if(item_.name){
        return profileConstruct(item_)
    }
}

var construct = function(item){
    // type
    if(item.attach.type == 'link'){
        var extra = (
            '<div class="extra">'+
            '<a class="title" href="'+item.attach.url+'" target="_blank">'+item.attach.title+'</a>'+
            '<span class="desc" target="_blank">'+item.attach.desc+'</span>'+
            '</div>'
        );
    }else if(item.attach.type == 'image'){
        var ratio = parseInt(item.attach.dimentions);
        var width = 300;
        var height = 300/ratio;
        // return "";
        var extra = (
            "<div class=\"extra\">"+
            "<img class=\"image\" src=\"/upload/items/"+item.attach.image+"\" width=\""+width+"\" height=\""+height+"\" />"+
            "</div>"
        );
    }else if(item.attach.type == 'video'){
        var extra = (
            '<div class="extra">'+item.attach.iframe+'</div>'
        );
    }else{
        var extra = '';
    }
    // user info
    var thumbnail = '/upload/users/'+item.item_userid.thumbnail;
    var username = item.item_userid.name;
    var user_href='href="/profile/'+item.item_userid._id+'/posts"';
    // comment
    if(item.comment == 0){var comment = 'write'}else{var comment = 'message'}
    // template
    var date = (item.sort_date ? get_time(item.sort_date) : item.date.split(" ")[0])
    var template = $(
        '<div class="item '+item._id+'" item-id="'+item._id+'" item-user-id="'+item.item_userid._id+'">'+
        '<div class="header">'+
        '<a '+user_href+' q-profile="true">'+
        '<img src="'+thumbnail+'" width="100%" height="100%">'+
        '<span class="user" user-id="'+item.item_userid._id+'">'+username+'</span>'+
        '<span class="time">'+date+'</span>'+
        '</a>'+
        '<button class="more" onclick=itemExtraViewController.itemMenu(this,\''+item._id+'\')></button>'+
        '<button class="like" onclick=itemExtraViewController.like(this)>'+item.like+'</button>'+
        '</div>'+
        extra+
        '<div class="main">'+
        '<span class="body">'+item.body+
        '<span class="remaining"></span>'+
        '</span>'+
        '<span class="reveal" onclick="$(this).parents(\'.main\').toggleClass(\'active\')">ادامه</span>'+
        '</div>'+
        '<div class="comment_div '+comment+'" post-id="'+item._id+'" creator-id="'+item.item_userid._id+'">'+
        '<button class="comment_message" onclick=itemExtraViewController.getComments(this) count="'+item.comment+'"><span>'+item.comment+'</span> نظر ثبت شده</button>'+
        '<div class="comment_list"></div>'+
        '<div class="in_response"></div>'+
        '<div class="comment_write">'+
        '<input type="text" placeholder="نظر خود را بنویسید..." onfocus=itemExtraViewController.commentOnFocus(this) />'+
        '<textarea type="text" placeholder="نظر خود را بنویسید..." onblur=itemExtraViewController.commentOnBlur(this) ></textarea>'+
        '<button class="comment_button" onclick=itemExtraViewController.sendComment(this)></button>'+
        '</div>'+
        '</div>'+

            "<div>"+
                "<span>flag: "+item.flag+" -- flag-24: "+item.flag_24+", safe: "+item.safe+"</span>"+
                "<button class=clear>set as safe: 1</button>"+
                "<button class=flag_hard>set as safe: -1</button>"+
                "<button class=flag_soft>set as safe: 0</button>"+
                "<button class='sustain'>sustain user</button>"+
                "<button class='blocked'>block user</button>"+
            "</div>"+

        '</div>'
    );

    if(item.allow_comment != true){
        template.find(".comment_div").hide()
    }

    template.find(".clean").click(function(){
        flag.setItemAs(item._id, 1)
    })

    template.find(".flag_soft").click(function(){
        flag.setItemAs(item._id, 0)
    })

    template.find(".flag_hard").click(function(){
        flag.setItemAs(item._id, -1)
    })

    template.find(".sustain").click(function(){
        flag.setUserAs(item.item_userid._id, "sustain")
    })

    template.find(".block").click(function(){
        flag.setUserAs(item.item_userid,_id, "block")
    })

    return template;
}

var commentConstruct = function(item){
    // user info
    var thumbnail = '/upload/users/'+item.commentor.thumbnail;
    var username = item.commentor.name;
    var user_href='href="/profile/'+item.commentor._id+'/posts"';
    // template
    var date = (item.sortDate ? get_time(item.sortDate) : item.date.split(" ")[0])
    var template = $(
        '<div class="item" comment-id="'+item._id+'">'+
        '<div class="body">'+
        item.body+
        '</div>'+

            "<div>"+
                "<span>flag: "+item.flag+"</span>"+
                "<button class=delete>delete</button>"+
                "<button class=clear>clear</button>"+
            "</div>"+

        '</div>'
    );

    template.find(".clear").click(function(){
        flag.setItemAs(item._id, 1)
    })

    template.find(".delete").click(function(){
        flag.setItemAs(item._id, 0)
    })

    return template;
}

var profileConstruct = function(item){
    if(!item.user) return ""
    var brief = (!item.brief ? "" : item.brief);
    var template = $(
    '<div class="item '+item._id+'" user-id="'+item._id+'">'+
    '<div class="mini_profile">'+
    '<div class="more"><button onclick="profileViewController.menu(this)"></button></div>'+
    '<a href="/profile/'+item._id+'/posts">'+
    '<img src="/upload/users/'+item.thumbnail+'" />'+
    '<span class="name">'+item.name+'</span>'+
    '<span class="brief">'+brief+'</span>'+
    '</a>'+
    '<div class="stat_div">'+
    '<span><span>'+item.following+'</span>دنبال میکند</span>'+
    '<span><span>'+item.followers+'</span>دنبال کننده</span>'+
    '<span><span>'+item.posts+'</span>پست ها</span>'+
    '</div>'+
    '</div>'+
    '<div>'+
        '<span>flag:'+item.user.flag+'</span>'+
    '</div>'+
    '<div>'+
        '<button class=clean >clean</button>'+
        '<button class=sustain >sustain</button>'+
        '<button class=block >block</button>'+
    '</div>'+
    '</div>'
    );

    template.find(".clean").click(function(){
        flag.setUserAs(item._id, "clean")
    })

    template.find(".sustain").click(function(){
        flag.setUserAs(item._id, "sustain")
    })

    template.find(".block").click(function(){
        flag.setUserAs(item._id, "block")
    })

    return template;
}
//endregion

//region flag module
    flag = {

        setItemAs: function(itemID_, safe_){
            var data = {
                id: itemID_,
                safe: safe_
            }
            httpRequest("/adminAPI/set-item-as", data, function(model_){
                say("flag done")
            });
        },

        setUserAs: function(itemID_, type_){
            var data = {
                id: itemID_,
                type: type_
            }
            httpRequest("/adminAPI/set-profile-as", data, function(model_){
            });
        },

    }
//endregion

//on ready
	window.onload = function(){
        getFlaggedItems("item-dirty-24")
	}

