// "use strict";
/**
 * adapter makes it easy to add, change and handle items with templates, loading
 * adapter stores item in the ram while page is not closed
 * fixme: use animation for adding and removing items
 * @constructor
 */
var Adapter = function(){
    this.itemsArray = {main: []}
    this.template = false
    this.parent = false
    this.itemDiv = false
    this.gridify = false
    this.onOverScroll = false
    this.constructView = false
    this.fetchCap = false // if this value is true, means a call to server is in progress, must wait
    this.reachedBedRock = {} // if this value is true, then it means this type has received all the items and loading more on this type should be disabled
    this.currentType = "main"
    this.url = false
    this.ajaxData = {}
    this.rawAjaxResult = {}
    this.onComplete = function(){}
    this.viewOffset = false
    this.limit = false
    this.loading = false
    this.loaderTemplate = false // to cache and use it faster, loader uses svg which is kinda heavy
    this.bottomLoader = false
    this.mainLoader = false
    this.disableCache = false
    this.emptyMessage = ""
}

/**
 *
 * @param parent_
 * @param config_ -> Object gridify: Boolean, gridifyWidth: Number, gridifyMethod: Function
 */
Adapter.prototype.init = function(parent_, config_){
    this.parent = parent_
    if(this.parent.length == 0) throw new Error("parent div was not found!")
    this.itemDiv = parent_.find("[item-div]")
    if(this.itemDiv.length == 0) throw new Error("item div was not found!")
    // clear the view
    this.clearItemdiv()
    // check for config_
    if(config_ && config_.gridify === true){
        this.gridify = true
        if(!config_.gridifyWidth) throw new Error("gridify width not defined")
        // if(!config_.gridifyMethod) throw new Error("gridify method ")
        gridify(this.itemDiv, config_.gridifyWidth)
    }
    // bind to scroll, to detect scroll to bottom
    var self = this;
    $(window).on('scroll', function() {
        if($(window).scrollTop() + $(window).height() >= $(document).height()-200) {
            if(self.onOverScroll) self.fetch()
        }
    })
    this.parent.on('scroll', function() {
        if($(this).scrollTop() + $(this).innerHeight() >= this.scrollHeight - 200) {
            if(self.onOverScroll) self.fetch()
        }
    })
    window.onresize = function(){
        // check if the item_div is gridified and the gridified columns has changed
        if(this.gridify && gridify(self.itemDiv, config_.gridifyWidth)){
            self.viewOffset = 0
            self.fetch()
        }
    }
    if(!this.limit) this.limit = 20
    this.viewOffset = 0
}

/**
 * this methods check local array for the type and if exists, pulls it from ram
 * otherwise fetch it from server by the url and date
 * if data is fetched from server, beginLoading is called to show a small animation
 * if data is fetched from server, data is then stored in the itemsArray
 *
 * @param url_ -> the url from which to fetch data from server
 * @param data_ -> data to send to server via ajax
 * @param type_ -> type of data, it's used to categories different set of data
 * @returns {*}
 */
Adapter.prototype.fetch = function(url_, data_, type_){
    if(url_) this.url = url_
    if(this.fetchCap) return
    if(type_ && type_ != this.currentType) this.clearItemdiv()
    if(type_) this.currentType = type_
    this.fetchCap = true
    var self = this
    if(!data_){
        data_ = this.ajaxData
    }
    var offset = (!self.itemsArray[this.currentType] ? 0 : self.itemsArray[this.currentType].length)
    data_["offset"] = offset
    this.ajaxData = data_
    // check if items exist in the local array
    var localResult = this.localFetch()
    if(localResult){
        setTimeout(function(){self.fetchCap = false}, 500)
        if(self.onComplete) self.onComplete(self.rawAjaxResult); self.onComplete = false
        return self.render()
    }
    // check if we have reached the bed rock -> if true, dont get anything from server
    if(this.checkIfReachedBedRock()){
        // render for empty message
        this.render()
        return this.fetchCap = false
    }
    // if the local fetch is not available, begin the loader
    this.beginLoader()
    httpRequest(this.url, this.ajaxData, function(model_){
            self.endLoader()
            self.rawAjaxResult = model_
            setTimeout(function(){self.fetchCap = false}, 500)
            if(self.onComplete) self.onComplete(self.rawAjaxResult); self.onComplete = false
            self.insertToLocal(model_.result)
            self.render()
    })
}

/**
 * the method gets the items from itemsArray
 * constructs the view and append them to itemDiv
 * after appending, inc the viewOffset
 * @param offset_
 * @param limit_
 */
Adapter.prototype.render = function(offset_, limit_){
    console.log("adapter init")
    if(!this.constructView) throw Error("constructView method in not defined!")
    var slice = this.localFetch()
    if(slice === null) slice = []
    var width = gridifyGetWidthOfColumnInDiv(this.itemDiv)
    // clear the itemDIv
    var itemTemplates = []
    for(var i in slice){
        var item = slice[i]
        var temp = this.constructView(item, width)
        itemTemplates.push(temp)
        this.viewOffset++
        if(!this.gridify){
            this.itemDiv.append(temp)
        }
    }
    // append the empty message
    if(this.viewOffset == 0){
        this.itemDiv.prepend(this.emptyMessage)
    }else{
        this.itemDiv.find("#empty").remove()
    }
    if(this.gridify){
        gridifyAppendWithBalance(this.itemDiv, itemTemplates)
    }
}

/**
 * this method removes all the items inside a itemDiv
 */
Adapter.prototype.clearItemdiv = function(){
    this.viewOffset = 0
    this.itemDiv.find("*").not(".column").remove()
}

//Adapter.prototype.append = function(itemsArray_){
//    if(!this.constructView) throw Error("constructView method in not defined!")
//    for(var i in this.itemsArraym){
//        var item = this.itemsArray[i]
//        var temp = this.constructView(item)
//        this.itemDiv.append(temp)
//    }
//}

/**
 * check if items are available from local store by the currentType, limit and viewOffset
 * if items exist, then return the items
 * else, return null
 * @returns {*}
 */
Adapter.prototype.localFetch = function(){
    if(!this.itemsArray[this.currentType]) return null
    if(this.viewOffset == this.itemsArray[this.currentType].length) return null
    var slice = this.itemsArray[this.currentType].slice(this.viewOffset, this.viewOffset+this.limit)
    return slice
}

/**
 * insert fetched data from server to itemsArray under the currentType
 * @param data_
 */
Adapter.prototype.insertToLocal = function(data_){
    if(!this.itemsArray[this.currentType]){
        this.itemsArray[this.currentType] = data_
    }else{
        this.itemsArray[this.currentType] = this.itemsArray[this.currentType].concat(data_)
    }
    // check for reachedBedRock
    if(data_ && data_.length < this.limit) this.reachedBedRock[this.currentType] = true
}

Adapter.prototype.checkIfReachedBedRock = function(){
    return this.reachedBedRock[this.currentType]
}


Adapter.prototype.beginLoader = function(){
    if(this.loading) return
    this.loading = true
    //add the big loader
    if(this.viewOffset == 0){
        this.loaderTemplate = View.getTemplateByID("loader")
    // add the small loader
    }else{
        this.loaderTemplate = View.getTemplateByID("loader").addClass("small relative")
    }
    this.itemDiv.append(this.loaderTemplate)
}

Adapter.prototype.endLoader = function(){
    this.loaderTemplate.remove()
    this.loading = false
}

/**
 * sets an interval to clear the type
 * time is in minutes
 * @param main_
 */
Adapter.prototype.clearInterval = false
Adapter.prototype.setClearTypeInterval = function(interval_){
    // check for valid interval
    if(interval_ > 60) throw new Error("interval too long")
    // check if previously interval has been set
    if(this.clearInterval != false) clearInterval(this.clearInterval)
    var self = this
    this.clearInterval = setInterval(function(){
        self.clearTypesNow()
    }, interval_* 1000 * 60)
}

/**
 * this method removes all the items in each type
 * @param main_
 */
Adapter.prototype.clearTypesNow = function(){
    this.itemsArray = {"main": []}
    this.reachedBedRock = {}
}