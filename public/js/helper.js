//validate form
validateForm = function(parent_){
    say("validate form")
    var return_ = true;
    $.each(parent_.find("input, textarea"), function(){
        elem = $(this)
        var value = elem.val();
        if((elem.attr('type') == "number") && isNaN(parseInt(value)) && (value != "")){
            tooltip({element: elem, message:'Value must be number'});
            return_ = false;
            return false;
        }else if(elem.attr('required')){
            if(value.length < elem.attr('min')){tooltip({element: elem, message: 'حداقل کاراکتر مجاز '+elem.attr('min')+" است!"}); return_ =false; return false;}
            if(value.length > elem.attr('max')){tooltip({element: elem, message: 'حداکثر کاراکتر مجاز '+elem.attr('max')+" است!"}); return_ =false; return false;}
        }
    });
    return return_;
}

// get param by name
function getParameterByName(url, name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(url);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

//tooltip
tooltip = function(obj_){
    say("tooltip")
    var identifier = makeId();
    $('.tooltip').remove();
    var node = $("<div/>", {"class": "tooltip "+identifier, "text": obj_.message});
    obj_.element.parent().css("position","relative").append(node);
    $('.tooltip.'+identifier).css("top", obj_.element.parent().height() + 15 + "px")
    var timeout = (obj_.timeout ? obj_.timeout : 3000)
    setTimeout(function(){
        $('.tooltip.'+identifier).remove()
    }, timeout);
}
betterTooltip = function(element_){
    var identifier = makeId();
    $('.tooltip').remove();
    var node = $("<div/>", {"class": "tooltip "+identifier, "text": element_.attr("tooltip")});
    element_.parent().css("position","relative").append(node);
    node.css("top", element_.parent().height() + 15 + "px")
    node.css("left", element_.position().left + element_.width()/2 - node.width()/2 + "px")
    setTimeout(function(){
        $('.tooltip.'+identifier).remove()
    },5000);
}

removeBetterTolltip = function(element_) {
    element_.parent().find(".tooltip").remove();
}

tooltipListen = function(dom_) {

    $(document).on("mouseover", "[tooltip]", function(){
        betterTooltip($(this));
    })

    $(document).on("mouseleave", "[tooltip]", function(){
        removeBetterTolltip($(this));
    })

}
//makeId
makeId = function(){
    var text = "id";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for( var i=0; i < 10; i++ ){
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
}
//localstorage
store = function(key_, value_){
    if(!value_){
        return localStorage.getItem(key_);
    }else{
        localStorage.setItem(key_, value_)
    }
}
//log
var log = function(flag_, message_, debugMode_){
    if(debugMode_ === false){return false}
    if(!message_){
        message_ = "NULL";
    }
    console.log('(---'+flag_+'---) =>', message_);
}
say = log;
//centerVertical
var centerVertical = function(section_){
    var margin =  ((window.innerHeight - section_.height())/2);
    if(margin < 50){margin = 50}
    section_.css('margin',margin+'px auto')
}
//gridify
/**
 * 1 -> single
 * 2 -> two single
 * 3 -> multi single
 * @param type_
 */
var setGridifyType = function(type_){
    store("gridify-type", type_)
}

var gridify = function(section_,itemWidth_){
    var itemWidth = itemWidth_;
    var itemPerRow = Math.floor(window.innerWidth/itemWidth);
    if(itemPerRow == 0){itemPerRow = 1;}
    var gridifyType = store("gridify-type")
    if(!gridifyType) gridifyType = itemPerRow
    if(gridifyType == 1 && itemPerRow > 1) itemPerRow = 1
    if(gridifyType == 2 && itemPerRow > 2) itemPerRow = 2
    if(section_.attr('item-per-row') == itemPerRow){return false;}
    section_.html('')
        .attr('item-per-row', itemPerRow);
    for (var i = itemPerRow; i >= 1; i--) {
        section_.append('<div class="column column_'+i+'"></div>');
        section_.find('.column').css('width', (100/itemPerRow)+'%');
    };
    return true;
};

var gridifyGetWidthOfColumnInDiv = function(itemDiv_){
    gridify(itemDiv_, 290)
    var ration = itemDiv_.find('.column_1').width()
    //console.log("width is", itemDiv_.find('.column_1').width())
    if(ration > 485) ration = 485
    return ration
}

var gridifyAppendWithBalance = function(gridifiedDiv_, itemsArray_, config_){
    // check if parent has been gidified
    var itemPerRow = gridifiedDiv_.attr('item-per-row')
    // enable balancing only if itemPerRow is more than 1
    // enable balancing after the 2/3 of items have been added
    var oneThirdOfItems = Math.floor(itemsArray_.length/3)
    if(itemPerRow == 1) oneThirdOfItems = 0
    // normal append
    var currentColumn = 1
    for (var i = 0; i < itemsArray_.length - oneThirdOfItems; i++) {
        // get the smallest height
        var item = itemsArray_[i]
        currentColumn++
        if(currentColumn > itemPerRow) currentColumn = 1
        gridifiedDiv_.find('.column_'+currentColumn).append(item)
    }
    // balanced append
    // heights
    var heights = []
    for(var i = 0; i < itemPerRow; i++){
        heights.push(gridifiedDiv_.find('.column_'+(i+1)).height());
    }
    // return
    for (var i = itemsArray_.length - oneThirdOfItems; i < itemsArray_.length; i++) {
        var item = itemsArray_[i]
        if(!item)return
        // get the smallest height
        var currentColumn = 0
        var minHeight = heights[0]
        for (var j in heights) {
            if(heights[j] <= minHeight){
                minHeight = heights[j]
                currentColumn = j
                currentColumn++
            }
        }
    	// say("apend with balance  "+currentColumn, heights)
        gridifiedDiv_.find('.column_'+currentColumn).append(item)
        currentColumn--
        heights[currentColumn] += item.height()
    }
}
//clean text
var cleanText = function(preText_){
    var cleanText = preText_.replace(/(?:\r\n|\r|\n)/g, '<br />').replace(/"/g, '\"');
    return cleanText;
}
//auto image upload
var autoImageUpload = function(){
    $("body").on("click","*[auto-upload-button]",function(){
        var parent = $(this).parent();
        parent.find("input[type=file]").click();
        parent.find("input[type=file]").on("change", function(){
            uploadImage($(this));
        })
    })
}

var uploadImage = function(image_){
    if(!u_image_validation(image_)){return 0;};
    image_.parent().find("img").css('background','url('+URL.createObjectURL(image_[0].files[0])+') 50% 50%').css("background-size","cover");
    var formData = new FormData();
    formData.append('image', image_[0].files[0]);
    formData.append('name', name);
    $.ajax({ type: 'POST', url: base_url+"api/"+image_.attr("upload-url"), data: formData, processData: false, contentType: false,
        success:function(data){
            image_.attr("image-name",data.image);
        },
    });
}
//image validation
u_image_validation = function(file_){
    if(file_.val() != ''){
        var type = file_.val().split('.').pop().toLowerCase();
        if($.inArray(type, ['gif','png','jpg','jpeg']) == -1) {
            tooltip({element: file_, message: messages['image_mis_type'],identifier: 'error'})
            return 0;
        } else if(file_[0].files[0].size > 500000){
            tooltip({element: file_, message: messages['image_too_large'],identifier: 'error'})
            return 0;
        }
        return 1;
    }else{
        return 0;
    }
}
//get time
get_time = function(date){

    var clientNow = Date.now()/1000;
    var itemTime = date/1000;

    var timeDeffrence = clientNow - itemTime;

    var timeDeffrenceInMinute = Math.floor(timeDeffrence/60);

    var timeDeffrenceInHour = Math.floor(timeDeffrenceInMinute/60);

    var timeDeffrenceInDay = Math.floor(timeDeffrenceInHour/24);

    if(timeDeffrenceInMinute < 59){
        return (timeDeffrenceInMinute+' دقیقه پیش');
    }else if(timeDeffrenceInHour < 23){
        return (timeDeffrenceInHour+' ساعت پیش');
    }else if(timeDeffrenceInDay < 31){
        return (timeDeffrenceInDay+' روز پیش');
    }else if(timeDeffrenceInDay >= 30){
        var months = Math.floor(timeDeffrenceInDay/30)
        return (months+' ماه پیش');
    }

}

var time_now = function(){
    d = new Date;
    return (d.getFullYear()+'-'+d.getMonth()+'-'+d.getDate()+' '+d.toTimeString().split(' ')[0]);
}
//toast
toast = function(message_,type_){
    var flasher_id = makeId();
    if($('.flasher_div').length == 0){
        $("body").append("<div class=flasher_div></div>")
    }
    $('.flasher_div').append('<span class="flasher '+flasher_id+' '+type_+'"><span class="body">'+message_+'</span></span>');
    setTimeout(function(){
        $('.flasher.'+flasher_id)
            .addClass('flasher_out')
            .bind("animationend webkitAnimationEnd oanimationend MSAnimationEnd", function(){
                $('.flasher.'+flasher_id).remove().unbind();
            });
    }, 5000);
    $('.flasher.'+flasher_id).click(function(){
        $('.flasher.'+flasher_id)
            .addClass('flasher_out')
            .bind("animationend webkitAnimationEnd oanimationend MSAnimationEnd", function(){
                $('.flasher.'+flasher_id).remove().unbind();
            });
    })
}
//scroll to bottom of div
scrollToBottom = function(div_){
    div_[0].scrollTop = div_[0].scrollHeight;
}
//empty message
$.prototype.isEmpty = function(isEmpty_){
    if(isEmpty_ && this.find(".empty_message").length == 0){
        this.append("<span class=empty_message>Sorry, No result(s) found!</span>")
    }else if(!isEmpty_ && this.find(".empty_message").length == 1){
        this.find(".empty_message").remove();
    }
}
//detect CSS Feature
var isAnimationAvailable = function(){
    var animation = false, animationstring = 'animation', keyframeprefix = '', domPrefixes = 'Webkit Moz O ms Khtml'.split(' '), pfx = '', elm = document.createElement('div'); if( elm.style.animationName !== undefined ) { animation = true; } if( animation === false ) { for( var i = 0; i < domPrefixes.length; i++ ) { if( elm.style[ domPrefixes[i] + 'AnimationName' ] !== undefined ) { pfx = domPrefixes[ i ]; animationstring = pfx + 'Animation'; keyframeprefix = '-' + pfx.toLowerCase() + '-'; animation = true; break; } } }
    return animation;
}

function detectCSSFeature(featurename){
    var feature = false,
        domPrefixes = 'Webkit Moz ms O'.split(' '),
        elm = document.createElement('div'),
        featurenameCapital = null;

    featurename = featurename.toLowerCase();

    if( elm.style[featurename] !== undefined ) { feature = true; }

    if( feature === false ) {
        featurenameCapital = featurename.charAt(0).toUpperCase() + featurename.substr(1);
        for( var i = 0; i < domPrefixes.length; i++ ) {
            if( elm.style[domPrefixes[i] + featurenameCapital ] !== undefined ) {
                feature = true;
                break;
            }
        }
    }
    return feature;
}
//bind to animate
var bindToAnimate = function(elem, callback){
    if(isAnimationAvailable()){
        elem.bind("animationend webkitAnimationEnd oanimationend MSAnimationEnd", function(e){
            $(this).unbind("animationend webkitAnimationEnd oanimationend MSAnimationEnd")
            callback(this);
        })
    }else{
        callback(false);
    }
}

var bindToAnimation = function(elem, callback){
    if(isAnimationAvailable()){
        elem.bind("animationend webkitAnimationEnd oanimationend MSAnimationEnd", function(e){
            $(this).unbind("animationend webkitAnimationEnd oanimationend MSAnimationEnd")
            callback(true, elem);
        })
    }else{
        callback(false, elem);
    }
}
var bindToTransition = function(elem, callback){
    if(isAnimationAvailable()){
        elem.bind("transitionend webkitTransitionEnd otransitionend MSTransitionEnd", function(e){
            $(this).unbind("transitionend webkitTransitionEnd otransitionend MSTransitionEnd")
            callback(true, elem);
        })
    }else{
        callback(false, elem);
    }
}
//press enter
var listenToEnter = function(){
    $("[listenToKey=true] input").keypress(function(e){
        say("pressed")
        if(e.keyCode == 13){
            $(this).parents("[listenToKey=true]").find("button").first().click();
        }
    })
}
//bind
function DataBinder(object_id) {
    // Use a jQuery object as simple PubSub
    var pubSub = $({});

    // We expect a `data` element specifying the binding
    // in the form: data-bind-<object_id>="<property_name>"
    var data_attr = "bind-" + object_id,
        message = object_id + ":change";

    // Listen to change events on elements with the data-binding attribute and proxy
    // them to the PubSub, so that the change is "broadcasted" to all connected objects
    $(document).on("change", "[data-" + data_attr + "]", function(evt){
        var $input = $(this);
        pubSub.trigger(message, [$input.data(data_attr), $input.val()]);
    });

    // PubSub propagates changes to all bound elements, setting value of
    // input tags or HTML content of other tags
    pubSub.on( message, function( evt, prop_name, new_val ) {
        $( "[data-" + data_attr + "=" + prop_name + "]" ).each( function() {
            var $bound = $( this );

            if ( $bound.is("input, textarea, select") ) {
                $bound.val( new_val );
            } else {
                $bound.html( new_val );
            }
        });
    });

    return pubSub;
}
//logs
var log_ = {
    e: function(flag_, message_){
        this.log(flag_, message_, "color: #999;")
    },
    w: function(flag_, message_){
        this.log(flag_, message_, "color: #00a3d9;")
    },
    a: function(flag_, message_){
        this.log(flag_, message_, "color: #ff4d4d;")
    },
    log: function(flag_, message_, style_){
        if(!message_) message_ = ""
        if(!style_) style_ = "color: #999;"
        console.log("%c "+flag_, style_, message_)
    }
}

