// for IE, to get the windows origin
if(!window.location.origin){window.location.origin = window.location.protocol + '//' + window.location.host}
/**
 * fixme: detect base url
 * @param map_
 * @returns {{init: (*|init), processURL: Function, setBaseUrl: Function, pushState: Function}}
 * @constructor
 */
var Router = function(){

	// debug, to output the log info to console
	this.debug = false
    // if set to true, you cannot re initialize the router
	this.initilized = false
    // if the popstate event has been fired, it won't be pushed to history again, to avoid loop
	this.popstated = false
    // this is the current valid url
	this.processedURL = false
    // this is the url->handler map
    this.map = []
    // this is the same as map, the difference is that routes in here won't get pushed to history
    this.looseMap = []
}

/**
 *
 * @param map_
 */
Router.prototype.init = function(map_, looseMap_){
	if(this.initilized === true) throw new Error("router already initialized on this function")
	this.initilized = true
	// if map is not defined, throw error
	if(undefined === map_) throw new Error("map is not defined")
	if(looseMap_) this.looseMap = looseMap_
	this.map = map_
	this.listenToAnchorCLick()
	this.listenToPopState()
    this.prepURL(document.URL)

}

/**
 * run only once, to start listening on all body <a> tag clicks
 */
Router.prototype.listenToAnchorCLick = function(){
	var self = this;
	$("body").on("click", "a", function(e) {
		e.preventDefault();
		if($(this).attr('target') != '_blank'){
			if($(this).attr('href')){
				self.prepURL($(this).attr('href'));
			}
		}
	});
}

/**
 * preparing the url for analyze
 * @param url_
 */
Router.prototype.prepURL = function(url_){
	// remove the last slash from url
	if(url_.substr(url_.length - 1) == "/"){url_ = url_.substring(0, url_.length - 1);}
	// remove the origin of the url
	url_ = url_.replace(window.location.origin , '')
	// split url by "/"
	url_ = url_.split("/")
    url_.splice(0, 1)
    if(url_.length == 0) url_ = [""]
	//this.preparedURL = url_
	this.analyzeURL(url_);
}

//Router.prototype.analyzeURL = function(urlArray_){
//
//	var urlParam_ = URLParam;
//	var variables = {};
//	var handler = false;
//
//	for(i in this.map){
//		var thisMapURL = thisMapURL[i]
//		var tempProcessedURL = ""
//		var tempExtractedVariables = {}
//		var count = 0
//		for(j in thisMapURL){
//			count++
//			if(!urlArray_[j]) break
//			var thisURLPart = urlArray_[j]
//			// if thisMapURL is string -> check for exact match
//			if(thisMapURL[j] === thisURLPart || thisMapURL[j].indexOf(thisURLPart) != -1 || thisMapURL[j] == false){
//				tempProcessedURL += thisMapURL[j]
//				tempExtractedVariables[j] = thisMapURL[j]
//			}else{
//				break
//			}
//		}
//		// check if there's a match
//		if(count === urlArray_.length){
//			handler = i
//			this.processedURL = tempProcessedURL
//			variables = tempExtractedVariables
//            this.pushState()
//			return handler(variables)
//		}
//	}
//    // if not match is found
//    helper.log("no match url found")
//}

/**
 * push current state to history
 */
Router.prototype.pushState = function(){
	if(this.popstated){
		this.popstated = false;
		return;
	}
	var finalURL = window.location.origin+"/"+this.processedURL;
	if(history.pushState){window.history.pushState(null, finalURL, finalURL)};
}

/**
 * listens to popstate event
 */
Router.prototype.listenToPopState = function(){
	var self = this
	window.addEventListener('popstate', function(event) {
		self.popstated = true;
		self.prepURL(document.URL);
	});
}

Router.prototype.analyzeURL = function(urlParam_){
    var handler
    var allMap = [this.map, this.looseMap]
	var push = false
    for(var I in allMap){
    	var map = allMap[I]
		if(I == 0){push = true}else{push = false}
	    for(var i in map){
	        var variables = {}
	        var processedURL = []
	        var mapMemberURL = i.split("/")
	        mapMemberURL.splice(0,1)
	        var mapMemberHandler = map[i]
	        // check for each part to see if url member matches the url
	        for(var i in mapMemberURL){
	            var segmentOfMapMemberURL = mapMemberURL[i]
	            var segmentOfWindowURL = urlParam_[i]
	            // we check if segment of Member url has special characters containing () []
	            // if it doesn't contain any special characters
	            var regexForSpecialChars = /(\((.*?)\))|(\[(.*?)\])/g
	            var extractedValueFromMapMemberURLArray = regexForSpecialChars.exec(segmentOfMapMemberURL);
	            if(extractedValueFromMapMemberURLArray == null){
	                if(segmentOfMapMemberURL != segmentOfWindowURL){
	                    break;
	                }else{
	                    processedURL.push(segmentOfWindowURL);
	                    continue;
	                }
	            // if it does contain speecial character
	            }else{
	                // check if varable is needed
	                var isStringContainingPrathesis = /(\((.*?)\))/g.exec(segmentOfMapMemberURL)
	                if(isStringContainingPrathesis){
	                    variables[isStringContainingPrathesis[2]] = segmentOfWindowURL;
	                }
	                // if is surrounded by [] there are options that need to be checked for
	                var regexForBrackets = /(\[(.*?)\])/g
	                var isStirigContainingBrackets = regexForBrackets.exec(segmentOfMapMemberURL)
	                // this variable is to be used if
	                var StringFromBracket
	                var pushed = false
	                if(isStirigContainingBrackets != null){
	                    var arrayOfStringsInBrackets = isStirigContainingBrackets[2].split("|")
	                    var windowURLMatch
	                    for(var i in arrayOfStringsInBrackets){
	                        if(segmentOfWindowURL == arrayOfStringsInBrackets[i]){
	                            processedURL.push(segmentOfWindowURL)
	                            var isStringContainingPrathesis = /(\((.*?)\))/g.exec(segmentOfMapMemberURL)
	                            if(isStringContainingPrathesis){
	                                variables[isStringContainingPrathesis[2]] = segmentOfWindowURL;
	                            }
	                        }
	                    }
	                }else{
	                    var isStringContainingPrathesis = /(\((.*?)\))/g.exec(segmentOfMapMemberURL)
	                    if(isStringContainingPrathesis){
	                        variables[isStringContainingPrathesis[2]] = segmentOfWindowURL;
	                    }
	                    processedURL.push(segmentOfWindowURL)
	                }
	            }
	        }
	        // check if match has been found
	        if(processedURL.length == mapMemberURL.length){
	            this.processedURL = processedURL.join("/")
	            variables.URL = this.processedURL
				if(push) this.pushState()
	            handler = mapMemberHandler
	            handler(variables)
	            break
	        }
    	}
    }
    // if no match has been found
    if(!handler){
        this.prepURL("/404")
    }
}
