var send = function(argument){
	var parent = ".box";
	if(!validateForm($(parent))){return false;}
	var ajax_data = {
		password: 	$(parent).find('.pass').val(),
		key: 	$(parent).find('.key').val(),
		email: 	$(parent).find('.email').val(),
	};
	$(parent).find(".err").remove();
	$.ajax({type: 'GET', url: "/api/saveNewPassword", data: ajax_data, dataType: "json",
		success:function(data){
			if(data.result == "false"){
				$(parent).find('.pass').after("<span class=err>درخواست قابل قبول نیست، لطفا مجددا درخواست تغییر رمز عبور دهید و از لینک ارسال شده به ایمیل خود اقدام کنید.</span>")
			}else{
				window.location.replace("/");
			}
		}
	});
}
