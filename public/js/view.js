'use strict'

function HandleView(){
    this.templates = false
    this.pageID = false
    this.overlayID = false
    this.body = false
    this.pageLoading = false
    this.page = false
    this.overlay = false
    this.terminateFunc = false
    this.overlayTerminateFunc = false
    this.cover = false
    this.sidebar = false // holds a pointer to current sidebar
    this.sidebarCover = false // holds a pointer to current sidebar
    this.menuHolder = false
    this.loader = false
    this.cachedTemplates = {}
}

/**
 */
HandleView.prototype.init = function(templates_, options_){
    // assign the document
    this.body = $("body")
    // add page
    this.page = $("<section />", {page: true})
    this.body.append(this.page)
    // add overlay
    this.overlay = $("<section />", {overlay: true})
    this.body.append(this.overlay)
    // add the cover
    this.cover = $("<section />", {cover: true}).hide()
    this.body.append(this.cover)
    // add the sidebarcover
    this.sidebarCover = $("<section />", {cover: true, class: "sidebar"}).hide()
    this.body.append(this.sidebarCover)
    // get the templates
    this.templates = templates_
    // init the listeners
    this.listeners()
    // options
    // get loader template
    if(options_.loaderTemplate){
        this.loader = this.getTemplateByID(options_.loaderTemplate).clone()
        this.body.append(this.loader.hide())
    }
}

HandleView.prototype.listeners = function(){
    // self
    var self = this
    // clear overlay
    this.overlay.click(function(e){
        if(e.target == this){
            self.clearOverlay()
        }
    })
    // clear menu and sidebar
    this.cover.click(function(e){
        if(e.target == this){
            self.clearMenu()
        }
    })
    // clear sidebar
    this.sidebarCover.click(function(e){
        if(e.target == this){
            self.clearSidebar()
        }
    })
    // clear menu on scroll
    $(window).bind('scroll', function() {
        self.clearMenu()
    })
}

// fix cache problem
HandleView.prototype.getTemplateByID = function(ID_){
    var template = this.templates.find("template#"+ID_).html()
    if(!template) throw new Error("no template found for this ID")
    template = $(template)
    var parent = $("<div />", {id: ID_})
    parent.append(template)
    this.checkTemplateForSpecialActions(parent)
    return parent
}

HandleView.prototype.putPage = function(view_){
    this.page.empty()
    this.page.html(view_)
}

HandleView.prototype.requestPage = function(templateID_, loading){
    // terminate the old page
    if(this.terminateFunc) this.terminateFunc()
    this.pageID = templateID_
    // empty the page
    this.page.empty()
    // add the new html
    var view = this.getTemplateByID(templateID_)
    this.page.html(view)
    // if loading -> hide the page
    if(loading){
        this.page.hide()
        this.loader.show()
    }
    return view
}

HandleView.prototype.clearLoadingPage = function(){
    this.page.show()
    this.loader.hide()
}

HandleView.prototype.requestOverlay = function(templateID_){
    // if no page has been initialize, then st overlay as page
    if(!this.pageID){
        return this.requestPage(templateID_)
    }
    // if the overlay id is the same, return a new template
    if(this.overlayID === templateID_){
        this.overlay.html(this.getTemplateByID(templateID_))
        return this.overlay
    }
    // terminate overlay
    if(this.overlayTerminateFunc) this.overlayTerminateFunc()
    // add the template
    this.overlay.html(this.getTemplateByID(templateID_).attr("overlay-div", "true"))
    // hide overflow of body
    this.body.css("overflow", "hidden")
    // self
    var self = this
    // only if a overlay is not on screen
    // because we don't want to add a new ovrlay and animations
    if(this.overlayID === false){
        // show the overlay
        self.overlay.show().addClass("in")
        // add back button
        var backButton = $("<button/>", {id: "overlay-remove"})
        self.overlay.append(backButton)
        // set onclick listener
        backButton.click(function(){
            self.clearOverlay()
        })
        // remove the animation on the overlay oce it's done
        bindToAnimation(self.overlay, function(){
            self.overlay.removeClass("in").addClass("done")
        })
    }
    this.overlayID = templateID_
    // center overlay
    centerVertical(self.overlay.children().first())
    return this.overlay.children().first()
}

HandleView.prototype.clearOverlay = function() {
    var self = this
    self.overlayID = false
    bindToAnimation(this.overlay.addClass("out"), function(){
        self.overlay.removeClass("done out").hide()
        self.body.css("overflow", "auto")
    })
}

/**
 * draws a sidebar on the left or right with the templateID
 */
HandleView.prototype.drawSidebar = function(tempID_, direction_, width_) {
    var direction = (direction_ == "r" ? "right" : "left")
    var width_ = (!width_ ? 250 : width_)
    if(width_ > window.innerWidth) width_ = window.innerWidth - 50
    var self = this
    var sidebarTemplate = this.getTemplateByID(tempID_)
    this.sidebar = $("<div />", {sidebar: true})
    this.sidebar.html(sidebarTemplate).show()
    this.body.append(this.sidebar)
    this.sidebarCover.show()
    var cssClass = "appear-from-"+direction
    this.sidebar.css("width", width_+"px").css(direction, "0").attr("direction", direction)
    bindToAnimation(this.sidebar.addClass(cssClass), function(){
        self.body.css("overflow", "hidden")
        self.sidebar.removeClass(cssClass)
    })
    return this.sidebar
}

HandleView.prototype.clearSidebar = function(){
    var self = this
    self.body.css("overflow", "auto")
    bindToAnimation(this.sidebarCover.addClass("out"), function(){
        self.sidebarCover.hide().removeClass("out")
    })
    // remove the sidebar
    var direction = this.sidebar.attr("direction")
    bindToAnimation(this.sidebar.addClass("disappear-to-"+direction), function(){
        self.sidebar.remove()
    })
    this.overlayID = false
}

HandleView.prototype.drawMenu = function(elem_, map_){
    var self = this
    this.menuHolder = $("<div/>", {"menu-holder": true})
    this.menuHolder.click(function(){self.clearMenu()})
    this.cover.html(this.menuHolder).show()
    for(var i in map_){
        if(i == "title"){
            this.menuHolder.append("<title>"+map_["title"]+"</title>");
        }else{
            if(typeof map_[i] === "object"){
                var active = (map_[i]["active"] ? "active" : "")
                this.menuHolder.append("<button class=\""+active+"\" selectable=true onclick=\"$(this).toggleClass('active'); "+map_[i]["callback"]+"\">"+i+"</button>")
            }else{
                this.menuHolder.append("<button onclick="+map_[i]+">"+i+"</button>")
            }
        }
    }
    this.addRelativeLayoutAccording(this.menuHolder, elem_)
    return this.menuHolder
}

HandleView.prototype.drawRawMenu = function(elem_, html_){
    var self = this
    this.menuHolder = $("<div/>", {"menu-holder": true})
    this.menuHolder.click(function(){self.clearMenu()})
    this.menuHolder.html(html_)
    this.cover.html(this.menuHolder).show()
    this.addRelativeLayoutAccording(this.menuHolder, elem_)
    return this.menuHolder
}

HandleView.prototype.addRelativeLayoutAccording = function(elem_, releativeTo_){
    var bottom = releativeTo_[0].getBoundingClientRect().top + releativeTo_.height();
    var middle = releativeTo_.offset().left + releativeTo_.width()/2 - elem_.outerWidth()/2;
    // check if height or width exceeds the current window dimensions
    if(elem_.height() + bottom > window.innerHeight) bottom = releativeTo_[0].getBoundingClientRect().top - 5 - elem_.height();
    if(middle - elem_.width()/2 < 10) middle = releativeTo_.offset().left
    if(middle + elem_.width()/2 > window.innerWidth) middle = releativeTo_.offset().left + releativeTo_.width();
    // add overlay
    elem_
        .css("position", "absolute")
        .css("top", bottom+"px")
        .css("left", middle+"px")
}

HandleView.prototype.clearMenu = function(){
    if(!this.menuHolder) return
    var self = this
    bindToAnimation(this.cover.addClass("out"), function(){
         self.cover.hide().removeClass("out")
    })
    // remove the menu
    this.menuHolder = false
    this.cover.html("")
}

HandleView.prototype.drawTooltip = function(elem_, message_){
    var tootltip = $("<div/>", {"tooltip-holder": true, text: message_})
    this.body.append(tootltip)
    // add overlay
    this.addRelativeLayoutAccording(tootltip, elem_)
}

HandleView.prototype.clearTooltip = function(elem_, message_){
    $("[tooltip-holder]").remove()
}

/**
 * this methods checks if this template need enterPress or tooltip bounded
 */
HandleView.prototype.checkTemplateForSpecialActions = function(addedTemplate_){
    var self = this
    addedTemplate_.find("[listenToKey=true] input").keypress(function(e){
        if(e.keyCode == 13){
            $(this).parents("[listenToKey=true]").find("button").first().click();
        }
    })
    $.each(addedTemplate_.find("[tooltip]"), function(){
        $(this).hover(
            function(){
                self.drawTooltip($(this), $(this).attr("tooltip"))
            },
            function(){
                self.clearTooltip()
            }
        )
    })
}

/**
 * options ->
 * * loader: Boolean -> adds loader
 * * darkOverlay: Boolean -> adds dark overlay
 * * emptyHTML: Boolean -> empty the html of parent
 * @param parent_
 * @param options_
 */
HandleView.prototype.makeLoadingView = function(parent_, options_){
    var cover = this.cover.clone().show().addClass("invisible").attr("inaccessible-cover", "true")
    parent_.prepend(cover)
    parent_.attr("disabled", "true")
}

HandleView.prototype.realseLoadingView = function(parent_, options_){

}

/**
 * this methods adds an invisible cover to the div so user can't interact with it no more
 * @parent DOM
 */
HandleView.prototype.makeViewInAccessible = function(parent_){
    var cover = this.cover.clone().show().addClass("invisible").attr("inaccessible-cover", "true")
    parent_.prepend(cover)
    parent_.attr("disabled", "true")
}

/**
 * removes the cover added by the makeViewInAccessible method
 */
HandleView.prototype.releaseViewInAccessible = function(parent_){
    parent_.find("[inaccessible-cover]").first().remove()
}

/**
 * makes this button into a loading button with loader
 */
HandleView.prototype.makeLoadingButton = function(button_){
     var text = button_.html()
     var onclick = button_.attr("onclick")
     var width = button_.outerWidth()
     button_.attr("temp-text", text)
     button_.attr("temp-onclick", onclick)
     button_.css("width", width+"px")
     button_.attr("disabled", "true")
     button_.html(this.loader.clone().show().css("stroke", "#FFF").addClass("tinycenter"))
}

/**
 * makes this button into a loading button with loader
 */
HandleView.prototype.releaseLoadingButton = function(button_){
     var text = button_.attr("temp-text")
     var onclick = button_.attr("temp-onclick")
     button_.html(text)
     button_.attr("onclick", onclick)
}

/**
 * add overlay with loader
 */
HandleView.prototype.makeLoaderDiv = function(div_){
    div_.css("position", "relative")
    var overlay = $("<div />", {class: "overlay-loader", style: "width: 100%; height: 40px;"})
    overlay.html(this.loader.clone().addClass("small").show())
    div_.append(overlay)
}

/**
 * makes this button into a loading button with loader
 */
HandleView.prototype.releaseLoaderDiv = function(div_){
    div_.find(".overlay-loader").remove()
}


/**
 * add overlay with loader
 */
HandleView.prototype.makeLoaderOverlay = function(div_){
    div_.css("position", "relative")
    var overlay = $("<div />", {class: "overlay-loader", style: "position: absolute; width: 100%; height: 100%; top: 0; left: 0; background: rgba(255,255,255,.5);"})
    overlay.html(this.loader.clone().show().addClass("small"))
    div_.append(overlay)
}

/**
 * makes this button into a loading button with loader
 */
HandleView.prototype.releaseLoaderOverlay = function(div_){
    div_.find(".overlay-loader").remove()
}

/**
 * static method
 * make the menu hide and show with the direction of the scroll
 */
HandleView.prototype.disableInteractiveMenu = false
HandleView.prototype.interActiveMenu = function(parent_){
    var lastScrollTop = 0,
        st,
        direction;
    var self = this

    function detectDirection() {
        st = window.pageYOffset;

        if (st > lastScrollTop) {
            direction = "down";
        } else {
            direction = "up";
        }

        lastScrollTop = st;

        return  direction;

    }

    $(window).bind('scroll', function() {

        var dir = detectDirection();

        if(self.disableInteractiveMenu) dir = "up"
        if(window.innerWidth > 700) dir = "up"

        if(dir == "up"){
            say("uup")
            $('#menu, #filter')
                .css("transform", "translate(0, 0)")
        }else{
            say("dowwn")
            $('#menu, #filter')
                .css("transform", "translate(0, -48px)")
        }

    });
}
//
//var fadeOut = function(elem_, callback_){
//    bindToTransition(elem_.css("opacity", "0"), function(a, e){
//        if (callback_) callback_()
//    })
//}
//
//var fadeIn = function(elem_, callback_){
//    bindToTransition(elem_.css("opacity", "1"), function(a, e){
//        if (callback_) callback_()
//    })
//}
//
//var ResponsiveIndicator = function(){
//    this.parent = false
//    this.type = false
//}




