/**
 * responsive indicator takes a div and makes it into a indicator that shows what type we are on
 * @constructor
 */
var ResponsiveIndicator = function(){
    this.parent = false
    this.type = false
}

ResponsiveIndicator.prototype.init = function(parent_, defaultType_){
    this.parent = parent_
    this.type = defaultType_
    if(this.parent.length == 0) throw new Error("parent DOM not found!")
    if(!this.type) throw new Error("default type not defined")
    // add the active class
    this.parent.find(".active").removeClass("active")
    this.parent.find("[type="+this.type+"]").addClass("active")
    // listeners
    this.listeners()
    // adapt to the width
    this.adapt()
}

ResponsiveIndicator.prototype.listeners = function(){
    var self = this
    this.parent.find("[type]").click(function(e){
        e.preventDefault()
        self.parent.find(".active").removeClass("active")
        $(this).addClass("active")
    })
}

ResponsiveIndicator.prototype.adapt = function(){
    var self = this
    var hiddenItems = this.parent.find("[hidden]")
    if(hiddenItems.length != 0){
        var more = this.parent.find("[more]")
        var menuHolder = $("<div />")
        hiddenItems.appendTo(menuHolder)
        more.show().click(function(){
            var view = View.drawRawMenu($(this), menuHolder);
            hiddenItems.click(function(){
                self.parent.find("[type]").removeClass("active")
                self.parent.find("[more]").addClass("active")
                hiddenItems.removeClass("active")
                $(this).addClass("active")
                more.addClass("active")
            })
        })
        if(hiddenItems.hasClass("active")){
            more.addClass("active")
        }
    }
}