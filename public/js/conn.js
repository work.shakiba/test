
//region http
	/**
	* create a model for the url
	*/
	var httpRequest = function(url, param, callback){
			$.ajax({
				// properties
				type: 'GET',
				url: url,
				data: param,
				dataType: "json",
				tryCount : 0,
    			retryLimit : 3,
    			// sucess
				success:function(data){
					callback(data)
				},
				// error
				error: function(xhr, textStatus, errorThrown){
					if (textStatus == 'timeout') {
			            this.tryCount++;
			            if (this.tryCount <= this.retryLimit) {
			                // try again
			                $.ajax(this);
			            }
			        }
			        if (xhr.status == 500) {
			        } else {
			            //handle error
			        }
				}
			});
	};
//endregion

//region socket
	var socket = (function(){

		var socket = {};
		var events = {};
		var pileOfRegisteredString = [];

		var init = function(){
			if(typeof(WebSocket) != "function"){return false;}
			if(!userHelper.user._id || !res.socketID){return false;}
			userHelper._id;
			socket = new WebSocket("localhost:81/"+userHelper.user._id+"/"+res.socketID);
			//socket = new WebSocket("ws:joky.ir:81/"+userHelper.user._id+"/"+res.socketID);

			socket.onmessage = function(message){
				var serverMessage = JSON.parse(message.data);
				var firstKey = serverMessage.event;
				var data = serverMessage.data;
				var handler = events[firstKey];
				if(handler){
					handler(data);
				}
			}

			socket.onopen = function(){
				onOpen();
			};

			socket.onerror = function(){
				onError();
			};

			socket.onclose = function(){
				onClose();
			}
		}

		var onOpen = function(){
			reconnecting = false;
			if(reconnectingProcess){
				clearTimeout(reconnectingProcess);
			}
		}

		var reconnecting = false;
		var reconnectingProcess;
		var onClose = function(){
			if(!reconnecting){
				reconnecting = true;
				reconnectingProcess = setTimeout(function(){init()}, 5000)
			}
		}

		var onError = function(){
			reconnectingProcess = setTimeout(function(){init()}, 5000)
		}

		var onMessage = function(){
		}

		var on = function(Event, callback){
			if(typeof(WebSocket) != "function"){return false;}
			events[Event] = callback;
			// if is socket open send the register string
			if(socket.readyState == 1){
				emit("register-for-string", Event);
				// else stack them and add them on open
			}else{
				pileOfRegisteredString.push(Event);
			}
		}

		var emit = function(String, Message){
			if(typeof(WebSocket) != "function"){return false;}
			var mess = {};
			mess["key"] = String;
			mess["message"] = Message;
			socket.send(JSON.stringify(mess));
		}

		return{
			init: init,
			on: on,
			emit: emit
		}

	}())
//endregion

//region Socket
Socket = function(){
    // object that maps each event(string) to handlers
    this.eventToHandlers = {}
    //connection url
    this.connectionURL = false
    // callback
    this.callback = function(){}
    // connecting
    this.connectionInProgress = false
    // connection established
    this.connectionEstablished = false
    // reconnect limit
    this.reconnectLimit = 3
    this.currentReconnectCount = 0
    // socket
    this.socket = false
}

/**
 * initializes a socket connection to the provided url
 * callbacks are optional, and are called after the connection is established, or failed
 * @param {string} url_
 * @param {function} allback_(err, result) optional
 */
Socket.prototype.init = function(url_, callback_){
    // if socket is not supported
    if(typeof(WebSocket) != "function"){
        if(callback_) callback_("socket not supported", false)
        return
    }
    // init the socket
    if(!url_) throw new Error("url is not defined")
    this.connectionURL = url_
    this.socket = new WebSocket(url_);
    if(callback_) this.callback = callback_
    this.connectionInProgress = true

    var self = this

	this.socket.onmessage = function(message){
        var data = JSON.parse(message.data);
        var event = data.event;
        var body = data.data;
        var handler = self.eventToHandlers[event];
        if(handler){
            if(callback_) callback_(null, "event received: "+event+", calling the handler")
            handler(body);
        }else{
            if(callback_) callback_("event received but no handler registered!", false)
        }
    }

	this.socket.onopen = function(){
        if(callback_) callback_(null, true)
        self.connectionEstablished = true
        self.connectionInProgress = false
    };

	this.socket.onerror = function(){
		if(callback_) callback_("connection error", false)
        self.connectionEstablished = false
        self.connectionInProgress = false
    };

	this.socket.onclose = function(){
		if(callback_) callback_("connection closed", false)
        self.connectionEstablished = false
        self.connectionInProgress = false
        self.reOpenSocketOnClose()
    }
}

/**
 * registers an event to a handler
 * when the event is sent from server, this handler is called
 * @param {string} event_
 * @param {function} handler_
 */
Socket.prototype.on = function(event_, handler_){
    this.eventToHandlers[event_] = handler_;
}

/**
 * @param {string} event_
 * @param {*} message_
 */
Socket.prototype.emit = function(event_, message_){
    var socketMessage = {};
    socketMessage["key"] = event_;
    socketMessage["message"] = message_;
    if(this.connectionEstablished && this.socket.readyState === 1){
        this.socket.send(JSON.stringify(mess));
    }else{
        this.callback("socket is not open, cannot send message to server", false)
    }
}

/**
 * reopens a connection in case the connection is closed
 * Note that if connection is closed due to error, it will not re open
 */
Socket.prototype.reOpenSocketOnClose = function(){
    var self = this
    setTimeout(function(){
        if(self.currentReconnectCount >= self.reconnectLimit) return self.callback("maximum number of reconnect limit reached", false)
        self.currentReconnectCount++
        if(self.connectionEstablished === false && self.connectionInProgress === false){
            self.callback("trying to reconnect", false)
            self.init(self.connectionURL, self.callback)
        }
    }, 5000)
}
//endregion