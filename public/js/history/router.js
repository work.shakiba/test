
//router
	/*
		self instantiating
	*/
	if(!window.location.origin){window.location.origin = window.location.protocol + '//' + window.location.host}
	var Router = (function(){

		// variable that is acceassible for all the methods
		var URLParam;
		// every valid url gets stacked into this variable
		var urlStack = [];
		// an object of key value that the key is the url and the value is the function to run
		var map = {};
		var debug = false;
		var lastValidUrl;

		var init = function(map_){
			// add map
			map = map_;
			// read the current URL
			processURL(document.URL);
			// starts listening to a anchors
			listeToAnchors()
			// lsiten to popstates
			listenToPopState()
		}

		var baseURL = "";
		var setBaseUrl = function(baseUrl_){
			baseURL = baseUrl_;
		}

		var listeToAnchors = function(){
			$('body').on("click", "a", function(e) {
				e.preventDefault();
				if($(this).attr('target') != '_blank'){
					if($(this).attr('href')){
						processURL($(this).attr('href'));
					}
				}
			});
		}

		var processURL = function(url_){
			// add slash to the end if not exists
			if(url_.substr(url_.length - 1) != "/"){url_ = url_+"/"}
			URLParam = url_.replace(window.location.origin , '').replace(baseURL+"/", "").split("/");
			analyzeURL();
		}

		var analyzeURL = function(){
			var urlParam_ = URLParam;
			var variables = {};
			var processedURL = [];
			var handler;
			// iterate throught each member of map to find a match
			for(i in map){
				var iForMap = i;
				var mapMemberURL = i.split("/");
				mapMemberURL.splice(0,1)
				var mapMemberHandler = map[i];
				// check for each part to see if url member matches the url
				for(i in mapMemberURL){
					var segmentOfMapMemberURL = mapMemberURL[i];
					var segmentOfWindowURL = (urlParam_[i] == undefined ? "" : urlParam_[i]);
					say("analyze url ", "map segment is = "+segmentOfMapMemberURL+" -- window segment is = "+segmentOfWindowURL, debug);
					// we chack if segment of Member url has special charecters containing () {} []
					var regexForSpecialChars = /(\((.*?)\))|(\[(.*?)\])/g;
					var extractedValueFromMapMemberURLArray = regexForSpecialChars.exec(segmentOfMapMemberURL);
					// if it doesnt contain any special characters
					if(extractedValueFromMapMemberURLArray == null){
						say("analyze url ", "map segment doesnt contail special characters", debug);
						if(segmentOfMapMemberURL == "*"){
							say("analyze url ", "contains *", debug);
							// if there is continuation to the map url just add the current window segment
							if(i != mapMemberURL.length-1){
								say("analyze url", "in * there is more", debug)
								processedURL.push(segmentOfWindowURL);
								continue;
							// if there is no more map url param left, add the url to the end
							}else{
								say("analyze url", "in * adding to the end", debug)
								var windowUrlFromHereToEnd = urlParam_.slice(i, -1);
								processedURL = processedURL.concat(windowUrlFromHereToEnd);
								break;
							}
						}else if(segmentOfMapMemberURL != segmentOfWindowURL){
							processedURL = [];
							break;
						}else{
							say("analyze url ", "exact match between map url segment and windown url segemnt", debug);
							processedURL.push(segmentOfWindowURL);
							continue;
						}
					// if it does contain speecial character
					}else{
						var extractedValueFromMapMemberURLString = segmentOfMapMemberURL;
						// check for conditions
						// if is sorrounded by [] there are options that need to be checked for
						var regexForBrackets = /(\[(.*?)\])/g;
						var isStrigContainingBrackets = regexForBrackets.exec(extractedValueFromMapMemberURLString);
						// this variable is to be used if
						var StringFromBracket;
						var pushed = false;
						if(isStrigContainingBrackets != null){
							say("analyze url ", "contains brackets -- extracted value is = "+isStrigContainingBrackets[2], debug);
							var arrayOfStringsInBrackets = isStrigContainingBrackets[2].split("|");
							var windowURLMatch;
							for(i_ in arrayOfStringsInBrackets){
								if(segmentOfWindowURL == arrayOfStringsInBrackets[i_]){
									windowURLMatch = segmentOfWindowURL
								}
							}
							if(windowURLMatch){
								say("analyze url ", "contains brackets -- window url segment match the options", debug);
								processedURL.push(segmentOfWindowURL);
								StringFromBracket = segmentOfWindowURL;
								pushed = true;
							}else{
								say("analyze url ", "contains brackets -- window url segment doesn't match map", debug);
								processedURL = []
								continue;
							}
							// else{
							// 	say("analyze url ", "contains brackets -- window url doesnt match so replacing the first memeber of options", debug);
							// 	StringFromBracket = arrayOfStringsInBrackets[0];
							// 	processedURL.push(StringFromBracket);
							// }
						}
						// if is sorrounded by () there is a variable that must be extracted
						var isStrigContainingPrathesis = /(\((.*?)\))/g.exec(extractedValueFromMapMemberURLString);
						if(isStrigContainingPrathesis != null){
							say("analyze url ", "contains prathesis -- variable is = "+isStrigContainingPrathesis[2]+" -- extracted value is = "+segmentOfWindowURL, debug);
							if(!pushed){
								processedURL.push(segmentOfWindowURL);
								variables[isStrigContainingPrathesis[2]] = segmentOfWindowURL;
							}else{
								variables[isStrigContainingPrathesis[2]] = StringFromBracket;
							}
						}
					}
				}
				// check if match has been found
				if(processedURL.length > 0){
					say("analyze url", "match has found for "+mapMemberURL+" and the proccessed url is "+processedURL, debug);
					handler = mapMemberHandler;
					variables.url = processedURL.join("/");
					lastValidUrl = variables.url;
					say("variables", variables, debug);
					handler(variables);
					break;
				}
			}
			// if no match has been found
			if(!handler){
				say("analyze url", "----404-----", debug);
				lastValidUrl = URLParam.join("/")
				var view = new Page("error404");
				view.render();
			}
		}

		// push state
		var popstated = false;
		var pushState = function(){
			if(popstated){
				popstated = false; return;
			}
			//
			if(lastValidUrl.length > 0 && lastValidUrl[lastValidUrl.length-1] != "/"){
				lastValidUrl = lastValidUrl+"/";
			}
			var states = lastValidUrl;
			var finalURL = window.location.origin+"/"+states;
			if(history.pushState){window.history.pushState(null, finalURL, finalURL)};
		}

		var listenToPopState = function(){
			window.addEventListener('popstate', function(event) {
				popstated = true;
				processURL(document.URL);
			});
		}

		return{
			init: init,
			processURL: processURL,
			setBaseUrl: setBaseUrl,
			pushState: pushState,
		}

	}())


