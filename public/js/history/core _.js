//region router
	/*
		self instantiating
	*/
	if(!window.location.origin){window.location.origin = window.location.protocol + '//' + window.location.host}
	var Router = (function(){

		// variable that is acceassible for all the methods
		var URLParam;
		// every valid url gets stacked into this variable
		var urlStack = [];
		// an object of key value that the key is the url and the value is the function to run
		var map = {};
		var debug = false;
		var lastValidUrl;

		var init = function(map_){
			// add map
			map = map_;
			// read the current URL
			processURL(document.URL);
			// starts listening to a anchors
			listeToAnchors()
			// lsiten to popstates
			listenToPopState()
		}

		var baseURL = "";
		var setBaseUrl = function(baseUrl_){
			baseURL = baseUrl_;
		}

		var listeToAnchors = function(){
			$('body').on("click", "a", function(e) {
				e.preventDefault();
				if($(this).attr('target') != '_blank'){
					if($(this).attr('href')){
						processURL($(this).attr('href'));
					}
				}
			});
		}

		var processURL = function(url_){
			// add slash to the end if not exists
			if(url_.substr(url_.length - 1) != "/"){url_ = url_+"/"}
			URLParam = url_.replace(window.location.origin , '').replace(baseURL+"/", "").split("/");
			analyzeURL();
		}

		var analyzeURL = function(){
			var urlParam_ = URLParam;
			var variables = {};
			var processedURL = [];
			var handler;
			// iterate throught each member of map to find a match
			for(i in map){
				var iForMap = i;
				var mapMemberURL = i.split("/");
				mapMemberURL.splice(0,1)
				var mapMemberHandler = map[i];
				// check for each part to see if url member matches the url
				for(i in mapMemberURL){
					var segmentOfMapMemberURL = mapMemberURL[i];
					var segmentOfWindowURL = (urlParam_[i] == undefined ? "" : urlParam_[i]);
					log("analyze url ", "map segment is = "+segmentOfMapMemberURL+" -- window segment is = "+segmentOfWindowURL, debug);
					// we chack if segment of Member url has special charecters containing () {} []
					var regexForSpecialChars = /(\((.*?)\))|(\[(.*?)\])/g;
					var extractedValueFromMapMemberURLArray = regexForSpecialChars.exec(segmentOfMapMemberURL);
					// if it doesnt contain any special characters
					if(extractedValueFromMapMemberURLArray == null){
						log("analyze url ", "map segment doesnt contail special characters", debug);
						if(segmentOfMapMemberURL == "*"){
							log("analyze url ", "contains *", debug);
							// if there is continuation to the map url just add the current window segment
							if(i != mapMemberURL.length-1){
								log("analyze url", "in * there is more", debug)
								processedURL.push(segmentOfWindowURL);
								continue;
							// if there is no more map url param left, add the url to the end
							}else{
								log("analyze url", "in * adding to the end", debug)
								var windowUrlFromHereToEnd = urlParam_.slice(i, -1);
								processedURL = processedURL.concat(windowUrlFromHereToEnd);
								break;
							}
						}else if(segmentOfMapMemberURL != segmentOfWindowURL){
							processedURL = [];
							break;
						}else{
							log("analyze url ", "exact match between map url segment and windown url segemnt", debug);
							processedURL.push(segmentOfWindowURL);
							continue;
						}
					// if it does contain speecial character
					}else{
						var extractedValueFromMapMemberURLString = segmentOfMapMemberURL;
						// check for conditions
						// if is sorrounded by [] there are options that need to be checked for
						var regexForBrackets = /(\[(.*?)\])/g;
						var isStrigContainingBrackets = regexForBrackets.exec(extractedValueFromMapMemberURLString);
						// this variable is to be used if
						var StringFromBracket;
						var pushed = false;
						if(isStrigContainingBrackets != null){
							log("analyze url ", "contains brackets -- extracted value is = "+isStrigContainingBrackets[2], debug);
							var arrayOfStringsInBrackets = isStrigContainingBrackets[2].split("|");
							var windowURLMatch;
							for(i_ in arrayOfStringsInBrackets){
								if(segmentOfWindowURL == arrayOfStringsInBrackets[i_]){
									windowURLMatch = segmentOfWindowURL
								}
							}
							if(windowURLMatch){
								log("analyze url ", "contains brackets -- window url segment match the options", debug);
								processedURL.push(segmentOfWindowURL);
								StringFromBracket = segmentOfWindowURL;
							}else{
								log("analyze url ", "contains brackets -- window url doesnt match so replacing the first memeber of options", debug);
								StringFromBracket = arrayOfStringsInBrackets[0];
								processedURL.push(StringFromBracket);
							}
							pushed = true;
						}
						// if is sorrounded by () there is a variable that must be extracted
						var isStrigContainingPrathesis = /(\((.*?)\))/g.exec(extractedValueFromMapMemberURLString);
						if(isStrigContainingPrathesis != null){
							log("analyze url ", "contains prathesis -- variable is = "+isStrigContainingPrathesis[2]+" -- extracted value is = "+segmentOfWindowURL, debug);
							if(!pushed){
								processedURL.push(segmentOfWindowURL);
								variables[isStrigContainingPrathesis[2]] = segmentOfWindowURL;
							}else{
								variables[isStrigContainingPrathesis[2]] = StringFromBracket;
							}
						}
					}
				}
				// check if match has been found
				if(processedURL.length > 0){
					log("analyze url", "match has found for "+mapMemberURL+" and the proccessed url is "+processedURL, debug);
					handler = mapMemberHandler;
					variables.url = processedURL.join("/");
					lastValidUrl = variables.url;
					log("variables", variables, debug);
					handler(variables);
					break;
				}
			}
			// if no match has been found
			if(!handler){
				log("analyze url", "----404-----", debug);
				lastValidUrl = URLParam.join("/")
				var view = new Page("error404");
				view.render();
			}
		}

		// push state
		var popstated = false;
		var pushState = function(){
			if(popstated){
				popstated = false; return;
			}
			//
			if(lastValidUrl.length > 0 && lastValidUrl[lastValidUrl.length-1] != "/"){
				lastValidUrl = lastValidUrl+"/";
			}
			var states = lastValidUrl;
			var finalURL = window.location.origin+"/"+states;
			if(history.pushState){window.history.pushState(null, finalURL, finalURL)};
		}

		var listenToPopState = function(){
			window.addEventListener('popstate', function(event) {
				popstated = true;
				processURL(document.URL);
			});
		}

		return{
			init: init,
			processURL: processURL,
			setBaseUrl: setBaseUrl,
			pushState: pushState,
		}

	}())
//endregion

//region page
	/**
	* identifier_: String
	*/
	var Page = function(templateAddress_, config_){

		this.config = (!config_ ? {} : config_);

		this.loadingState = false;

		this.templateAddress = templateAddress_;
		this.template = templates.find("#"+this.templateAddress+"_template").html();
		this.html = false;
		/*
			0: un initialized
			1: saved in html file
			2: added to body
		*/
		this.pageState = false;
		this.visibilityState = false;

		this.animationState = false;

		this.events = {};

		this.render = function(){
			// if animation is running || page has been rendered
			if(this.animationState || this.pageState == 2){return ;}
			// if !this.html ? must create the container
			if(!this.html){
				// create the page
				this.html = $("<section class=page><div class=body id=\""+this.templateAddress+"\"></div></section>");
				// add template to html
				this.html.find(".body").html(this.template);
				// listen to over scroll
				this.windowEventListen()
				var onInit = true;
				// add loading
				if(this.config.loading){
					// add html
					this.html.append("<div id=\"loading\"> <div> <span class=\"logo\"></span> <span class=\"text\">در حال دریافت اطلاعات</span> <span class=\"animation\"><span></span><span></span><span></span><span></span><span></span><span></span><span></span></span> </div> </div>");
					// set laoding state
					this.loadingState = true;
				}

			}
			// add html to the view
			$("body").prepend(this.html);
			// pushadate
			Router.pushState();
			// add fade-in animation to page
			var _this = this;
			this.animationState = true;
			bindToAnimate(this.html.removeClass("page-done").addClass("page-fade-in"), function(e){
				if(e){
					$(e).removeClass("page-fade-in").addClass("page-done");
				}else{
					_this.html.css("opacity", 1);
				}
				_this.animationState = false;
			});
			// set render as true
			this.pageState = 2;
			// trigger the "on-init"
			if(onInit){
				this.triggerEvent("on-init");
			}
			// add loading
			if(!this.config.loading){
				// clea the loading floading doesn;t exits
				this.clearLoading();
			}
		}

		this.clearLoading = function(){
			// check if visible
			if(this.visibilityState){return ;}
			// fade out the loading
			var _this = this;
			bindToAnimate(this.html.find("#loading").addClass("fade-out"), function(e){
				if(e){
					$(e).remove();
				}else{
					_this.html.find("#loading").remove()
				}
				_this.loadingState = false;
				// setTimeout(function(){$(e).remove();}, 1000)
			});
			// fade in the body
			bindToAnimate(this.html.find(".body").addClass("fade-in"), function(e){
				if(e){
					$(e).removeClass("fade-in").addClass("done");
				}else{
					_this.html.find(".body").css("opacity", 1);
				}
				_this.visibilityState = true;
			});
			// trigger the "on-render"
			this.triggerEvent("on-render");
			// add bottom loading
			if(this.config.bottomLoading){
				// add html
				this.html.append("<div id=\"bottomLoading\"> <span class=\"text\">در حال دریافت اطلاعات</span> <span class=\"lines\"><span></span></span></div>");
				// set laoding state
				this.loadingState = true;
			}
		}

		this.terminate = function(){
			// clone current html to local variable
			var clonedHtml = this.html.clone();
			// run onTerminate function
			if(this.onTerminate){
				this.onTerminate();
			}
			// run animation
			var _this = this;
			this.animationState = true;
			bindToAnimate(this.html.addClass("page-fade-out"), function(e){
				if(e){
					_this.html.removeClass("page-fade-out")
					// clone current html to local variable
					var clonedHtml = _this.html.clone();
					// remove the view
					_this.html.remove();
					// add cloned view to html
					if(_this.config.cache){
						_this.html = clonedHtml;
						_this.windowEventListen();
					}
				}else{
					_this.html.remove()
				}
				_this.animationState = false;
			});
			// set visible and rendered as false
			this.visibilityState = false;
			this.pageState = 1;
			// trigger the "on-terminate"
			this.triggerEvent("on-terminate");
		}

		this.windowEventListen = function(){
			var _this = this;
			$(window).scroll(function(){
				var reachedBottom = $(window).scrollTop() + $(window).height() >= $(document).height()-200;
				if(reachedBottom && _this.pageState == 2){
					_this.triggerEvent("on-overscroll");
				}
			})
			window.onresize = function(){
				if(_this.pageState == 2){
					_this.triggerEvent("resize");
				}
			}
		}

		this.on = function(event_, handler_){
			// valid events
			var validEvents = ["on-init", "on-render", "on-terminate", "on-overscroll", "resize", "render-finish"];
			// check for validity
			if(validEvents.indexOf(event_) == -1){
				throw new Error("Event provided is not valid!");
			}
			// add handler to events
			this.events[event_] = handler_;
		}

		this.triggerEvent = function(event_){
			// find the handler
			var handler = this.events[event_];
			// check if exists, run it
			if(handler){
				handler();
			}
		}

	}
//endregion
//region overlay
	var Overlay = function(templateAddress_, loading_, cache_){

		this.enableCache = cache_;

		this.enableLoading = loading_;
		this.loadingState = false;

		this.templateAddress = templateAddress_;
		this.template = templates.find("#"+templateAddress_+"_template").html();
		this.html = false;
		this.pageState = false;
		this.visibilityState = false;

		this.backButton = false;

		this.animationState = false;

		this.events = {};

		this.render = function(){
			// if animation is running || page has been rendered
			if(this.animationState || this.pageState == 2){return ;}
			// if !this.html ? must create the container
			if(!this.html){
				// create the page
				this.html = $("<section class=overlay><section class=body id=\""+this.templateAddress+"\"></section></section>");
				// add template to html
				this.html.find(".body").html(this.template);
				// listen to over scroll
				this.windowEventListen()
				var onInit = true;
				// add loading
				if(this.enableLoading){
					// add html
					this.html.append("<div id=\"loading\"> <div> <span class=\"logo\"></span> <span class=\"text\">در حال دریافت اطلاعات</span> <span class=\"animation\"><span></span></span> </div> </div>");
					// set laoding state
					this.loadingState = true;
				}
			}
			// add html to the view
			$("body").append(this.html);
			// center
			if(onInit){
				var margin =  ((window.innerHeight - this.html.find(".body").first().height())/2);
		 		if(margin < 50){margin = 50}
				this.html.find(".body").first().css('margin',margin+'px auto')
			}
			// add fade-in animation to page
			this.html.removeClass("overlay-done").addClass("overlay-fade-in");
			var _this = this;
			this.animationState = true;
			bindToAnimate(this.html, function(e){
				if(e){
					$(e).removeClass("overlay-fade-in").addClass("overlay-done");
				}else{
					_this.html.css("opacity", 1);
				}
				_this.animationState = false;
			});
			// set render as true
			this.pageState = 2;
			// trigger the "on-init"
			if(onInit){
				this.triggerEvent("on-init");
			}
			// add the beck button if needed
			if(!this.backButton){
				this.backButton = $("<button id=back onclick=$(\".overlay\").click()></button>")
				$("body").prepend(this.backButton);
			}
			// add loading
			if(!this.enableLoading && !this.loadingState){
				// clea the loading floading doesn;t exits
				this.clearLoading();
			}
		}

		this.clearLoading = function(){
			// check if visible
			if(this.visibilityState){return ;}
			// fade out the loading
			var _this = this;
			this.html.find("#loading").addClass("fade-out")
			bindToAnimate(this.html.find("#loading"), function(e){
				if(e){
					$(e).hide();
				}else{
					_this.html.find("#loading").hide()
				}
				_this.loadingState = false;
			});
			// fade in the body
			this.html.find(".body").removeClass("done").addClass("fade-in")
			bindToAnimate(this.html.find(".body"), function(e){
				if(e){
					$(e).removeClass("fade-in").addClass("done");
				}else{
					_this.html.find(".body").css("opacity", 1);
				}
				_this.visibilityState = true;
			});
			// trigger the "on-render"
			this.triggerEvent("on-render");
		}

		this.terminate = function(){
			// run onTerminate function
			if(this.onTerminate){
				this.onTerminate();
			}
			var _this = this;
			this.animationState = true;
			bindToAnimate(this.html.addClass("overlay-fade-out"), function(e){
				if(e){
					_this.html.removeClass("overlay-fade-out")
					// clone current html to local variable
					var clonedHtml = _this.html.clone();
					// remove the view
					_this.html.remove();
					// add cloned view to html
					if(_this.enableCache){
						_this.html = clonedHtml;
						_this.windowEventListen();
					}
					_this.backButton.remove();
					_this.backButton = false;
				}else{
					_this.html.remove()
				}
				_this.animationState = false;
			});
			// set visible and rendered as false
			this.visibilityState = false;
			this.pageState = 1;
			// trigger the "on-terminate"
			this.triggerEvent("on-terminate");
		}

		this.windowEventListen = function(){
			var _this = this;
			_this.html.scroll(function(){
				var reachedBottom = _this.overlayContainer.scrollTop() + _this.overlayContainer.height() >= _this.overlayContainer[0].scrollHeight-200;
				if(reachedBottom && _this.pageState == 2){
					_this.triggerEvent("on-overscroll");
				}
			})
			window.onresize = function(){
				_this.triggerEvent("resize");
			}
			_this.html.click(function(e){
				if(e.target == this){
					_this.terminate();
				}
			});
		}

		this.on = function(event_, handler_){
			// valid events
			var validEvents = ["on-init", "on-render", "on-terminate", "on-overscroll"];
			// check for validity
			if(validEvents.indexOf(event_) == -1){
				throw new Error("Event provided is not valid!");
			}
			// add handler to events
			this.events[event_] = handler_;
		}

		this.triggerEvent = function(event_){
			// find the handler
			var handler = this.events[event_];
			// check if exists, run it
			if(handler){
				handler();
			}
		}

	}
//endregion

//region view controller
	// view controller
	/*
		request_ = {cache: [true|false],  config: obj}
	*/
	viewController = (function(){

		var debug = true;
		var currentOverlay = false;
		var currentPage = false;
		var stack = {};

		var pageStack = {};
		var requestPage = function(templateAddress_, loading_, identifier_, bottomLoading_){
			if(!identifier_){identifier_ = false;}
			if(currentPage.identifier == identifier_){return currentPage;}
			// clear overlay
			if(currentPage){removeOverlay();}
			// terminate prev page
			terminatePage()
			// if page exists
			if(!identifier_){
				// create the view
				currentPage = new Page(templateAddress_, {loading: loading_, cache: false, bottomLoading: bottomLoading_});
			}else{
				// check if page exists in the stack
				currentPage = pageStack[identifier_];
				// if page doen't exits
				if(!currentPage){
					var cache = (identifier_ ? false : true);
					// create the view
					currentPage = new Page(templateAddress_, {loading: loading_, cache: cache, bottomLoading: bottomLoading_});
					currentPage.identifier = identifier_;
					// add page to stack
					pageStack[identifier_] = currentPage;
				}
			}
			// return the page
			return currentPage;
		}

		var terminatePage = function(){
			if(currentPage){
				currentPage.terminate();
			}
		}

		var requestOverlay = function(templateAddress_, loading_, identifier_){
			if(!identifier_){identifier_ = false;}
			if(currentOverlay.identifier === identifier_){return currentOverlay;}
			var overlay = false;
			// if page exists
			if(!identifier_){
				// create the view
				overlay = new Overlay(templateAddress_, loading_, false);
			}else{
				// check if page exists in the stack
				overlay = pageStack[identifier_];
				// if page doen't exits
				if(!overlay){
					var cache = (identifier_ ? false : true);
					// create the view
					overlay = new Overlay(templateAddress_, loading_, cache);
					overlay.identifier = identifier_;
					// add page to stack
					pageStack[identifier_] = overlay;
				}
			}
			// terminate prev page
			if(currentOverlay){currentOverlay.terminate();}
			currentOverlay = overlay;
			// return the page
			return currentOverlay;
		}

		var listenToOverlayClick = function(){
			currentOverlay.html.click(function(e){
				if(e.target == this && currentOverlay.overlayContainer){
					removeOverlay();
				}
			})
		}

		var removeOverlay = function(disableAnimate_){
			if(currentOverlay){currentOverlay.terminate(disableAnimate_)};
			currentOverlay = false;
		}

		var pushState = function(){
			Router.pushState();
		}

		var pushOverlayState = function(){
			Router.pushOverlayState();
		}

		return{
			requestPage: requestPage,
			requestOverlay: requestOverlay,
			removeOverlay: removeOverlay,
		}

	}())
//endregion
//region http request
	/**
	* create a model for the url
	*/
	var httpRequest = function(url, param, callback){
			say("httpRequest", "run")
			$.ajax({
				// properties
				type: 'GET',
				url: url,
				data: param,
				dataType: "json",
				tryCount : 0,
    			retryLimit : 3,
    			// sucess
				success:function(data){
					callback(data)
				},
				// error
				error: function(xhr, textStatus, errorThrown){
					if (textStatus == 'timeout') {
			            this.tryCount++;
			            if (this.tryCount <= this.retryLimit) {
			                // try again
			                $.ajax(this);
			            }
			        }
			        if (xhr.status == 500) {
						log("HttpRequest error", errorThrown);
			        } else {
			            //handle error
						log("HttpRequest unknown error", errorThrown);
			        }
				}
			});
	};
//endregion
//region view
var drawCoverBelow= function(cls, zindex, duration, callback){
	var cover = $('<div class="'+cls+'" id="cover"></div>');
	$('body').append(cover);
	cover.click(function(){
		fadeOutAndRemoveElem(cover)
		callback()
	})
}

var fadeOutAndRemoveElem = function(elem){
	bindToAnimate(elem.addClass("fade-out"), function(e){
		if(e){
			elem.remove()
		}else{
			elem.remove()
		}
	});
}
//endregion

//region socket
var socket = (function(){

	var socket = {};
	var events = {};
	var pileOfRegisteredString = [];

	var init = function(){
		say("attempting to connect")
		if(typeof(WebSocket) != "function"){return false;}
		if(!userHelper.user._id || !res.socketID){return false;}
		userHelper._id;
		socket = new WebSocket("ws:joky.ir:81/"+userHelper.user._id+"/"+res.socketID);

		socket.onmessage = function(message){
			say("message", message);
			var serverMessage = JSON.parse(message.data);
			var firstKey = serverMessage.event;
			var data = serverMessage.data;
			var handler = events[firstKey];
			if(handler){
				handler(data);
			}
		}

		socket.onopen = function(){
			onOpen();
		};

		socket.onerror = function(){
			onError();
		};

		socket.onclose = function(){
			onClose();
		}
	}

	var onOpen = function(){
		console.log("OPEN")
		reconnecting = false;
		if(reconnectingProcess){
			clearTimeout(reconnectingProcess);
		}
	}

	var reconnecting = false;
	var reconnectingProcess;
	var onClose = function(){
		console.log("CLOSE");
		if(!reconnecting){
			reconnecting = true;
			reconnectingProcess = setTimeout(function(){init()}, 5000)
		}
	}

	var onError = function(){
		console.log("ERROR");
		reconnectingProcess = setTimeout(function(){init()}, 5000)
	}

	var onMessage = function(){
		console.log("MESSAGE")
	}

	var on = function(Event, callback){
		if(typeof(WebSocket) != "function"){return false;}
		events[Event] = callback;
		// if is socket open send the register string
		if(socket.readyState == 1){
			emit("register-for-string", Event);
			// else stack them and add them on open
		}else{
			pileOfRegisteredString.push(Event);
		}
	}

	var emit = function(String, Message){
		if(typeof(WebSocket) != "function"){return false;}
		var mess = {};
		mess["key"] = String;
		mess["message"] = Message;
		socket.send(JSON.stringify(mess));
	}

	return{
		init: init,
		on: on,
		emit: emit
	}

}())
//endregion

//region validate form
	validateForm = function(parent_){
		var return_ = true;
		$.each(parent_.find("input, textarea"), function(){
			elem = $(this)
			var value = elem.val();
			if((elem.attr('type') == "number") && isNaN(parseInt(value)) && (value != "")){
				tooltip({element: elem, message:'Value must be number'}); return_ =false; return false;
			}else if(elem.attr('required') == "true"){
				if(value.length < elem.attr('min')){tooltip({element: elem, message: 'حداقل کاراکتر مجاز '+elem.attr('min')+" است!"}); return_ =false; return false;}
				if(value.length > elem.attr('max')){tooltip({element: elem, message: 'حداکثر کاراکتر مجاز '+elem.attr('max')+" است!"}); return_ =false; return false;}
			}
		});
		return return_;
	}
//endregion
//region tooltip
	tooltip = function(obj_){
		var identifier = makeId();
		$('.tooltip').remove();
		var node = $("<div/>", {"class": "tooltip "+identifier, "text": obj_.message});
		obj_.element.parent().css("position","relative").append(node);
		$('.tooltip.'+identifier).css("top", obj_.element.parent().height() + 15 + "px")
		var timeout = (obj_.timeout ? obj_.timeout : 3000)
		setTimeout(function(){
				$('.tooltip.'+identifier).remove()
		},timeout);
	}
	betterTooltip = function(element_){
		var identifier = makeId();
		$('.tooltip').remove();
		var node = $("<div/>", {"class": "tooltip "+identifier, "text": element_.attr("tooltip")});
		element_.parent().css("position","relative").append(node);
		node.css("top", element_.parent().height() + 15 + "px")
		node.css("left", element_.position().left + element_.width()/2 - node.width()/2 + "px")
		setTimeout(function(){
				$('.tooltip.'+identifier).remove()
		},5000);
	}

	removeBetterTolltip = function(element_) {
		element_.parent().find(".tooltip").remove();
	}

	tooltipListen = function(dom_) {

			$(document).on("mouseover", "[tooltip]", function(){
				betterTooltip($(this));
			})

			$(document).on("mouseleave", "[tooltip]", function(){
				removeBetterTolltip($(this));
			})

	}
//endregion
//region makeId
	makeId = function(){
		var text = "id";
		var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
		for( var i=0; i < 10; i++ ){
			text += possible.charAt(Math.floor(Math.random() * possible.length));
		}
		return text;
	}
//endregion
//region localstorage
	store = function(key_, value_){
		if(!value_){
			return localStorage.getItem(key_);
		}else{
			localStorage.setItem(key_, value_)
		}
	}
//endregion
//region log
	var log = function(flag_, message_, debugMode_){
		if(debugMode_ === false){return false}
        if(!message_){
        	message_ = "NULL";
        }
        console.log('(---'+flag_+'---) =>', message_);
    }
	var 
say = log;
//endregion
//region centerVertical
	var centerVertical = function(section_){
		var margin =  ((window.innerHeight - section_.height())/2);
 		if(margin < 50){margin = 50}
		section_.css('margin',margin+'px auto')
	}
//endregion
//region gridify
	var gridify = function(section_,itemWidth_){
		var itemWidth = itemWidth_;
		var itemPerRow = Math.floor(section_.width()/itemWidth);
		if(itemPerRow == 0){itemPerRow = 1;}
		if(section_.attr('item-per-row') == itemPerRow){return false;}
		section_.html('')
		.attr('item-per-row', itemPerRow);
		for (var i = itemPerRow; i >= 1; i--) {
			section_.append('<div class="column column_'+i+'"></div>');
			section_.find('.column').css('width', (100/itemPerRow)+'%');
		};
		return true;
	};
//endregion
//region clean text
	var cleanText = function(preText_){
		var cleanText = preText_.replace(/(?:\r\n|\r|\n)/g, '<br />').replace(/"/g, '\"');
		return cleanText;
	}
//endregion
//region auto image upload
	var autoImageUpload = function(){
		$("body").on("click","*[auto-upload-button]",function(){
			var parent = $(this).parent();
			parent.find("input[type=file]").click();
			parent.find("input[type=file]").on("change", function(){
				uploadImage($(this));
			})
		})
	}

	var uploadImage = function(image_){
		if(!u_image_validation(image_)){return 0;};
		image_.parent().find("img").css('background','url('+URL.createObjectURL(image_[0].files[0])+') 50% 50%').css("background-size","cover");
		var formData = new FormData();
	    formData.append('image', image_[0].files[0]);
		formData.append('name', name);
	    $.ajax({ type: 'POST', url: base_url+"api/"+image_.attr("upload-url"), data: formData, processData: false, contentType: false,
			success:function(data){
				image_.attr("image-name",data.image);
			},
		});
	}
//endregion
//region image validation
	u_image_validation = function(file_){
		if(file_.val() != ''){
			var type = file_.val().split('.').pop().toLowerCase();
			if($.inArray(type, ['gif','png','jpg','jpeg']) == -1) {
				tooltip({element: file_, message: messages['image_mis_type'],identifier: 'error'})
			return 0;
			} else if(file_[0].files[0].size > 500000){
				tooltip({element: file_, message: messages['image_too_large'],identifier: 'error'})
			return 0;
			}
			return 1;
		}else{
			return 0;
		}
	}
//endregion
//region get time
	get_time = function(date){

		var clientNow = Date.now()/1000;
		var itemTime = date/1000;

		var timeDeffrence = clientNow - itemTime;

		var timeDeffrenceInMinute = Math.floor(timeDeffrence/60);

		var timeDeffrenceInHour = Math.floor(timeDeffrenceInMinute/60);

		var timeDeffrenceInDay = Math.floor(timeDeffrenceInHour/24);

		if(timeDeffrenceInMinute < 59){
			return (timeDeffrenceInMinute+' دقیقه پیش');
		}else if(timeDeffrenceInHour < 23){
			return (timeDeffrenceInHour+' ساعت پیش');
		}else if(timeDeffrenceInDay < 31){
			return (timeDeffrenceInDay+' روز پیش');
		}else if(timeDeffrenceInDay >= 30){
			var months = Math.floor(timeDeffrenceInDay/30)
			return (months+' ماه پیش');
		}

	}

	var time_now = function(){
		d = new Date;
      return (d.getFullYear()+'-'+d.getMonth()+'-'+d.getDate()+' '+d.toTimeString().split(' ')[0]);
	}
//endregion
//region toast
	toast = function(message_,type_){
		var flasher_id = makeId();
		if($('.flasher_div').length == 0){
			$("body").append("<div class=flasher_div></div>")
		}
		$('.flasher_div').append('<span class="flasher '+flasher_id+' '+type_+'"><span class="body">'+message_+'</span></span>');
		setTimeout(function(){
			$('.flasher.'+flasher_id)
			.addClass('flasher_out')
			.bind("animationend webkitAnimationEnd oanimationend MSAnimationEnd", function(){
				$('.flasher.'+flasher_id).remove().unbind();
			});
		}, 5000);
		$('.flasher.'+flasher_id).click(function(){
			$('.flasher.'+flasher_id)
			.addClass('flasher_out')
			.bind("animationend webkitAnimationEnd oanimationend MSAnimationEnd", function(){
				$('.flasher.'+flasher_id).remove().unbind();
			});
		})
	}
//endregion
//region scroll to bottom of div
	scrollToBottom = function(div_){
	    div_[0].scrollTop = div_[0].scrollHeight;
	}
//endregion
//region empty message
	$.prototype.isEmpty = function(isEmpty_){
		if(isEmpty_ && this.find(".empty_message").length == 0){
			this.append("<span class=empty_message>Sorry, No result(s) found!</span>")
		}else if(!isEmpty_ && this.find(".empty_message").length == 1){
			this.find(".empty_message").remove();
		}
	}
//endregion
//region detect CSS Feature
	var isAnimationAvailable = function(){
		var animation = false, animationstring = 'animation', keyframeprefix = '', domPrefixes = 'Webkit Moz O ms Khtml'.split(' '), pfx = '', elm = document.createElement('div'); if( elm.style.animationName !== undefined ) { animation = true; } if( animation === false ) { for( var i = 0; i < domPrefixes.length; i++ ) { if( elm.style[ domPrefixes[i] + 'AnimationName' ] !== undefined ) { pfx = domPrefixes[ i ]; animationstring = pfx + 'Animation'; keyframeprefix = '-' + pfx.toLowerCase() + '-'; animation = true; break; } } }
		return animation;
	}

	function detectCSSFeature(featurename){
	var feature = false,
	domPrefixes = 'Webkit Moz ms O'.split(' '),
	elm = document.createElement('div'),
	featurenameCapital = null;

	featurename = featurename.toLowerCase();

	if( elm.style[featurename] !== undefined ) { feature = true; }

	if( feature === false ) {
	featurenameCapital = featurename.charAt(0).toUpperCase() + featurename.substr(1);
	for( var i = 0; i < domPrefixes.length; i++ ) {
	    if( elm.style[domPrefixes[i] + featurenameCapital ] !== undefined ) {
	      feature = true;
	      break;
	    }
	}
	}
	return feature;
	}
//endregion
//region bind to animate
	var bindToAnimate = function(elem, callback){
		if(isAnimationAvailable()){
			elem.bind("animationend webkitAnimationEnd oanimationend MSAnimationEnd", function(e){
				$(this).unbind("animationend webkitAnimationEnd oanimationend MSAnimationEnd")
				callback(this);
			})
		}else{
			callback(false);
		}
	}

	var bindToAnimation = function(elem, callback){
		if(isAnimationAvailable()){
			elem.bind("animationend webkitAnimationEnd oanimationend MSAnimationEnd", function(e){
				$(this).unbind("animationend webkitAnimationEnd oanimationend MSAnimationEnd")
				callback(true, this);
			})
		}else{
			callback(false, elem);
		}
	}
//endregion
//region ripple
	$("body").click(function(e){
		var x = e.pageX;
        var y = e.pageY;
        var clickY = y - $(this).offset().top;
        var clickX = x - $(this).offset().left;
        var setX = parseInt(clickX);
		var setY = parseInt(clickY);
		var box = this;
        $(this).append('<svg><circle cx="'+setX+'" cy="'+setY+'" r="1"></circle></svg>');
        $("circle").bind("animationend webkitAnimationEnd oanimationend MSAnimationEnd", function(){$(this).parent().remove()})
	})
//endregion
//region press enter
	var listenToEnter = function(){
		$("[listenToKey=true] input").keypress(function(e){
			if(e.keyCode == 13){
				$(this).parents("[listenToKey=true]").find("button").first().click();
			}
		})
	}
//endregion
//region bind
	function DataBinder(object_id) {
	  // Use a jQuery object as simple PubSub
	  var pubSub = $({});

	  // We expect a `data` element specifying the binding
	  // in the form: data-bind-<object_id>="<property_name>"
	  var data_attr = "bind-" + object_id,
	      message = object_id + ":change";

	  // Listen to change events on elements with the data-binding attribute and proxy
	  // them to the PubSub, so that the change is "broadcasted" to all connected objects
	  $(document).on("change", "[data-" + data_attr + "]", function(evt){
	    var $input = $(this);
	    pubSub.trigger(message, [$input.data(data_attr), $input.val()]);
	  });

	  // PubSub propagates changes to all bound elements, setting value of
	  // input tags or HTML content of other tags
	  pubSub.on( message, function( evt, prop_name, new_val ) {
	    $( "[data-" + data_attr + "=" + prop_name + "]" ).each( function() {
	      var $bound = $( this );

	      if ( $bound.is("input, textarea, select") ) {
	        $bound.val( new_val );
	      } else {
	        $bound.html( new_val );
	      }
	    });
	  });

	  return pubSub;
	}
//endregion
