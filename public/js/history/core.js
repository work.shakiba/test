// vars
    var pageLoading = "<div id=\"page-loading\"> <div> <span class=\"logo\"></span> <span class=\"text\">محتویاتی برای نمایش وجود ندارد</span></div>"

//router
	/*
		self instantiating
	*/
	if(!window.location.origin){window.location.origin = window.location.protocol + '//' + window.location.host}
	var Router = (function(){

		// variable that is acceassible for all the methods
		var URLParam;
		// every valid url gets stacked into this variable
		var urlStack = [];
		// an object of key value that the key is the url and the value is the function to run
		var map = {};
		var debug = false;
		var lastValidUrl;

		var init = function(map_){
			// add map
			map = map_;
			// read the current URL
			processURL(document.URL);
			// starts listening to a anchors
			listeToAnchors()
			// lsiten to popstates
			listenToPopState()
		}

		var baseURL = "";
		var setBaseUrl = function(baseUrl_){
			baseURL = baseUrl_;
		}

		var listeToAnchors = function(){
			$('body').on("click", "a", function(e) {
				e.preventDefault();
				if($(this).attr('target') != '_blank'){
					if($(this).attr('href')){
						processURL($(this).attr('href'));
					}
				}
			});
		}

		var processURL = function(url_){
			// add slash to the end if not exists
			if(url_.substr(url_.length - 1) != "/"){url_ = url_+"/"}
			URLParam = url_.replace(window.location.origin , '').replace(baseURL+"/", "").split("/");
			analyzeURL();
		}

		var analyzeURL = function(){
			var urlParam_ = URLParam;
			var variables = {};
			var processedURL = [];
			var handler;
			// iterate throught each member of map to find a match
			for(i in map){
				var iForMap = i;
				var mapMemberURL = i.split("/");
				mapMemberURL.splice(0,1)
				var mapMemberHandler = map[i];
				// check for each part to see if url member matches the url
				for(i in mapMemberURL){
					var segmentOfMapMemberURL = mapMemberURL[i];
					var segmentOfWindowURL = (urlParam_[i] == undefined ? "" : urlParam_[i]);
					log("analyze url ", "map segment is = "+segmentOfMapMemberURL+" -- window segment is = "+segmentOfWindowURL, debug);
					// we chack if segment of Member url has special charecters containing () {} []
					var regexForSpecialChars = /(\((.*?)\))|(\[(.*?)\])/g;
					var extractedValueFromMapMemberURLArray = regexForSpecialChars.exec(segmentOfMapMemberURL);
					// if it doesnt contain any special characters
					if(extractedValueFromMapMemberURLArray == null){
						log("analyze url ", "map segment doesnt contail special characters", debug);
						if(segmentOfMapMemberURL == "*"){
							log("analyze url ", "contains *", debug);
							// if there is continuation to the map url just add the current window segment
							if(i != mapMemberURL.length-1){
								log("analyze url", "in * there is more", debug)
								processedURL.push(segmentOfWindowURL);
								continue;
							// if there is no more map url param left, add the url to the end
							}else{
								log("analyze url", "in * adding to the end", debug)
								var windowUrlFromHereToEnd = urlParam_.slice(i, -1);
								processedURL = processedURL.concat(windowUrlFromHereToEnd);
								break;
							}
						}else if(segmentOfMapMemberURL != segmentOfWindowURL){
							processedURL = [];
							break;
						}else{
							log("analyze url ", "exact match between map url segment and windown url segemnt", debug);
							processedURL.push(segmentOfWindowURL);
							continue;
						}
					// if it does contain speecial character
					}else{
						var extractedValueFromMapMemberURLString = segmentOfMapMemberURL;
						// check for conditions
						// if is sorrounded by [] there are options that need to be checked for
						var regexForBrackets = /(\[(.*?)\])/g;
						var isStrigContainingBrackets = regexForBrackets.exec(extractedValueFromMapMemberURLString);
						// this variable is to be used if
						var StringFromBracket;
						var pushed = false;
						if(isStrigContainingBrackets != null){
							log("analyze url ", "contains brackets -- extracted value is = "+isStrigContainingBrackets[2], debug);
							var arrayOfStringsInBrackets = isStrigContainingBrackets[2].split("|");
							var windowURLMatch;
							for(i_ in arrayOfStringsInBrackets){
								if(segmentOfWindowURL == arrayOfStringsInBrackets[i_]){
									windowURLMatch = segmentOfWindowURL
								}
							}
							if(windowURLMatch){
								log("analyze url ", "contains brackets -- window url segment match the options", debug);
								processedURL.push(segmentOfWindowURL);
								StringFromBracket = segmentOfWindowURL;
								pushed = true;
							}else{
								log("analyze url ", "contains brackets -- window url segment doesn't match map", debug);
								processedURL = []
								continue;
							}
							// else{
							// 	log("analyze url ", "contains brackets -- window url doesnt match so replacing the first memeber of options", debug);
							// 	StringFromBracket = arrayOfStringsInBrackets[0];
							// 	processedURL.push(StringFromBracket);
							// }
						}
						// if is sorrounded by () there is a variable that must be extracted
						var isStrigContainingPrathesis = /(\((.*?)\))/g.exec(extractedValueFromMapMemberURLString);
						if(isStrigContainingPrathesis != null){
							log("analyze url ", "contains prathesis -- variable is = "+isStrigContainingPrathesis[2]+" -- extracted value is = "+segmentOfWindowURL, debug);
							if(!pushed){
								processedURL.push(segmentOfWindowURL);
								variables[isStrigContainingPrathesis[2]] = segmentOfWindowURL;
							}else{
								variables[isStrigContainingPrathesis[2]] = StringFromBracket;
							}
						}
					}
				}
				// check if match has been found
				if(processedURL.length > 0){
					log("analyze url", "match has found for "+mapMemberURL+" and the proccessed url is "+processedURL, debug);
					handler = mapMemberHandler;
					variables.url = processedURL.join("/");
					lastValidUrl = variables.url;
					log("variables", variables, debug);
					handler(variables);
					break;
				}
			}
			// if no match has been found
			if(!handler){
				log("analyze url", "----404-----", debug);
				lastValidUrl = URLParam.join("/")
				var view = new Page("error404");
				view.render();
			}
		}

		// push state
		var popstated = false;
		var pushState = function(){
			if(popstated){
				popstated = false; return;
			}
			//
			if(lastValidUrl.length > 0 && lastValidUrl[lastValidUrl.length-1] != "/"){
				lastValidUrl = lastValidUrl+"/";
			}
			var states = lastValidUrl;
			var finalURL = window.location.origin+"/"+states;
			if(history.pushState){window.history.pushState(null, finalURL, finalURL)};
		}

		var listenToPopState = function(){
			window.addEventListener('popstate', function(event) {
				popstated = true;
				processURL(document.URL);
			});
		}

		return{
			init: init,
			processURL: processURL,
			setBaseUrl: setBaseUrl,
			pushState: pushState,
		}

	}())
    var loadingHTML = "<div id=\"loading\"> <div> <span class=\"logo\"></span> <span class=\"text\">در حال دریافت اطلاعات</span> <span class=\"animation\"><span></span><span></span><span></span><span></span><span></span><span></span><span></span></span> </div> </div>"

//view
	View = {

		templateAddress: false,
		callback: false,
		pageReady: false,
		html: false,

		terminateFunc: function(){},

		page: function(templateAddress_, wait_){
			// check if cureent page match
			Router.pushState();
			if(this.templateAddress == templateAddress_) return this;
			// page is in progg
			this.pageReady = false;
			// terminate current page function
			this.terminateFunc()
			// declare the page
			var page = $("#page")
			// var newPage = $("<div id=\"page\" class=\"new out\"></div>")
			// page.after(newPage)
			// asign new template address
			this.templateAddress = templateAddress_;
			// get the template file
			var html = $(templates.find("#"+this.templateAddress+"_template").html());
			this.html = html;
			page.html(html);
			if(wait_){
				page.append(pageLoading);
			}
			// newPage.removeClass("out")
			//
			// wait for transition to end
			// var that = this;
			// bindToTransition(page.addClass("out"), function(a, e){
			// 	// insert the html
			// 	page.remove();
			// 	newPage.removeClass("new")
			// })
			return this;
		},

		overlayTemplateAddress: false,
		overlayReady: false,

		overlayTerminateFunc: function(){},

		overlay: function(templateAddress_, wait_){
			// check if cureent page match
			// if(this.overlayTemplateAddress == templateAddress_){return this;}
			// page is in progg
			this.overlayReady = false;
			// terminate current page function
			this.overlayTerminateFunc();
			// asign new template address
			this.overlayTemplateAddress = templateAddress_;
			// get the template file
			var html = $(templates.find("#"+this.overlayTemplateAddress+"_template").html());
			// declare the page
			var overlay = $(".overlay")
			var innerOverlay = $("<div/>", {id: templateAddress_, class: "body"})
			// insert the html
			innerOverlay.html(html);
			overlay.html(innerOverlay);
			overlay.prepend("<button id=back onclick=$('.overlay').click()></button>")
			bindToAnimation(overlay.show().addClass("overlay-fade-in"), function(a, e){
				e.removeClass("overlay-fade-in").addClass("overlay-done")
			})
			var that = this;
			overlay.click(function(e){
				if(e.target == this){
					bindToAnimation(overlay.removeClass("overlay-done").addClass("overlay overlay-fade-out"), function(a, e){
						e.removeClass("overlay-fade-out").hide().html("")
						that.overlayTerminateFunc = function(){};
						that.overlayOverScrollFunc = function(){};
						that.overlayResizeFunc = function(){};
					})
				}
			})
			// show the overlay
			overlay.show();
			centerVertical(innerOverlay)
			return html;
		},

		clearLoading: function(callback){
			// if page is ready
			// if(this.pageReady){
			// 	// run callback
			// 	callback()
			// 	// clean the callback
			// 	this.callback = false;
			// 	// fad ein the callback
			// 	var page = $("#page")
			// 	page.removeClass("out");
			// }else{
			// 	this.callback = callback;
			// }
			$("#page-loading").remove()
		},

		clearOverlayLoading: function(){
			var overlay = $("#overlay");
			overlay.find("#page-loading").remove();
		},

        menuDrawn: false,
        drawMenu: function(elem_, map_){
            var menu = $("<div/>", {id: "hover-menu"})
            var cover = $("<div/>", {id: "cover", onclick: "View.clearMenu()"})
            $("body").append(menu)
            $("body").append(cover)
            for(i in map_){
            	if(i == "title"){ 
            		menu.append("<title>"+map_["title"]+"</title>");
            	}else{
	                menu.append("<button onclick="+map_[i]+">"+i+"</button>")
            	}
            }
            var bottom = elem_.offset().top + elem_.height() + 10;
            var middle = elem_.offset().left + elem_.width()/2 - menu.width()/2;
            // add overlay
            $("#hover-menu")
                .css("position", "absolute")
                .css("background", "#FFF")
                .css("top", bottom+"px")
                .css("left", middle+"px")
            this.menuDrawn = true;
        },

        clearMenu: function(){
            if(!this.menuDrawn) return
            $("#cover").remove()
            $("#hover-menu").remove()
        }

	}

//view
var Pager = (function(){

    var templateAddress = false;
    var callback = false;
    var pageReady = false;
    var html = false;

    var terminateFunc = function(){}

    var page = function(templateAddress_, wait_){
        // if page is in progress, do not proceed
        if(!pageReady){return;}
        // push the new state
        Router.pushState();
        // if template address is the same
        if(templateAddress == templateAddress_) return this;
        // set page is in progress so that new page cannot be initailized
        pageReady = false;
        // terminate current page function
        terminateFunc()
        // declare the page
        var page = $("#page")
        // add a new page which is invisible
        var newPage = $("<div id=\"page\" class=\"new out\"></div>")
        page.after(newPage)
        // assign new template address
        templateAddress = templateAddress_;
        html = $(templates.find("#"+templateAddress+"_template").html());
        newPage.html(html);
        newPage.removeClass("out")
        // wait for transition to end
        bindToTransition(page.addClass("out"), function(a, e){
        	// insert the html
        	page.remove();
        	newPage.removeClass("new")
        })
    }

}());

//http request
	/**
	* create a model for the url
	*/
	var httpRequest = function(url, param, callback){
			$.ajax({
				// properties
				type: 'GET',
				url: url,
				data: param,
				dataType: "json",
				tryCount : 0,
    			retryLimit : 3,
    			// sucess
				success:function(data){
					callback(data)
				},
				// error
				error: function(xhr, textStatus, errorThrown){
					if (textStatus == 'timeout') {
			            this.tryCount++;
			            if (this.tryCount <= this.retryLimit) {
			                // try again
			                $.ajax(this);
			            }
			        }
			        if (xhr.status == 500) {
						log("HttpRequest error", errorThrown);
			        } else {
			            //handle error
						log("HttpRequest unknown error", errorThrown);
			        }
				}
			});
	};
//view
	var drawCoverBelow= function(cls, zindex, duration, callback){
		var cover = $('<div class="'+cls+'" id="cover"></div>');
		$('body').append(cover);
		cover.click(function(){
			fadeOutAndRemoveElem(cover)
			callback()
		})
	}

	var fadeOutAndRemoveElem = function(elem){
		bindToAnimate(elem.addClass("fade-out"), function(e){
			if(e){
				elem.remove()
			}else{
				elem.remove()
			}
		});
	}

	var fadeOut = function(elem_, callback_){
		//elem_.css("opacity", "0").removeClass("fade-in").addClass("fade-active fade-out")
		bindToTransition(elem_.addClass("fade-active fade-out"), function(a, e){
            // e.removeClass("fade-active fade-out")
            callback_()
		})
	}

	var fadeIn = function(elem_, callback_){
		elem_.removeClass("fade-out").addClass("fade-active fade-in")
		bindToTransition(elem_, function(a, e){
			e.removeClass("fade-active fade-in")
            callback_()
		})
	}

//socket
	var socket = (function(){

		var socket = {};
		var events = {};
		var pileOfRegisteredString = [];

		var init = function(){
			say("attempting to connect")
			if(typeof(WebSocket) != "function"){return false;}
			if(!userHelper.user._id || !res.socketID){return false;}
			userHelper._id;
			socket = new WebSocket("ws:joky.ir:81/"+userHelper.user._id+"/"+res.socketID);

			socket.onmessage = function(message){
				say("message", message);
				var serverMessage = JSON.parse(message.data);
				var firstKey = serverMessage.event;
				var data = serverMessage.data;
				var handler = events[firstKey];
				if(handler){
					handler(data);
				}
			}

			socket.onopen = function(){
				onOpen();
			};

			socket.onerror = function(){
				onError();
			};

			socket.onclose = function(){
				onClose();
			}
		}

		var onOpen = function(){
			console.log("OPEN")
			reconnecting = false;
			if(reconnectingProcess){
				clearTimeout(reconnectingProcess);
			}
		}

		var reconnecting = false;
		var reconnectingProcess;
		var onClose = function(){
			console.log("CLOSE");
			if(!reconnecting){
				reconnecting = true;
				reconnectingProcess = setTimeout(function(){init()}, 5000)
			}
		}

		var onError = function(){
			console.log("ERROR");
			reconnectingProcess = setTimeout(function(){init()}, 5000)
		}

		var onMessage = function(){
			console.log("MESSAGE")
		}

		var on = function(Event, callback){
			if(typeof(WebSocket) != "function"){return false;}
			events[Event] = callback;
			// if is socket open send the register string
			if(socket.readyState == 1){
				emit("register-for-string", Event);
				// else stack them and add them on open
			}else{
				pileOfRegisteredString.push(Event);
			}
		}

		var emit = function(String, Message){
			if(typeof(WebSocket) != "function"){return false;}
			var mess = {};
			mess["key"] = String;
			mess["message"] = Message;
			socket.send(JSON.stringify(mess));
		}

		return{
			init: init,
			on: on,
			emit: emit
		}

	}())

// async func
    async = function(array_, callback_){
        var results = [];
        for(i in array_){
            var thisFunc = array_[i]
            thisFunc(function(err, result){
                results.push(result);
                if(results.length == array_.length){callback_(null, results)}
            })
        }
    }

//validate form
	validateForm = function(parent_){
		var return_ = true;
		$.each(parent_.find("input, textarea"), function(){
			elem = $(this)
			var value = elem.val();
			if((elem.attr('type') == "number") && isNaN(parseInt(value)) && (value != "")){
				tooltip({element: elem, message:'Value must be number'}); return_ =false; return false;
			}else if(elem.attr('required') == "true"){
				if(value.length < elem.attr('min')){tooltip({element: elem, message: 'حداقل کاراکتر مجاز '+elem.attr('min')+" است!"}); return_ =false; return false;}
				if(value.length > elem.attr('max')){tooltip({element: elem, message: 'حداکثر کاراکتر مجاز '+elem.attr('max')+" است!"}); return_ =false; return false;}
			}
		});
		return return_;
	}
//tooltip
	tooltip = function(obj_){
		var identifier = makeId();
		$('.tooltip').remove();
		var node = $("<div/>", {"class": "tooltip "+identifier, "text": obj_.message});
		obj_.element.parent().css("position","relative").append(node);
		$('.tooltip.'+identifier).css("top", obj_.element.parent().height() + 15 + "px")
		var timeout = (obj_.timeout ? obj_.timeout : 3000)
		setTimeout(function(){
				$('.tooltip.'+identifier).remove()
		},timeout);
	}
	betterTooltip = function(element_){
		var identifier = makeId();
		$('.tooltip').remove();
		var node = $("<div/>", {"class": "tooltip "+identifier, "text": element_.attr("tooltip")});
		element_.parent().css("position","relative").append(node);
		node.css("top", element_.parent().height() + 15 + "px")
		node.css("left", element_.position().left + element_.width()/2 - node.width()/2 + "px")
		setTimeout(function(){
				$('.tooltip.'+identifier).remove()
		},5000);
	}

	removeBetterTolltip = function(element_) {
		element_.parent().find(".tooltip").remove();
	}

	tooltipListen = function(dom_) {

			$(document).on("mouseover", "[tooltip]", function(){
				betterTooltip($(this));
			})

			$(document).on("mouseleave", "[tooltip]", function(){
				removeBetterTolltip($(this));
			})

	}
//makeId
	makeId = function(){
		var text = "id";
		var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
		for( var i=0; i < 10; i++ ){
			text += possible.charAt(Math.floor(Math.random() * possible.length));
		}
		return text;
	}
//localstorage
	store = function(key_, value_){
		if(!value_){
			return localStorage.getItem(key_);
		}else{
			localStorage.setItem(key_, value_)
		}
	}
//log
	var log = function(flag_, message_, debugMode_){
		if(debugMode_ === false){return false}
        if(!message_){
        	message_ = "NULL";
        }
        console.log('(---'+flag_+'---) =>', message_);
    }
	var 
	say = log;
//centerVertical
	var centerVertical = function(section_){
		var margin =  ((window.innerHeight - section_.height())/2);
 		if(margin < 50){margin = 50}
		section_.css('margin',margin+'px auto')
	}
//gridify
	var gridify = function(section_,itemWidth_){
		var itemWidth = itemWidth_;
		var itemPerRow = Math.floor(section_.width()/itemWidth);
		if(itemPerRow == 0){itemPerRow = 1;}
		if(section_.attr('item-per-row') == itemPerRow){return false;}
		section_.html('')
		.attr('item-per-row', itemPerRow);
		for (var i = itemPerRow; i >= 1; i--) {
			section_.append('<div class="column column_'+i+'"></div>');
			section_.find('.column').css('width', (100/itemPerRow)+'%');
		};
		return true;
	};
//clean text
	var cleanText = function(preText_){
		var cleanText = preText_.replace(/(?:\r\n|\r|\n)/g, '<br />').replace(/"/g, '\"');
		return cleanText;
	}
//auto image upload
	var autoImageUpload = function(){
		$("body").on("click","*[auto-upload-button]",function(){
			var parent = $(this).parent();
			parent.find("input[type=file]").click();
			parent.find("input[type=file]").on("change", function(){
				uploadImage($(this));
			})
		})
	}

	var uploadImage = function(image_){
		if(!u_image_validation(image_)){return 0;};
		image_.parent().find("img").css('background','url('+URL.createObjectURL(image_[0].files[0])+') 50% 50%').css("background-size","cover");
		var formData = new FormData();
	    formData.append('image', image_[0].files[0]);
		formData.append('name', name);
	    $.ajax({ type: 'POST', url: base_url+"api/"+image_.attr("upload-url"), data: formData, processData: false, contentType: false,
			success:function(data){
				image_.attr("image-name",data.image);
			},
		});
	}
//image validation
	u_image_validation = function(file_){
		if(file_.val() != ''){
			var type = file_.val().split('.').pop().toLowerCase();
			if($.inArray(type, ['gif','png','jpg','jpeg']) == -1) {
				tooltip({element: file_, message: messages['image_mis_type'],identifier: 'error'})
			return 0;
			} else if(file_[0].files[0].size > 500000){
				tooltip({element: file_, message: messages['image_too_large'],identifier: 'error'})
			return 0;
			}
			return 1;
		}else{
			return 0;
		}
	}
//get time
	get_time = function(date){

		var clientNow = Date.now()/1000;
		var itemTime = date/1000;

		var timeDeffrence = clientNow - itemTime;

		var timeDeffrenceInMinute = Math.floor(timeDeffrence/60);

		var timeDeffrenceInHour = Math.floor(timeDeffrenceInMinute/60);

		var timeDeffrenceInDay = Math.floor(timeDeffrenceInHour/24);

		if(timeDeffrenceInMinute < 59){
			return (timeDeffrenceInMinute+' دقیقه پیش');
		}else if(timeDeffrenceInHour < 23){
			return (timeDeffrenceInHour+' ساعت پیش');
		}else if(timeDeffrenceInDay < 31){
			return (timeDeffrenceInDay+' روز پیش');
		}else if(timeDeffrenceInDay >= 30){
			var months = Math.floor(timeDeffrenceInDay/30)
			return (months+' ماه پیش');
		}

	}

	var time_now = function(){
		d = new Date;
      return (d.getFullYear()+'-'+d.getMonth()+'-'+d.getDate()+' '+d.toTimeString().split(' ')[0]);
	}
//toast
	toast = function(message_,type_){
		var flasher_id = makeId();
		if($('.flasher_div').length == 0){
			$("body").append("<div class=flasher_div></div>")
		}
		$('.flasher_div').append('<span class="flasher '+flasher_id+' '+type_+'"><span class="body">'+message_+'</span></span>');
		setTimeout(function(){
			$('.flasher.'+flasher_id)
			.addClass('flasher_out')
			.bind("animationend webkitAnimationEnd oanimationend MSAnimationEnd", function(){
				$('.flasher.'+flasher_id).remove().unbind();
			});
		}, 5000);
		$('.flasher.'+flasher_id).click(function(){
			$('.flasher.'+flasher_id)
			.addClass('flasher_out')
			.bind("animationend webkitAnimationEnd oanimationend MSAnimationEnd", function(){
				$('.flasher.'+flasher_id).remove().unbind();
			});
		})
	}
//scroll to bottom of div
	scrollToBottom = function(div_){
	    div_[0].scrollTop = div_[0].scrollHeight;
	}
//empty message
	$.prototype.isEmpty = function(isEmpty_){
		if(isEmpty_ && this.find(".empty_message").length == 0){
			this.append("<span class=empty_message>Sorry, No result(s) found!</span>")
		}else if(!isEmpty_ && this.find(".empty_message").length == 1){
			this.find(".empty_message").remove();
		}
	}
//detect CSS Feature
	var isAnimationAvailable = function(){
		var animation = false, animationstring = 'animation', keyframeprefix = '', domPrefixes = 'Webkit Moz O ms Khtml'.split(' '), pfx = '', elm = document.createElement('div'); if( elm.style.animationName !== undefined ) { animation = true; } if( animation === false ) { for( var i = 0; i < domPrefixes.length; i++ ) { if( elm.style[ domPrefixes[i] + 'AnimationName' ] !== undefined ) { pfx = domPrefixes[ i ]; animationstring = pfx + 'Animation'; keyframeprefix = '-' + pfx.toLowerCase() + '-'; animation = true; break; } } }
		return animation;
	}

	function detectCSSFeature(featurename){
	var feature = false,
	domPrefixes = 'Webkit Moz ms O'.split(' '),
	elm = document.createElement('div'),
	featurenameCapital = null;

	featurename = featurename.toLowerCase();

	if( elm.style[featurename] !== undefined ) { feature = true; }

	if( feature === false ) {
	featurenameCapital = featurename.charAt(0).toUpperCase() + featurename.substr(1);
	for( var i = 0; i < domPrefixes.length; i++ ) {
	    if( elm.style[domPrefixes[i] + featurenameCapital ] !== undefined ) {
	      feature = true;
	      break;
	    }
	}
	}
	return feature;
	}
//bind to animate
	var bindToAnimate = function(elem, callback){
		if(isAnimationAvailable()){
			elem.bind("animationend webkitAnimationEnd oanimationend MSAnimationEnd", function(e){
				$(this).unbind("animationend webkitAnimationEnd oanimationend MSAnimationEnd")
				callback(this);
			})
		}else{
			callback(false);
		}
	}

	var bindToAnimation = function(elem, callback){
		if(isAnimationAvailable()){
			elem.bind("animationend webkitAnimationEnd oanimationend MSAnimationEnd", function(e){
				$(this).unbind("animationend webkitAnimationEnd oanimationend MSAnimationEnd")
				callback(true, elem);
			})
		}else{
			callback(false, elem);
		}
	}
	var bindToTransition = function(elem, callback){
		if(isAnimationAvailable()){
			elem.bind("transitionend webkitTransitionEnd otransitionend MSTransitionEnd", function(e){
				$(this).unbind("transitionend webkitTransitionEnd otransitionend MSTransitionEnd")
				callback(true, elem);
			})
		}else{
			callback(false, elem);
		}
	}
//press enter
	var listenToEnter = function(){
		$("[listenToKey=true] input").keypress(function(e){
			if(e.keyCode == 13){
				$(this).parents("[listenToKey=true]").find("button").first().click();
			}
		})
	}
//bind
	function DataBinder(object_id) {
	  // Use a jQuery object as simple PubSub
	  var pubSub = $({});

	  // We expect a `data` element specifying the binding
	  // in the form: data-bind-<object_id>="<property_name>"
	  var data_attr = "bind-" + object_id,
	      message = object_id + ":change";

	  // Listen to change events on elements with the data-binding attribute and proxy
	  // them to the PubSub, so that the change is "broadcasted" to all connected objects
	  $(document).on("change", "[data-" + data_attr + "]", function(evt){
	    var $input = $(this);
	    pubSub.trigger(message, [$input.data(data_attr), $input.val()]);
	  });

	  // PubSub propagates changes to all bound elements, setting value of
	  // input tags or HTML content of other tags
	  pubSub.on( message, function( evt, prop_name, new_val ) {
	    $( "[data-" + data_attr + "=" + prop_name + "]" ).each( function() {
	      var $bound = $( this );

	      if ( $bound.is("input, textarea, select") ) {
	        $bound.val( new_val );
	      } else {
	        $bound.html( new_val );
	      }
	    });
	  });

	  return pubSub;
	}
