//region messages
var messages = {
    'loading': 'لطفا چند لحظه صبر کنید',
    'signup-email': 'این ایمیل قبلا استفاده شده!',
    'signup-name': 'این نام قبلا استفاده شده!',
    'profile_edit': 'اطلاعات پروفایل شما به روز شد',
    'profile_thumbnail': 'تصویر پرفایل شما با موفقیت به روز شد',
    'comment_noti': '',
    'like_noti': '',
    'follow_noti': '<span></span>',
    'empty_noti': 'پیامی برای مشاهده وجود ندارد',
    'true_follow': 'دنبال می کنید',
    'false_follow': 'دنبال کنید',
    'true_register': 'عضو هستید',
    'false_register': 'عضو شوید',
    'upload_image_succ': 'تصویر با موفقیت آپلود شد',
    'image_mis_type': 'فایل مورد نظر اشتباه است',
    'image_too_large': 'سایز تصویر بزرگتر از حد مجاز است',
    'status_online': 'آنلاین',
    'status_viewing': 'آنلاین - مشاهده این چت',
    'status_offline': 'آفلاین',
    "imageIsBeingLoaded": "تصویر در حال آپلود شدن است!",
    "text_placeholder": "هر چه می خواهد دل تنگت بگو...",
    "image_placeholder": "توضیح در مورد تصویر - اختیاری",
    "link_placeholder": "توضیح در مورد لینک - اختیاری",
    "poll_placeholder": "توضیح در مورد نظر سنجی - اختیاری",
    "no-user_login_message": "نام کاربری یا رمز عبور اشتباه است",
    "blocked_login_message": "حساب کاربری شما مسدود شده است",
    "unknown-error": "Unknown Error!",
    "empty_post": "پست شما خالی است!",
    "unautorized_to_send_post": "شما به علت رعایت نکردن قوانین امکان ارسال پست را برای مدت محدودی نخواهید داشت.",
}
//endregion

// region main
var mainController = (function(){

    var view = false;
    var type = "trending";
    var adapter = false;
    var indicator = false;

    init = function(urlVars_){

        if(urlVars_.type){
            type = urlVars_.type
        }

        if(!userHelper.access(false) && type == "you"){
            type = "trending"
        }

        // request view
        view = View.requestPage("main")

        if(window.innerWidth < 700){
            View.disableInteractiveMenu = false
        }

        View.terminateFunc = function(){
            makeHomeIntoBack();
            View.disableInteractiveMenu = true
        }

        makeHomeIntoSidebar();
        if(!adapter){
            adapter = new Adapter()
            adapter.onOverScroll = true
            adapter.limit = itemClass.guessLimit()
            adapter.constructView = function(item, width){return itemClass.construct(item, width)}
            adapter.disableCache = true
            adapter.emptyMessage = View.getTemplateByID("empty_feed_message")
        }
        //adapter.itemsArray = {}
        adapter.init(view, {gridify: true, gridifyWidth: 290})
        adapter.fetch("/api/item/fetch", {type: type, limit: 20}, type)

        if(!indicator){
            indicator = new ResponsiveIndicator()
        }
        indicator.init(view.find("#filter"), type)

        changeTheFeedToNew()
        listenToFollowings()
    };

    var newItemsArray= [];
    var newItemsCount= 0;
    var listening= false;
    var listenToFollowings = function(){
        if(!this.listening){
            socket.on('post', function(msg){
                var item = msg.item;
            })
            socket.on('new-item', function(msg){
                var item = msg;
                if(item.item_userid = userHelper.user._id){
                    //adapter = null
                    //init("feeds")
                }
            })
        }
    }

    var changeTheFeedToNew = function(){
        if(userHelper.access()){

        }else{
           $("#main").css("padding-top", "0")
           view.find("#filter").remove()
        }
    }

    //var listenTo = function(userId_){
    //    socket.on('post:'+userId_, function(msg){
    //        var item = msg.item;
    //        item.item_userid = msg.item.model;
    //        // push it into newItemsArray
    //        newItemsArray.push(item);
    //        // update the newItemsCount
    //        newItemsCount++;
    //        // do the message
    //        if(newItemsCount>0 && $(".new_item_message").length == 0){
    //            $("#main").append(
    //                "<span class=new_item_message onclick=mainViewController.addNewItems()>"+
    //                "<span class=count>"+newItemsCount+"</span>"+
    //                "<span>پست جدید</span>"+
    //                "</span>"
    //            )
    //        }else if(newItemsCount>0 && $(".new_item_message").length == 1){
    //            $(".new_item_message .count").html(newItemsCount);
    //        }
    //    });
    //}

    ///**
    // * go to feeds
    // * animate to top
    // * rerender the items
    // */
    //var addNewItems = function(){
    //    $('body,html').animate({scrollTop: 0}, ($('body,html').scrollTop/10),
    //        function(){
    //            itemClass.construct(mainViewController.newItemsArray, $("#main .item_div"), {"prepend": true});
    //            $.each(mainViewController.newItemsArray, function(key, value){
    //                mainViewController.model.unshift(value)
    //            })
    //            mainViewController.newItemsArray = [];
    //            mainViewController.newItemsCount = 0;
    //            $('.new_item_message').remove();
    //        }
    //    );
    //}

    redrawItems = function(){
        adapter = null
        init(type)
    }

    return {
        init: init,
        redrawItems: redrawItems,
        view: function(){return view}
    }

}())

//endregion

//region item class
var itemClass = {

    construct: function(model_, width_){
        if(model_ && model_.item_userid){
            return this.template(model_, width_)
        }else if(model_ && model_.name){
            return this.userTemplate(model_)
        }
    },

    template: function(item, width){
        // text split
        var split_text_var = this.split_text(item.body);
        var main_body = split_text_var.main;
        var remaining_body = split_text_var.remaining;
        var more_button_class = split_text_var.button;
        // type
        if(item.attach.type == 'link'){
            var extra = (
            '<div class="extra">'+
            '<a class="title" href="'+item.attach.url+'" target="_blank">'+item.attach.title+'</a>'+
            '<span class="desc" target="_blank">'+item.attach.desc+'</span>'+
            '</div>'
            );
        }else if(item.attach.type == 'image'){
            var ratio = item.attach.dimentions
            var width = width;
            var height = width / ratio;
            var extra = (
            "<div class=\"extra\">"+
            "<img class=\"image\" src=\"/upload/items/"+item.attach.image+"\" width=\""+width+"\" height=\""+height+"\" />"+
            "</div>"
            );
        }else if(item.attach.type == 'video'){
            var extra = (
            '<div class="extra">'+item.attach.iframe+'</div>'
            );
        }else{
            var extra = '';
        }
        // user info
        var thumbnail = '/upload/users/'+item.item_userid.thumbnail;
        var username = item.item_userid.name;
        var user_href='href="/profile/'+item.item_userid._id+'/posts"';
        // comment
        if(item.comment == 0){var comment = 'write'}else{var comment = 'message'}
        // like
        if(itemExtraViewController.isLiked(item._id)){var like = 'liked'}else{var like = ''}
        // template
        var date = (item.sort_date ? get_time(item.sort_date) : item.date.split(" ")[0])
        var template = $(
        '<div class="item '+item._id+'" item-id="'+item._id+'" item-user-id="'+item.item_userid._id+'">'+
        '<div class="header">'+
        '<a '+user_href+' q-profile="true">'+
        '<img src="'+thumbnail+'" width="100%" height="100%">'+
        '<span class="user" user-id="'+item.item_userid._id+'">'+username+'</span>'+
        '<span class="time">'+date+'</span>'+
        '</a>'+
        '<button class="more" onclick=itemExtraViewController.itemMenu(this,\''+item._id+'\')></button>'+
        '<button class="like '+like+'" onclick=itemExtraViewController.like(this)>'+item.like+'</button>'+
        '</div>'+
        extra+
        '<div class="main">'+
        '<span class="body">'+main_body+
        '<span class="remaining">'+remaining_body+'</span>'+
        '</span>'+
        '<span class="reveal '+more_button_class+'" onclick="$(this).parents(\'.main\').toggleClass(\'active\')">ادامه</span>'+
        '</div>'+
             '<div class="comment_div '+comment+'" post-id="'+item._id+'" creator-id="'+item.item_userid._id+'">'+
             '<button class="comment_message" onclick=itemExtraViewController.getComments(this) count="'+item.comment+'"><span>'+item.comment+'</span> نظر ثبت شده</button>'+
             '<div class="comment_list"></div>'+
             '<div class="in_response"></div>'+
             '<div class="comment_write">'+
             '<input type="text" placeholder="نظر خود را بنویسید..." onfocus=itemExtraViewController.commentOnFocus(this) />'+
             '<textarea type="text" placeholder="نظر خود را بنویسید..." onblur=itemExtraViewController.commentOnBlur(this) ></textarea>'+
             '<button class="comment_button" onclick=itemExtraViewController.sendComment(this)></button>'+
             '</div>'+
             '</div>'+
        '</div>'
        );
        if(item.allow_comment != true){
            template.find(".comment_div").hide()
        }
        return template;
    },

    split_text: function(body_){
        // variables
        var max_breaks = 3, max_length = 300;
        var sliced_body = body_, remaining_body = '', button_class='';
        // if count of breaks is too much
        var bodyBreaks = body_.split("<br />").length;
        if(bodyBreaks > (max_breaks+5)){
            // slice body to the max breaks
            var slicePoint = body_.split('<br />', max_breaks).join('<br />').length;
            sliced_body = body_.slice(0, slicePoint);
            sliced_body = sliced_body+"<span class=more splitType=breaks breaks="+bodyBreaks+" breaks="+body_.length+">...</span>"
            // slice the remaining
            remaining_body = body_.slice(slicePoint, body_.length);
            // if lenght of body is too much
        }else if(body_.length > (max_length+300)){
            // slice to the max length and the last space
            sliced_body = body_.slice(0,max_length);
            sliced_body = body_.slice(0,sliced_body.lastIndexOf(" "));
            sliced_body = sliced_body+"<span class=more splitType=lenth breaks="+body_.length+">...</span>"
            // slice remaining
            remaining_body = body_.slice(sliced_body.length, body_.length);
        }
        // get button class
        if(remaining_body != ''){button_class = 'active';}
        return {main: sliced_body, remaining: remaining_body, button: button_class}
    },

    userTemplate: function(item){
        var following = userHelper.is_following(item._id);
        var following_text = messages[following+'_follow'];
        var brief = (!item.brief ? "" : item.brief);
        var template = (
        '<div class="item '+item._id+'" user-id="'+item._id+'">'+
        '<div class="mini_profile">'+
        '<div class="more"><button onclick="profileViewController.menu(this)"></button></div>'+
        '<a href="/profile/'+item._id+'/posts">'+
        '<img src="/upload/users/'+item.thumbnail+'" />'+
        '<span class="name">'+item.name+'</span>'+
        '<span class="brief">'+brief+'</span>'+
        '</a>'+
        '<button class="follow '+following+'" onclick="userHelper.follow(\''+item._id+'\',\''+following+'\')">'+following_text+'</button>'+
        '<div class="stat_div">'+
        '<span><span>'+item.following+'</span>دنبال میکند</span>'+
        '<span><span>'+item.followers+'</span>دنبال کننده</span>'+
        '<span><span>'+item.posts+'</span>پست ها</span>'+
        '</div>'+
        '</div>'+
        '</div>'
        );
        return template;
    },

    /**
     * load items when permanent key is entered
     * @param itemID_
     */
    loadSingleItemInPage: function(itemID_){
        //draw the overlay
        var view = View.requestOverlay("single_item", false)
        // fetch the Item
        itemClass.loadItem(itemID_.id, function(item_){
            view.html(item_)
        })
    },

    /**
     * shiw the permanent key for itemID
     * @param itemID_
     */
    loadPermanentLink: function(itemID_){
        var view = View.requestOverlay("permanent", false)
        view.find("input").val("http://joky.ir/item/"+itemID_)
    },

    /**
     * load an item by ID
     */
    loadItem: function(itemID_, callback){
        var self = this
        httpRequest("/api/item/get_item", {id: itemID_}, function(data){
            var itemTemplate = self.template(data.result)
            callback(itemTemplate)
        })
    },

    guessLimit: function(){
        var itemPerRow = Math.floor(window.innerWidth/290);
        var limit = itemPerRow * 5;
        if(limit < 10){limit = 10};
        return limit;
    },

    getItemByID: function(itemID_){
        return $("[item-div]").find("[item-id="+itemID_+"]")
    },

    drawLayoutMenu: function(this_){
        var map = {}
        var columns = Math.floor(window.innerWidth/300)
        var currentSetting = store("gridify-type")
        if(!currentSetting) currentSetting = columns
        for(var i = 1; i <= columns; i++){
            if(i == 1){
                var thisSetting = (currentSetting == 1 ? true : false)
                map["تک ستونه"] = {
                    callback: "itemClass.changeGridifyType(1)",
                    active: thisSetting,
                }
            }else if(i == 2){
                var thisSetting = (currentSetting == 2 ? true : false)
                map["دو ستونه"] = {
                    callback: "itemClass.changeGridifyType(2)",
                    active: thisSetting,
                }
            }else if(i == 3){
                var thisSetting = (currentSetting >= 3 ? true : false)
                map["چند ستونه"] = {
                    callback: "itemClass.changeGridifyType(3)",
                    active: thisSetting,
                }
            }
        }
        View.drawMenu($(this_), map)
    },

    changeGridifyType: function(type){
        setGridifyType(type)
        mainController.redrawItems()
    }

};
//endregion
//region item extra
itemExtraViewController = {

    setResponse: function(this_){
        var comment = $(this_).parents(".comment_item")
        var userName = comment.find(".name").html()
        var in_response = "<span class='response_name'>"+userName+"</span>"
        var removeResponse = "<span class='remove' onclick='itemExtraViewController.removeResponse(this)'></span>"
        var parent = comment.parents(".item").find(".in_response").show().html("در پاسخ به " + in_response + removeResponse)
    },

    removeResponse: function(this_){
        var comment = $(this_)
        var parent = comment.parents(".item").find(".in_response").html("").hide()
    },

    sendComment: function(this_){
        this_ = $(this_);
        if(!userHelper.access(true)){return false;}
        if(!userHelper.sustain()){return false;}
        var parent = this_.parents('.comment_div');
        if(parent.find('textarea').val().length < 1){return false;}
        var ajax_data = {
            id: parent.attr('post-id'),
            creator: parent.attr('creator-id'),
            body: cleanText(parent.find('textarea').val()),
            in_response: parent.find('.response_name').val()
        };
        // clear the response
        parent.find('.in_response').html("")
        var comment = [{_id: makeId(), body: ajax_data.body, commentor: userHelper.user, sortDate: Date.now()}]
        itemExtraViewController.commentConstruct(comment, parent.find('.comment_list'));
        parent.find('input').show();
        parent.find('textarea').val('').hide();
        httpRequest("/authapi/item_extra/create_comment", ajax_data, function(data){

        })
        // refresh the count
        var count = parseInt(parent.find('.comment_message span').html());
        count++;
        parent.find('.comment_message span').html(count);
        if(parent.hasClass('write')){
            parent.addClass('expand');
            parent.addClass('message');
            parent.removeClass('write');
            parent.attr('loaded', 'true');
        }
    },

    getComments: function(this_){
        this_ = $(this_);
        var parent = this_.parents('.comment_div');
        if(parent.hasClass('expand')){parent.removeClass('expand'); return;}
        if(parent.attr('loaded') == 'true'){parent.addClass('expand'); return;}
        var current_message = parent.find('.comment_message').html();
        this_.html("لطفا چند لحظه صبر کنید...");
        var ajax_data = {
            id: parent.attr('post-id'),
        };
        $.ajax({type: 'GET', url: "/api/item_extra/get_comments", data: ajax_data, dataType:"json",
            success:function(data){
                parent.addClass('expand');
                parent.attr('loaded', 'true');
                this_.html(current_message);
                itemExtraViewController.commentConstruct(data.result, parent.find('.comment_list'))
            },
        });
    },

    commentConstruct: function(comments_, div_){
        $.each(comments_, function(key, item){
            var template = (
            '<div class="comment_item" owner="'+item.commentor._id+'">'+
            '<button class=more onclick="itemExtraViewController.comment_menu(this, \''+item._id+'\')"></button>'+
            // '<button class=reply onclick="itemExtraViewController.setResponse(this)"></button>'+
            '<a href="/profile/'+item.commentor._id+'/posts">'+
            '<img src="/upload/users/'+item.commentor.thumbnail+'" />'+
            '</a>'+
            '<a class="user" href="/profile/'+item.commentor._id+'/posts">'+
            '<span class="name">'+item.commentor.name+'</span>'+
            '<span class="date">'+get_time(item.sortDate)+'</span>'+
            '</a>'+
            '<span class="body">'+item.body+'</span>'+
            '</div>'
            );
            div_.append(template);
        });
        scrollToBottom(div_);
    },

    commentOnFocus: function(this_){
        $(this_).hide();
        $(this_).parent().find("textarea").show().focus();
        $(this_).parent().find("button").addClass("active");
    },

    commentOnBlur: function(this_){
        if($(this_).val() == ""){
            $(this_).hide();
            $(this_).parent().find("input").show();
            $(this_).parent().find("button").removeClass("active")
        }
    },

    like: function(this_){
        this_ = $(this_);
        thisClass = this
        if(!userHelper.access(true)){return false;}
        var parent = this_.parents('.item');
        var ajax_data = {
            id: parent.find('.comment_div').attr('post-id'),
        };
        $.ajax({type: 'GET', url: "/authapi/item_extra/like", data: ajax_data, dataType:"json"});
        var curr_val = this_.html();
        if(this_.hasClass('liked')){
            var like = true;
            this_.removeClass('liked');
            thisClass.setItemLikeStatus(ajax_data.id, true)
            curr_val--;
            this_.html(curr_val);
        }else{
            var like = false;
            this_.addClass('liked')
            thisClass.setItemLikeStatus(ajax_data.id, false)
            curr_val++;
            this_.html(curr_val);
        }
    },

    setItemLikeStatus: function(itemID_, remove_){
        if(!this.likedItemArray) this.likedItemArray = localStorage.getItem("likedItemArray")
        if(!this.likedItemArray) this.likedItemArray = ""
        if(remove_){
            this.likedItemArray.replace(itemID_+",", "")
        }else{
            this.likedItemArray += itemID_+","
        }
        localStorage.setItem("likedItemArray", this.likedItemArray)
    },

    likedItemArray: false,
    isLiked: function(itemID_){
        if(!this.likedItemArray) this.likedItemArray = localStorage.getItem("likedItemArray")
        if(!this.likedItemArray) this.likedItemArray = ""
        if(this.likedItemArray.indexOf(itemID_) != -1){
            return true
        }
        return false
    },

    itemMenu: function(this_, itemID_){
        var item = $(this_).parents(".item")
        var postOwner = item.attr('item-user-id')
        var map = {}
        map["title"] = "منوی پست"
        if(userHelper.user._id == postOwner){
            map["حذف این پست"] = "itemExtraViewController.delete_item('"+itemID_+"')"
        }else{
            map["گزارش پست نا مناسب"] = "itemExtraViewController.flag_item('"+itemID_+"')"
        }
        if(!userHelper.is_following(postOwner) && userHelper.user && postOwner != userHelper.user._id){
            map["دنبال کردن این کاربر"] = "userHelper.follow('"+postOwner+"',false)"
        }else if(userHelper.is_following(postOwner) && userHelper.user && postOwner != userHelper.user._id){
            map["دنبال نکردن این کاربر"] = "userHelper.follow('"+postOwner+"',true)"
        }
        // toggle allow comment
        if(userHelper.user._id == postOwner && item.find(".comment_div").is(':visible')){
            map["غیر فعال کردن کامنت"] = "itemExtraViewController.toggleAllowComment('"+itemID_+"',false)"
        }else if(userHelper.user._id == postOwner){
            map["فعال کردن کامنت"] = "itemExtraViewController.toggleAllowComment('"+itemID_+"',true)"
        }
        map["لینک این پست"] = "itemClass.loadPermanentLink(\""+itemID_+"\")"
        View.drawMenu($(this_), map)
    },

    showPermanentList: function(itemID_){
        var view = View.requestOverlay("showlink", false)
    },

    delete_item: function(item_id_){
        var ajax_data = {
            item_id: item_id_,
        };
        $.ajax({type: 'GET', url: "/api/auth/delete_item", data: ajax_data, dataType:"json"});
        $('.item.'+item_id_).addClass('delete').bind('animationend webkitAnimationEnd oanimationend MSAnimationEnd', function(){
            $('.item.'+item_id_).remove();
        })
    },

    toggleAllowComment: function(itemID_, allow_){
        $.ajax({type: 'GET', url: "/authapi/item/disallow_comment", data: {id: itemID_, allow: allow_}, dataType:"json"});
        var itemArr = itemClass.getItemByID(itemID_)
        $.each(itemArr, function(){
            if(allow_){
                itemArr.find(".comment_div").show()
            }else{
                itemArr.find(".comment_div").hide()
            }
        })
    },

    flag_item: function(item_id_){
        var ajax_data = {
            id: item_id_,
        };
        toast("پست مورد نظر به زودی بررسی خواهد شد.")
        $.ajax({type: 'GET', url: "/authapi/item/flag", data: ajax_data, dataType:"json"});
    },

    comment_menu: function(this_, commentID_){
        var postOwner = $(this_).parents(".item").attr('item-user-id')
        var commentOwner = $(this_).parents(".comment_item").attr("owner")
        var map = {}
        map["title"] = "منوی کامنت"
        if(postOwner == userHelper.user._id || userHelper.user._id == commentOwner){
            map["حذف این کامنت"] = "itemExtraViewController.removeComment(\""+commentID_+"\")"
        }else{
            map["گزارش کامنت نامناسب"] = "itemExtraViewController.flag_comment(\""+commentID_+"\")"
        }
        View.drawMenu($(this_), map)
    },

    flag_comment: function(commentId_){
        var ajax_data = {
            id: commentId_,
        };
        toast("کامنت مورد نظر به زودی بررسی خواهد شد.")
        $.ajax({type: 'GET', url: "/api/item_extra/flag_comment", data: ajax_data, dataType:"json"});
    },

    removeComment: function(commentId_){
        var ajax_data = {
            id: commentId_,
        };
        $.ajax({type: 'GET', url: "/authapi/item_extra/remove_comment", data: ajax_data, dataType:"json"});
    }

}
//endregion

//region add
var addViewController = (function(){

    var attachment = {type: "none"};
    var view = false;
    var exit = false;
    var init = function(routerUrl_){
        if(!userHelper.access(true)){
            return false;
        }
        view = View.requestOverlay("add");
        autosize(view.find('textarea'));
    };

    var send = function(){
        if(!userHelper.sustain()){return false;}
        var parent = $('#add');
        if(!userHelper.access(true)) return
        if($('#add .body').val() == '' && attachment.type == 'none'){
            return toast(messages['empty_post'])
        }
        parent.find(".send").attr("disabled", true);

        extractLinkInfo();

        var ajax_data = {
            body: cleanText($('#add .body').val()),
            attachment: attachment,
            comment: store("disable-comment")
        };
        View.makeLoadingButton(view.find(".send"))
        View.makeViewInAccessible(view)

        // ajax
        $.ajax({type: 'POST', url: "/api/auth/add", data: ajax_data, dataType:"json",
            success:function(data){
                View.releaseLoadingButton(view.find(".send"))
                View.releaseViewInAccessible(view)
                View.clearOverlay()
                if(data.done){
                }else if(data.err == "401"){
                    toast(messages["unautorized_to_send_post"])
                }
            }
        });
    };

    var drawSetting = function(){
        var active = (store("disable-comment") == "true" ? true : false)
        var map = {}
        map["title"] = "تنظیمات پست"
        map["غیر فعال کردن نظرات"] = {
            callback: "addViewController.disableComment()",
            active: active
        }
        View.drawMenu(view.find(".post-setting"), map)
    }

    var changePostType = function(event){
        var parent = $('#add');
        var this_button = parent.find('.attachment .'+event);
        var this_div = parent.find('.responses_div #'+event);
        this_button.addClass('active');
        this_div.addClass('active');
        parent.find('.responses_div > .active:not(#'+event+')').removeClass('active');
        parent.find('.attachment > .active:not(.'+event+')').removeClass('active');
        attachment.type = event;
        parent.find("textarea").addClass("mini");
        parent.find("textarea").attr("placeholder", messages[event+"_placeholder"]);
    };

    var sendImage = function(){
        var parent = $("#add #image");
        var imageInput = parent.find("input.image_file");
        var imageFile = imageInput[0].files[0];
        if(!u_image_validation(imageInput)){return 0;};
        var formData = new FormData();
        formData.append('image', imageFile);
        // localy show the image
        parent.find("img").show().attr('src', URL.createObjectURL(imageFile));
        parent.find(".image_button").hide();
        parent.find(".remove_image").show();
        View.makeLoaderOverlay(parent)
        // send ajax
        $.ajax({ type: 'POST', url: "/api/auth/addImage", data: formData, processData: false, contentType: false,
            success:function(data){
                View.releaseLoaderOverlay(parent)
                // show image is loaded
                parent.find("img").removeClass("loading")
                // change the attachment type
                attachment.image = data.image;
                attachment.dimentions = parent.find("img").width() / parent.find("img").height();
            },
        });
    }

    var removeImage = function(){
        var parent = $("#add #image");
        View.releaseLoaderOverlay(parent)
        parent.find("img").removeClass("loading").attr("src", null).hide();
        parent.find(".image_button").show();
        parent.find(".remove_image").hide();
        delete attachment.image;
        delete attachment.dimentions;
        $(".overlay").attr("confirmOnExit","false");
    }

    var processLink = function(){
        var parent = $("#add #link");
        var url = parent.find("input").val();
        parent.find(".check").html(messages["loading"]);
        $.ajax({type: 'GET', url: "/api/auth/processLink", data: {"url": url}, dataType:"json",
            success: function(data){
                if(data.result != true){
                    parent.find(".check").html("لینک مورد نظر یافت نشد");
                    return;
                }
                var title = (data.title ? data.title : "");
                var description = (data.description ? data.description : "");
                parent.find("input").hide();
                parent.find(".check").hide().html("بررسی لینک");
                parent.find(".result .title").val(title);
                parent.find(".result .desc").val(description);
                parent.find(".result").show();
                // attachment
                attachment.url = data.url;
                $(".overlay").attr("confirmOnExit","true");
            }
        })
    }

    var removeLink = function(){
        var parent = $("#add #link");
        parent.find("input").show();
        parent.find(".result").hide();
        parent.find(".check").show()
        delete attachment.url;
        $(".overlay").attr("confirmOnExit","false");
    }

    var extractLinkInfo = function(){
        var parent = $("#add #link");
        attachment.title = parent.find(".result .title").val();
        attachment.desc = parent.find(".result .desc").val();
    }

    var onTextChangingListener = function(this_){
        if($(this_).val().length > 3){
            $(".overlay").attr("confirmOnExit","true");
        }else{
            $(".overlay").attr("confirmOnExit","false");
        }

    }

    var checkVideo = function(){
        var parent = $("#add #video");
        var url = parent.find("input").val();
        parent.find(".check").html(messages["loading"]);
        var testForIframe = $("<div/>");
        testForIframe.html(url);
        if(testForIframe.find("iframe").length == 1){
            parent.find("input").hide();
            parent.find("> span").hide();
            parent.find(".check").hide().html("بررسی لینک");
            parent.find(".result .iframe").html(testForIframe.html());
            parent.find(".result .iframe iframe").css("width", "100%");
            parent.find(".result").show();
            // attachment
            attachment.iframe = testForIframe.html();
            $(".overlay").attr("confirmOnExit","true");
            return;
        }
        $.ajax({type: 'GET', url: "/api/auth/processVideo", data: {"url": url}, dataType:"json",
            success: function(data){
                parent.find("input").hide();
                parent.find("> span").hide();
                parent.find(".check").hide().html("بررسی لینک");
                parent.find(".result .iframe").html(data.iframe);
                parent.find(".result .iframe iframe").css("width", "100%");
                parent.find(".result").show();
                // attachment
                attachment.iframe = data.iframe;
                $(".overlay").attr("confirmOnExit","true");
            }
        })
    }

    var removeVideo = function(){
        var parent = $("#add #video");
        parent.find("input").val("").show();
        parent.find(".result").hide();
        parent.find("> span").show();
        parent.find(".check").show();
        delete attachment.iframe;
        $(".overlay").attr("confirmOnExit","false");
    }

    var disableComment = function(){
        var currentState = store("disable-comment")
        if(!currentState || currentState == "false"){
            store("disable-comment", "true")
        }else{
            store("disable-comment", "false")
        }
    }

    return{
        init: init,
        send: send,
        drawSetting: drawSetting,
        changePostType: changePostType,
        sendImage: sendImage,
        removeImage: removeImage,
        processLink: processLink,
        removeLink: removeLink,
        onTextChangingListener: onTextChangingListener,
        checkVideo: checkVideo,
        removeVideo: removeVideo,
        disableComment: disableComment,
    }

}());
//endregion

//region sign
sign = {

    init: function(type_){
        var view = View.requestOverlay(type_.type);
    },

    signin: function(){
        var parent = "#signin";
        var ajax_data = {
            email: 	$(parent).find('.email').val(),
            password: 	$(parent).find('.password').val(),
        };
        $(parent).find(".error_label").remove();
        $.ajax({type: 'GET', url: "/api/login", data: ajax_data, dataType: "json",
            success:function(data){
                if(data.response == "ok" || data.response == "not-verified"){
                    window.location.replace("/");
                }else if(data.response == "no-user"){
                    $(parent).find(".header_div").after('<label class="error_label">'+messages[data.response+"_login_message"]+'</label>');
                }else if (data.response == "blocked") {
                    $(parent).find(".header_div").after('<label class="error_label">'+messages[data.response+"_login_message"]+'</label>');
                }
            }
        });
    },

    signup: function(){
        var parent = "#signup";
        if(!validateForm($(parent))){return false;}
        var ajax_data = {
            email: 	$(parent).find('.email').val(),
            name: 	$(parent).find('.name').val(),
            password: 	$(parent).find('.password').val(),
        };
        $.ajax({type: 'GET', url: "/api/signup", data: ajax_data, dataType: "json",
            success:function(data){
                if(data.error){
                    tooltip({element: $(parent).find('*[bind='+data.error+"]"), message: messages["signup-"+data.error]});
                }else{
                    window.location.replace("/");
                }
            }
        });
    },

    logout: function(){
        $.ajax({type: 'GET', url: "/api/logout", dataType: "json",
            success: function(){location.reload();},
        });
    },

    checkEmail: function(){
        var parent = "#forgot-password";
        if(!validateForm($(parent))){return false;}
        var ajax_data = {
            email: 	$(parent).find('.email').val(),
        };
        $.ajax({type: 'GET', url: "/api/sendPasswordLinkToEmail", data: ajax_data, dataType: "json",
            success:function(data){
                if(data.result == "succ"){
                    toast("لینک تغییر رمز عبور به ایمیل مورد نظر ارسال شد");
                    View.clearOverlay();
                }else{
                    $(parent).find(".texter").html("ایمیل مورد نظر در سیستم یافت نشد!")
                }
            }
        });
    }

}
//endregion
//region user helper
userHelper = {

    user: false,
    refetch: false,

    init: function(){
        if(User === undefined){
            User = {}
        }
        this.user = User
        this.RUD($("body"), this.user)
        if(!this.user) return
        this.initFollow()
    },

    followArray: [],
    initFollow: function(){
        var self = this
        var rawUserFollow = store("user-follow-data")
        if(!rawUserFollow){
            rawUserFollow = JSON.stringify(["empty"])
            store("user-follow-data", rawUserFollow)
        }
        this.followArray = JSON.parse(rawUserFollow)
        var lastFollowEdit = store("last-follow-edit")
        if(!lastFollowEdit) lastFollowEdit = 0
        // update the user info
        httpRequest("/api/auth/get-following-data-since", {since: lastFollowEdit}, function(data){
            var result = data.result
            for(var i in result){
                self.writeFollowDataToLocalStorage(result[i].is_following, result[i].target_user_id)
            }
            self.commitChanges()
            if(self.user.following != self.followArray.length && !self.refetch){
                self.refetch= true // sometime would go into infinite loop :|
                store("user-follow-data", "[]")
                store("last-follow-edit", "0")
                self.initFollow()
            }
        })
    },

    RUD: function(section_, model_){
        if(model_._id){
            $.each(section_.find('[RUD]'),function(){
                var type = $(this).attr('RUD');
                var value = model_[type];
                if(!value){return;}
                if($(this)[0].tagName == 'INPUT'){$(this).val(value)}
                else if($(this)[0].tagName == 'IMG'){$(this).attr('src', '/upload/users/'+value)}
                else{$(this).html(value)}
            });
            $.each(section_.find('a'),function(){
                var orig_href = $(this).attr('href');
                if(orig_href !== undefined){
                    var replace_with = orig_href.substring(orig_href.lastIndexOf("{")+1,orig_href.lastIndexOf("}"));
                    if(replace_with != ''){
                        if(replace_with.length == 1){var new_value = siteM.return_()[replace_with]}
                        else{var new_value = model_[replace_with]}
                        var new_href = orig_href.replace('{'+replace_with+'}', new_value);
                        $(this).attr('href', new_href);
                    }
                }
            })
            section_.find("*[RUD=in]").show();
            section_.find("*[RUD=out]").hide();
        }else{
            section_.find("*[RUD=in]").hide();
            section_.find("*[RUD=out]").show();
            $("*[RUD=thumbnail]").attr('src', '/static/images/user.svg');
            $(".ction_div *").css("pointer-events", "none")
            $(".ction_div").attr("onclick", "$(\".sign_div a\").click()")
        }
    },

    follow: function(user_id_, added_){
        if(!userHelper.access(true)){return false;}
        var ajax_data = {
            id: user_id_,
        };
        $.ajax({type: 'GET', url: "/api/auth/follow", data: ajax_data, dataType:"json"});
        if(added_ == true || added_ == "true"){
            $('.item.'+user_id_).find('.follow').html('دنبال کنید').removeClass('true').addClass('false').attr('onclick','userHelper.follow(\''+user_id_+'\',\'false\')')
            $('[user-id='+user_id_+"]").find('.follow.text').html('دنبال کنید').removeClass('true').addClass('false').attr('onclick','userHelper.follow(\''+user_id_+'\',\'false\')')
            $('[user-id='+user_id_+"]").find('.follow').removeClass('true').addClass('false').attr('onclick','userHelper.follow(\''+user_id_+'\',\'false\')')
            var is_following = false
        }else{
            $('.item.'+user_id_).find('.follow').html('حذف کنید').removeClass('false').addClass('true').attr('onclick','userHelper.follow(\''+user_id_+'\',\'true\')')
            $('[user-id='+user_id_+"]").find('.menu > .follow.text').html('حذف کنید').removeClass('false').addClass('true').attr('onclick','userHelper.follow(\''+user_id_+'\',\'true\')')
            $('[user-id='+user_id_+"]").find('.follow').removeClass('false').addClass('true').attr('onclick','userHelper.follow(\''+user_id_+'\',\'true\')')
            var is_following = true
        }
        this.writeFollowDataToLocalStorage(is_following, user_id_)
        this.commitChanges()
    },

    writeFollowDataToLocalStorage: function(is_following_, target_user_id_){
        if(is_following_){
            this.followArray.push(target_user_id_);
        }else{
            var index = this.followArray.indexOf(target_user_id_);
            this.followArray.splice(index,1);
        }
    },

    commitChanges: function(){
        store("user-follow-data", JSON.stringify(this.followArray))
        store("last-follow-edit", Date.now())
    },

    is_following: function(user_id_){
        if(!this.access()){return false;}
        if(this.followArray.indexOf(user_id_) != -1 || user_id_ === this.user._id){
            var following = true;
        }else{
            var following = false;
        }
        return following;
    },

    access: function(error_){
        if(!this.user._id){
            if(error_){toast("لطفا ابتدا وارد سایت شوید!");}
            return false;
        }else{
            return true;
        }
    },

    sustain: function(){
        if(this.user.slug == "sustained" || this.user.slug == "blocked"){
            toast("اکانت شما مسدود شده است!");
            return false;
        }else{
            return true;
        }
    }
}
//endregion
//region profile
profileViewController = (function(){

    var view = false;
    var adapter = false;
    var indicator = false;
    var userID = false;

    var init = function (urlVars_) {

        urlVars = urlVars_;

        if(urlVars._id != userID){
            adapter = null
            view = View.requestPage("profile", true);
            View.terminateFunc = function(){
                itemHolder = false;
                userID = false
            }
            userID = urlVars._id
        }

        if(!adapter){
            adapter = new Adapter()
            adapter.onOverScroll = true
            adapter.limit = 20
            adapter.constructView = function(item){return itemClass.construct(item)}
        }

        adapter.onComplete = function(data_){
                View.clearLoadingPage()
                load(data_.user)
        }
        adapter.init(view, {gridify: true, gridifyWidth: 290})
        adapter.fetch("/api/user/profile", {type: urlVars.type, limit: 20, userID: urlVars._id}, urlVars.type)

        if(!indicator){
            indicator = new ResponsiveIndicator()
        }
        indicator.init(view.find(".actions"), urlVars.type)
    }

    var load = function(userModel_){
        var parent = view;
        userHelper.RUD(parent, userModel_);
        view.attr("user-id", userModel_._id)
        if(userModel_._id == userHelper.user._id){
            // do the following
            parent.find(".menu.target").hide();
            parent.find(".menu.owner").show();
        }else{
            parent.find(".menu.target").show();
            parent.find(".menu.owner").hide();
            // do the following
            var followingButton = parent.find(".menu .follow");
            if(userHelper.is_following(userModel_._id)){
                followingButton.html("حذف کنید").addClass("true").removeClass("false").attr("onclick", "userHelper.follow(\""+userModel_._id+"\", true)")
            }else{
                followingButton.html("دنبال کنید").addClass("false").removeClass("true").attr("onclick", "userHelper.follow(\""+userModel_._id+"\", false)")
            }
        }
        parent.find(".more").attr("onclick", "profileViewController.menu(\""+userModel_._id+"\")")
    }

    var menu = function(userID_){
        var map = {}
        map["title"] = "منوی پروفایل"
        map["گزارش پروفایل نا مناسب"] = "profileViewController.flag(\""+userID_+"\")"
        View.drawMenu(view.find(".more"), map)
    }

    var flag = function(userId_){
        var ajax_data = {
            target: userId_,
        };
        $.ajax({type: 'GET', url: "/authapi/user/flag", data: ajax_data, dataType:"json"});
        toast("پروفایل مورد نظر بررسی خواهد شد.")
    }

    var suggAdaper = false;
    var suggestions = function(){
        if(!userHelper.access(true)){return false;}
        view  = View.drawSidebar("user_sugg", "l", 300)
        // declare the new adapter
        suggAdaper = null
        suggAdaper = new Adapter()
        suggAdaper.onOverScroll = true
        suggAdaper.constructView = function(item){return returnTemp(item)}
        suggAdaper.disableCache = true
        suggAdaper.init(view)
        suggAdaper.fetch("/api/auth/get-suggestions")
    }

    returnTemp = function(item){
        var temp = View.getTemplateByID("user_sugg_item")
        temp.attr("user-id", item._id)
        temp.find("a").attr("href", "/profile/"+item._id+"/posts")
        temp.find(".image img").attr("src", "/upload/users/"+item.thumbnail)
        temp.find(".name").html(item.name)
        temp.find(".following").html(item.following)
        temp.find(".follow").click(function(){
            var followStatus = userHelper.is_following(item._id)
            userHelper.follow(item._id, followStatus)
        })
        return temp
    }

    return{
        init: init,
        menu: menu,
        flag: flag,
        suggestions: suggestions,
    }

}());
//endregion
//region edit
editViewController = {

    init: function(){
        if(!userHelper.access(true)){return false;}
        var view = View.requestOverlay("edit")
        userHelper.RUD($("#edit"), userHelper.user)
    },

    update_edit: function(){
        var parent = $('#edit');
        if(!validateForm(parent)){return false;}
        var ajax_data = {
            name: parent.find('.input_div input.name').val(),
            brief: parent.find('.input_div textarea.brief').val(),
        };
        parent.find('[loading=true]').css('loading');
        $.ajax({type: 'GET', url: "/api/auth/user/edit", data: ajax_data, dataType:"json",
            success:function(data){
                parent.find('[loading=true]').removeClass('loading');
                if(data.error){
                    tooltip({element: parent.find('.'+data.error),message: data.message});
                }else{
                    userHelper.user.name = ajax_data.name;
                    userHelper.user.brief = ajax_data.brief;
                    userHelper.RUD($('body'), userHelper.user);
                    toast(messages['profile_edit']);
                }
            }
        });
    },

    send_thumbnail: function(){
        if(!u_image_validation($('#edit_user_thumbnail'))){return 0;};
        var formData = new FormData();
        formData.append('image', $('#edit_user_thumbnail')[0].files[0]);
        formData.append('name', name);
        $('#edit').find('#edit_send_thumbnail').addClass('loading');
        $.ajax({ type: 'POST', url: "/api/auth/thumbnail", data: formData, processData: false, contentType: false,
            success:function(data){
                $('#edit').find('img').attr('src','/upload/users/'+data.image);
                userHelper.user.thumbnail = data.image;
                toast(messages['profile_thumbnail']);
                userHelper.RUD($('body'),userHelper.user);
            },
        });
    },
}
//endregion

//region search view controller
searchViewController = (function(){

    var view;
    var adapter;
    var indicator;
    var searchType;
    var searchBox;

    var begin = function(){
        router.prepURL("/search/user/")
    }

    var init = function(routerUrl_){
        searchBox = $("#menu .search_div input")
        if(searchBox.val().length < 3) return
        view = View.requestPage("search");
        searchType = routerUrl_.type

        adapter = null

        if(!adapter){
            adapter = new Adapter()
            adapter.onOverScroll = true
            adapter.limit = 20
            adapter.constructView = function(item){return itemClass.construct(item)}
        }

        adapter.init(view, {gridify: true, gridifyWidth: 290})

        if(!indicator){
            indicator = new ResponsiveIndicator()
        }
        indicator.init(view.find(".sort_div"), searchType)
        adapter.fetch("/api/"+searchType+"/search", {limit: 20, term: searchBox.val()}, searchType+"--"+searchBox.val())
        listenToEnter();
    }

    detect = function(e){
        if (e.keyCode == 13) {
            begin()
            return false;
        }
    }

    return{
        begin: begin,
        detect: detect,
        init: init,
    }
}())
//endregion

//region noti module
notificationViewController = (function(){

    /**
     * listen to sockets
     * get the count of notifications from server
     */
    var prepare = function(){
        // listen to socket incomes
        socket.on('notification', function(obj){
            handleNewItemsFromScoket(obj);
        });
        // get number of notifications form server
        httpRequest("/api/auth/get_notifications_count", {}, function(result){
            if(result && result.result){
                badgeCount = result.result
                badgeControll()
            }
        })
    };

    /**
     * initialize the adapter and the view
     */
    var view;
    var adapter;
    var init = function(routerUrl_){
        view  = View.drawSidebar("notification", "l", 300)
        // declare the new adapter
        if(!adapter){
            adapter = new Adapter()
            adapter.onOverScroll = true
            adapter.constructView = function(item){return returnTemp(item)}
            adapter.emptyMessage = "<div class='empty'>روز آرامی به نظر می رسد</div>"
        }
        adapter.init(view)
        adapter.fetch("/api/auth/get_notifications")
        // after its rendered
        setTimeout(function(){
            $.ajax({type: 'PUT', url: "/api/auth/clear_noti", dataType:"json"});
        }, 1000)
        // show the count of notifications in the view
        view.find(".head .badge").html(badgeCount)
        if(badgeCount > 0) view.find(".head .badge").addClass("alert")
        // make the view badges 0
        badgeCount = 0
        // after 100 seconds, so that the sidebar is drawn
        setTimeout(function(){
            badgeControll()
        }, 100)
    };

    // number of unread notifications
    var badgeCount = 0
    /**
     * takes care of showing the count of notifications in the menu
     */
    var badgeControll = function(){
        var notification = $("#menu").find(".user_div .noti")
        if(badgeCount > 0){
            notification.addClass("active")
        }else{
            notification.removeClass("active")
        }
        notification.html(badgeCount)
    }

    var returnTemp = function(item){
        if(!item || !item.type) return ""

        if(item.date) var time = get_time(item.date)

        var temp = View.getTemplateByID("notification_item")

        var message = getMessageByType(item)
        temp.find("a").attr("href", message.href)
        temp.find(".image img").attr("src", message.image)
        temp.find(".username").html(message.name)
        temp.find(".message").html(message.text)
        temp.find(".time").html(time)

        if(item.seen == true) temp.addClass("seen")
        item.seen = true

        var postHolder = $("<div />", {class: "item_holder"})
        temp.find(".message .post").click(function(){
            postHolder.remove()
            postHolder = $("<div />", {class: "item_holder"})
            var close = $("<div />", {class: "close", text: "بستن"})
            close.click(function(){postHolder.remove()})
            postHolder.prepend(close)
            itemClass.loadItem(item.objectId, function(result){
                postHolder.append(result)
                View.releaseLoaderDiv(postHolder)
            })
            View.makeLoaderDiv(postHolder)
            temp.after(postHolder)
        })

        return temp
    }

    var getMessageByType = function(model){
        var result = false;
        switch (model.type) {
            case "comment":
                result = {
                    image: "/upload/users/"+model.dowerId.thumbnail,
                    href: "/profile/"+model.dowerId._id+"/posts",
                    name: model.dowerId.name,
                    text: "برای <span class=post>پست</span> شما نظر گذاشته"
                }
                break
            case "like":
                result = {
                    image: "/upload/users/"+model.dowerId.thumbnail,
                    href: "/profile/"+model.dowerId._id+"/posts",
                    name: model.dowerId.name,
                    text: "<span class=post>پست</span> شما را لایک کرده"
                }
                break
            case "follow":
                result = {
                    image: "/upload/users/"+model.dowerId.thumbnail,
                    href: "/profile/"+model.dowerId._id+"/posts",
                    name: model.dowerId.name,
                    text: "شما را دنبال می کند"
                }
                break
            case "post_blocked":
                result = {
                    image: "/static/images/logo.svg",
                    href: "",
                    name:"جوکی",
                    text: "کاربر گرامی، <span class=post>پست شما</span> به دلیل رعایت نکردن <a href=/policies >قوانین سایت </a> حذف شده است<br/> اگر در طول هفته 5 بار پست شما حذف شود، حساب کاربری شما مسدود خواهد شد"
                }
                break
            case "flag_post_blocked":
                result = {
                    image: "/static/images/logo.svg",
                    href: "",
                    name:"جوکی",
                    text: "کاربر گرامی <span class=post>این پست</span> که توسط شما گزارش تخلف داشته، اکنون از سایت حذف شده است. با تشکر "
                }
                break
            case "you_are_blocked":
                result = {
                    image: "/static/images/logo.svg",
                    href: "",
                    name:"جوکی",
                    text: "کاربر گامی، حساب کاربری شما به دلیل رعایت نکردن <a href=/policies >قوانین سایت </a> مسدود شده است، در صورت اعتراض  میتوانید از بخش تماس با ما، این موضوع را مطرح کنید"
                }
                break
            case "you_are_sustained":
                var time = model.objectId
                result = {
                    image: "/static/images/logo.svg",
                    href: "",
                    name:"جوکی",
                    text: "کاربر گرامی، متاسفانه به دلیل ارسال 5 پست نامناسب در طول هفته، حساب کاربری شما برای مدت <span>"+time+" روز</span> روز مسدود شده است، در این زمان شما قادر به ارسال پست یا نظر نخواهید بود."
                }
                break
            case "you_unsustained":
                result = {
                    image: "/static/images/logo.svg",
                    name:"جوکی",
                    text: "کاربر گرامی، حساب کاربری شما از حالت مسدود بودن خارج شده، لطفا نسبت به پست های ارسالی دقت بیشتری به خرج دهید."
                }
                break

        }
        return result
    }

    var handleNewItemsFromScoket = function(newSocketItem_){
        if(adapter){
            adapter.itemsArray.main.push(newSocketItem_)
        }
        badgeCount++
        badgeControll()
    }

    return{
        init: init,
        prepare: prepare,
    }

}())
//endregion

//region on home click
var goHome = function(){
    router.prepURL("/")
}
var makeHomeIntoSidebar = function(){
    var menu = $("#menu");
    menu.find(".logo_div").attr("onclick", "open_sidebar()")
    menu.find(".logo_div .sidebar").show()
    menu.find(".logo_div .back").hide()
}

var makeHomeIntoBack = function(){
    var menu = $("#menu");
    menu.find(".logo_div").attr("onclick", "goHome()")
    menu.find(".logo_div .sidebar").hide()
    menu.find(".logo_div .back").show()
}
//endregion
//region sidebar
var open_sidebar = function(){
    var view = View.drawSidebar("sidebar", "r")
    userHelper.RUD(view, userHelper.user)
}
//endregion

//region feedback
var feedback = {

    init: function(routerUrl_){
    	// var from = getParameterByName(routerUrl_.URL, "subject")
        var view = View.requestOverlay("feedback")
        userHelper.RUD(view.html, userHelper.user);
    },

    send: function(){
        var parent = $("#feedback");
        if(!validateForm(parent)){return false;}
        ajax_data = {
            name: parent.find(".name").val(),
            email: parent.find(".email").val(),
            body: parent.find(".body").val(),
        }
        View.clearOverlay()
        $.ajax({type: 'GET', url: "/api/sendFeedback", data: ajax_data, dataType: "json",});
        toast("فیدبک شما ارسال شد و در صورت نیاز به آن پاسخ داده میشود.")
        store("feedback", "true")
    }
}
//endregion
//region splash action
splash = function(){
    if(detectCSSFeature("animation")){
        $('.splash_page').addClass('fadeAway').bind("animationend webkitAnimationEnd oanimationend MSAnimationEnd", function(){
            $(this).remove();
        });
    }else{
        $('.splash_page').remove();
    }
}
//endregion

//region socket
var socket = false
var connectToSocket = function(){
    socket = new Socket()
    if(!userHelper.user) return
    // get the sessionID from server
    httpRequest("/api/auth/get-session-id", {}, function(data){
        var sessionID = data.result
        // establish the socket connection
        //socket.init("ws://localhost:81/"+sessionID+"/"+userHelper.user._id, function(err, result){
        socket.init("ws://joky.ir:81/"+sessionID+"/"+userHelper.user._id, function(err, result){
        })
    })
}
//endregion

//region view init 
var View
var viewInit = function(){
    templates = $("<div/>");
    templates.html(Templates);
    $("body").append(templates.find("#body").html())
    View = new HandleView()
    View.init(templates, {loaderTemplate: "loader"})
     View.interActiveMenu($("#menu"))
     View.disableInteractiveMenu = true
    makeHomeIntoBack();
}
//endregion
//region router
var routerInit = function(){
    var map = {
        "/": mainController.init,
        "/(type)[feeds|trending|popular|new|you|top-users|controversial]": mainController.init,
        "/item/(id)": itemClass.loadSingleItemInPage,
        "/profile/(_id)/(type)[posts|following|followers]": profileViewController.init,
        "/search/(type)[user|item]": searchViewController.init,
        "/404": function(){View.requestPage("404")},
    }
    var looseMap = {
        "/user/(type)[signup|signin]": function(url){sign.init(url)},
        "/edit-profile": function(url){editViewController.init(url)},
        "/policies": function(){View.requestOverlay("policies")},
        "/feedback": feedback.init
    }
    router = new Router()
    router.init(map, looseMap);
}
//endregion
//region on ready
var templates;
window.onload = function(){
    viewInit()
    userHelper.init()
    connectToSocket()
    routerInit();
    notificationViewController.prepare()
    splash()
}
//endregion
