//region required modules
var express = require('express'),
    app = express(),
    http = require('http').Server(app),
    path = require('path'),
    url = require('url'),
    mongoose = require('mongoose'),
    models = require('./models'),
    dbUrl = process.env.MONGOHQ_URL || 'localhost:27017/blog',
    //dbUrl = '185.81.96.62:27017/blog',
    db = mongoose.connect(dbUrl, {safe: true}, function(err){if(err){console.log("error", err)}}),
    cookieParser = require('cookie-parser'),
    errorHandler = require('errorhandler'),
    session = require('express-session'),
    bodyParser = require('body-parser'),
    methodOverride = require('method-override'),
    logger = require('morgan'),
    useragent = require('express-useragent'),
    async = require('async');
var ua = require('universal-analytics');
var MongoStore = require('connect-mongo')(session);
var compress = require('compression');
//endregion

//region initialization
var ga = ua('UA-64601265-1');
app.set('views', path.join(__dirname, 'public'));
app.set('view engine', 'jade');
app.use(bodyParser.json());
app.use(useragent.express());
app.use(cookieParser('3CCC4ACD-6ED1-4844-9217-82131BDCB239'));
var store = new MongoStore({
    mongooseConnection: mongoose.connection,
    ttl: 14 * 24 * 60 * 60 // = 14 days. Default
});
app.use(session({
    //store: store,
    secret: '2C44774A-D649-4D44-9535-46E296EF984F',
    cookie: {
        maxAge: 1000 * 60 * 60 * 24 * 14 // 14 days
    },
}))
app.use(compress());
app.use(bodyParser.urlencoded());
app.use(methodOverride());
app.use('/static', express.static(__dirname + '/public'));
app.use('/upload', express.static(__dirname + '/upload'));
app.use(logger('dev'));
if('development' === app.get('env')){app.use(errorHandler());}
// use models
app.use(function(req, res, next) {req.models = models; return next();});
var helper = require('./scripts/helper.js');
var fileHandling = require('./scripts/FileHandling.js');
require('./scripts/auth.js')(app);
require('./scripts/messages.js')(app);
require('./scripts/Item.js').app(app);
require('./scripts/Follow.js').app(app);
require('./scripts/ItemExtra.js').app(app);
require('./scripts/Noti.js').app(app);
require('./scripts/Add.js').app(app);
require('./scripts/User.js').app(app);
require('./scripts/Admin.js').app(app);
//endregion

//region authenticate by sessionID
exports.authenticateBySessionID = function(userID, callback){
    models.User.findOne({"_id": userID}, function(err, result){
        if(!err && result) return callback(true, result)
        callback(false, null)
    })
}
//endregion

//region feedback
app.get('/api/sendFeedback', function(req, res, next){
    var config = {
        "to": "work.shakiba@gmail.com",
        "subject": "USER FEED BACK",
        "body": "from: "+req.query.name+"<br/> as email: "+req.query.email+"<br/> says: "+req.query.body,
    }
    helper.emailTo(config);
});
//endregion

//region index
app.get('/*', function(req, res, next){
    if(req.path.indexOf("/upload/") != -1){
        return next()
    }
    if(req.path.indexOf("/static/") != -1){
        return next()
    }
    if(req.path.indexOf("/api/") != -1){
        return next()
    }
    if(req.path.indexOf("/ajax/") != -1){
        return next()
    }
    if(req.path.indexOf(".ico") != -1){
        return next()
    }
    return res.render('html/main', {jsName: jsfileName});
});
//endregion

//region listen
var jsfileName = "js3"
http.listen(80, function(){
    // update the template file if necessary
    //fileHandling.generateTemplateFile("public/html/html.html", "public/html/html.js")
    // update the js files if needed
    var jsFiles = ["base", "helper", "view", "indicator", "router", "conn", "adapter", "main"]
    //fileHandling.watchJsFiles(jsFiles, "minified")
    console.log("server ready");
});
//endregion
